//
//  Observable.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

import UIKit

class Observable<T> {
    var value: T {
        didSet {
            DispatchQueue.main.async {
                for event in self.valueChanged {
                    event?(self.value)
                }
                
                self.valueChangedDic.keys.forEach {
                    if let event: event = self.valueChangedDic[$0] {
                        event?(self.value)
                    }
                }
            }
        }
    }
    
    private var valueChanged = [((T) -> Void)?] ()
    private var valueChangedDic = [String : ((T) -> Void)?] ()
    private typealias event = ((T) -> Void)?
    init(value: T) {
        self.value = value
    }
    
    // Add closure as an observer and trigger the closure immediately if fireNow = true
    func addObserver(fireNow: Bool = true, _ onChange: ((T) -> Void)?) {
        valueChanged.append(onChange)
        if fireNow {
            onChange?(value)
        }
    }
    
    func removeObserver() {
        valueChanged.removeAll()
    }
    
    func addObserver(observer: NSObject, fireNow: Bool = true, _ onChange: ((T) -> Void)?) {
        let key = String(describing: observer)
        if !valueChangedDic.keys.contains(key) {
            valueChangedDic[key] = onChange
        }
        
        if fireNow {
            onChange?(value)
        }
    }
    
    func removeObserver(observer: NSObject) {
        let key = String(describing: observer)
        if valueChangedDic.keys.contains(key) {
            valueChangedDic.removeValue(forKey: key)
        }
    }
}
