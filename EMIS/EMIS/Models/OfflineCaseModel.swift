//
//  OfflineCaseModel.swift
//  EMIS
//
//  Created by Jimmy on 2022/11/14.
//

import Foundation

struct OfflineCaseModel: Codable {
    var addCaseModel: AddCaseModel!
    var casualtyModels: [CasualtyModel] = []
    var imageList: [String] = []

    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let addCaseModel = data["addCaseModel"] as? [String : Any] {
            self.addCaseModel = AddCaseModel(data: addCaseModel)
        } else {
            errorList.append("addCaseModel")
        }
        
        var models:[CasualtyModel] = []
        if let dataList = data["casualtyModels"] as? [[String : Any]] {
            dataList.forEach {
                models.append(CasualtyModel(data: $0))
            }
            
            casualtyModels = models
        } else {
            errorList.append("influence_scope")
        }
        
        if let imageList = data["imageList"] as? [String] {
            self.imageList = imageList
        } else {
            errorList.append("imageList")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("ScopeModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        dictionary["addCaseModel"] = self.addCaseModel.dictionary()
        var casualty: [[String: Any]] = []
        self.casualtyModels.forEach {
            casualty.append($0.dictionary())
        }
        dictionary["casualtyModels"] = casualty

        dictionary["imageList"] = imageList
        
        return dictionary
    }
}
