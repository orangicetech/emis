//
//  DesSampleModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/27.
//

import UIKit

struct DesSampleModel {
    var category_id: String = ""
    var category: String = ""
    var situ: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let category_id = data["category_id"] as? String {
            self.category_id = category_id
        } else {
            errorList.append("category_id")
        }
        
        if let category = data["category"] as? String {
            self.category = category
        } else {
            errorList.append("category")
        }
        
        if let situ = data["situ"] as? String {
            self.situ = situ
        } else {
            errorList.append("situ")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("DesSampleModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}
