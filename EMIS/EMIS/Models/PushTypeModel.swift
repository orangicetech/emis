//
//  PushLogModel.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/11.
//

import Foundation

struct PushTypeModel {
    var id: Int = 0
    var name: String = ""

    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
                
        if let id = data["id"] as? Int {
            self.id = id
        } else {
            errorList.append("id")
        }
        
        if let name = data["name"] as? String {
            self.name = name
        } else {
            errorList.append("name")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("PushTypeModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}

