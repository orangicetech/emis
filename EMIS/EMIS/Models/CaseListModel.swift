//
//  CaseListModel.swift
//  EMIS
//
//  Created by Polowu on 2021/3/7.
//

import UIKit
struct CaseListModel {
    var no: String = ""
    var category: String = ""
    var receive_date: String = ""
    var location_address: String = ""
    var location_district: String = ""
    var status: String = ""
    var situation: String = ""
    var case_pic: [String] = []
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        if let no = data["no"] as? String {
            self.no = no
        } else {
            errorList.append("no")
        }
        
        if let category = data["category"] as? String {
            self.category = category
        } else {
            errorList.append("category")
        }
        
        if let receive_date = data["receive_date"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            let date = dateFormatter.date(from: receive_date)
            self.receive_date = DateManager.dateChinaTransformTimestamp(Int(date!.timeIntervalSince1970))!
        } else {
            errorList.append("receive_date")
        }
        
        if let location_address = data["location_address"] as? String {
            self.location_address = location_address
        } else {
            errorList.append("location_address")
        }
        
        if let location_district = data["location_district"] as? String {
            self.location_district = location_district
        } else {
            errorList.append("location_district")
        }
        
        if let status = data["status"] as? String {
            self.status = status
        } else {
            errorList.append("status")
        }
        
        if let situation = data["situation"] as? String {
            self.situation = situation
        } else {
            errorList.append("situation")
        }
        
        if let case_pic = data["case_pic"] as? [String] {
            self.case_pic = case_pic
        } else {
            errorList.append("case_pic")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("CaseListModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
}
