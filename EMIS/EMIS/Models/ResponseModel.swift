//
//  ResponseModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

import UIKit

class ResponseModel: NSObject {
    var status: Bool = false
    var message: String = ""
    var data: [String : Any] = [:]
    var datas: [Any] = []
    var data_injury: [Any] = []
    var data_disaster: [Any] = []
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        self.data = data
        
        if let status = data["status"] as? Bool {
            self.status = status
        } else {
            errorList.append("status")
        }
        
        if let message = data["message"] as? String {
            self.message = message
        } else {
            errorList.append("message")
        }
        
        if let d = data["data"] as? [String: Any] {
            self.data = d
        } else {
            if let d = data["data"] as? [Any] {
                self.datas = d
            } else {
                errorList.append("data")
            }
        }
        
        if let data_injury = data["data_injury"] as? [Any] {
            self.data_injury = data_injury
        }
        
        if let data_disaster = data["data_disaster"] as? [Any] {
            self.data_disaster = data_disaster
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("ResponseModel fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
    init(success: Bool) {
        self.status = success
        self.data = [String: Any]()
    }
    
    init(error: APIError) {
        self.status = false
        self.message = "Error code: \(error)"
        self.data = [String: Any]()
    }
    
    init(success: Bool, data: [String: Any]) {
        self.status = success
        self.data = data
        
        if let d = data["data"] as? [String: Any] {
            self.data = d
        } else {
            if let d = data["data"] as? [Any] {
                self.datas = d
            } else {
                errorList.append("data")
            }
        }
    }
}

public enum APIError: Error {
    case resultFail
    case timeOut
    case permissionDenied
    case unknowError
    case decodeJSONFail
    case inProgress
    case uploadError
    case encodingError
}
