//
//  CasualtyModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/3.
//

import Foundation

struct CasualtyModel: Codable {
    var seqno: String = ""
    var injury_id: String = ""
    var injury: String = ""
    var name: String = ""
    var age: String = ""
    var gender: String = "" // 男或女
    var description: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        if let seqno = data["seqno"] as? String {
            self.seqno = seqno
        } else {
            errorList.append("seqno")
        }
        
        if let injury = data["injury"] as? String {
            self.injury = injury
        } else {
            errorList.append("injury")
        }
        
        if let injury_id = data["injury_id"] as? String {
            self.injury_id = injury_id
        } else {
            errorList.append("injury_id")
        }
        
        if let name = data["name"] as? String {
            self.name = name
        } else {
            errorList.append("name")
        }
        
        if let age = data["age"] as? String {
            self.age = age
        } else {
            errorList.append("age")
        }
        
        if let gender = data["gender"] as? String {
            self.gender = gender
        } else {
            errorList.append("gender")
        }
        
        if let description = data["description"] as? String {
            self.description = description
        } else {
            errorList.append("description")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("CasualtyModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["seqno"] = self.seqno
        dictionary["injury_id"] = self.injury_id
        dictionary["injury"] = self.injury
        dictionary["name"] = self.name
        dictionary["age"] = self.age
        dictionary["gender"] = self.gender
        dictionary["description"] = self.description
        return dictionary
    }
}
