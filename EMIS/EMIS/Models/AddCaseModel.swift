//
//  AddCaseDataModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/8.
//

import Foundation
struct AddCaseModel:Codable {
    
    var project_id: String = ""
    var receive_date: String = ""
    var request_person: String = ""
    var request_person_phone: String = ""
    var degree: String = ""
    var category: String = ""
    var description: String = ""
    var influence_scope: [ScopeModel] = []
    var location: MapInfo!
    
    // for offline
    var categoryString: String = ""
    var projectString: String = ""
    var case_no: String = ""
    var upload: Bool = false

    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let project_id = data["project_id"] as? String {
            self.project_id = project_id
        } else {
            errorList.append("project_id")
        }
        
        if let receive_date = data["receive_date"] as? String {
            self.receive_date = receive_date
        } else {
            errorList.append("receive_date")
        }
        
        if let request_person = data["request_person"] as? String {
            self.request_person = request_person
        } else {
            errorList.append("request_person")
        }
        
        if let request_person_phone = data["request_person_phone"] as? String {
            self.request_person_phone = request_person_phone
        } else {
            errorList.append("request_person_phone")
        }
        
        if let degree = data["degree"] as? String {
            self.degree = degree
        } else {
            errorList.append("degree")
        }
        
        if let category = data["category"] as? String {
            self.category = category
        } else {
            errorList.append("category")
        }
        
        if let description = data["description"] as? String {
            self.description = description
        } else {
            errorList.append("description")
        }
                
        var scope:[ScopeModel] = []
        if let scopeList = data["influence_scope"] as? [[String : Any]] {
            scopeList.forEach {
                scope.append(ScopeModel(data: $0))
            }
            
            influence_scope = scope
        } else {
            errorList.append("influence_scope")
        }
        
        if let location = data["location"] as? [String : Any] {
            self.location = MapInfo(data: location)
        } else {
            errorList.append("location")
        }
        
        if let case_no = data["case_no"] as? String {
            self.case_no = case_no
        } else {
            errorList.append("case_no")
        }
        
        if let projectString = data["projectString"] as? String {
            self.projectString = projectString
        } else {
            errorList.append("projectString")
        }
        
        if let categoryString = data["categoryString"] as? String {
            self.categoryString = categoryString
        } else {
            errorList.append("categoryString")
        }
        
        if let upload = data["upload"] as? Bool {
            self.upload = upload
        } else {
            errorList.append("upload")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("ScopeModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["project_id"] = self.project_id
        dictionary["receive_date"] = self.receive_date
        dictionary["request_person"] = self.request_person
        dictionary["request_person_phone"] = self.request_person_phone
        dictionary["degree"] = self.degree
        dictionary["category"] = self.category
        dictionary["description"] = self.description
        dictionary["projectString"] = self.projectString
        dictionary["categoryString"] = self.categoryString
        dictionary["case_no"] = self.case_no
        dictionary["upload"] = self.upload

        var scope: [[String: Any]] = []
        self.influence_scope.forEach {
            scope.append($0.dictionary())
        }
        
        dictionary["influence_scope"] = scope
        dictionary["location"] = location.dictionary()
        return dictionary
    }
}
