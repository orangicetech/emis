//
//  PushLogModel.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/11.
//

import Foundation

struct PushLogModel {
    var title: String = ""
    var message: String = ""
    var cdate: String = ""
    var no: Int = 0
    var typeid: Int = 0
    var typename: String = ""
    var read: Bool = false
    var replyno: Int = 0
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let title = data["title"] as? String {
            self.title = title
        } else {
            errorList.append("title")
        }
        
        if let message = data["message"] as? String {
            self.message = message
        } else {
            errorList.append("message")
        }
        
        if let creation_date = data["cdate"] as? String {
            self.cdate = creation_date
        } else {
            errorList.append("cdate")
        }
        
//        if let creation_date = data["cdate"] as? String {
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//            dateFormatter.timeZone = TimeZone.current
//            dateFormatter.locale = Locale.current
//            let date = dateFormatter.date(from: creation_date)
//            self.cdate = DateManager.dateChinaTransformTimestamp(Int(date!.timeIntervalSince1970))!
//        } else {
//            errorList.append("cdate")
//        }
        
        if let no = data["no"] as? Int {
            self.no = no
        } else {
            errorList.append("no")
        }
        
        if let replyno = data["replyno"] as? Int {
            self.replyno = replyno
        } else {
            errorList.append("replyno")
        }
        
        if let typeid = data["typeid"] as? Int {
            self.typeid = typeid
        } else {
            errorList.append("typeid")
        }
        
        if let typename = data["typename"] as? String {
            self.typename = typename
        } else {
            errorList.append("typename")
        }
        
        if let read = data["read"] as? Bool {
            self.read = read
        } else {
            errorList.append("read")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("PushLogModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}

