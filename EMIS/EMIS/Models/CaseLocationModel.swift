//
//  CaseLocationModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/11.
//

import Foundation
import CoreLocation

struct CaseLocationModel {
    var latitude: CLLocationDegrees = 0
    var longitude: CLLocationDegrees = 0
    var execution_date: String = ""
    var case_no: String = ""
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["case_no"] = self.case_no
        dictionary["execution_date"] = self.execution_date
        dictionary["latitude"] = self.latitude
        dictionary["longitude"] = self.longitude
        return dictionary
    }
}
