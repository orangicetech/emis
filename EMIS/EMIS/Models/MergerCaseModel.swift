//
//  MergerCaseModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/11.
//

import Foundation

struct MergerCaseModel {
    var case_no: String = ""
    var mergerCaseNoList: [String] = []
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["case_no"] = self.case_no
        dictionary["Merger"] = self.mergerCaseNoList
        return dictionary
    }
}
