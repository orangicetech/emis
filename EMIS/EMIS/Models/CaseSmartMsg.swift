//
//  CaseSmartMsg.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/31.
//

import Foundation

struct CaseSmartMsg {
    var typeid: String = ""
    var typename: String = ""
    var content: String = ""
    
    var isValid = false
    var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let typeid = data["typeid"] as? String {
            self.typeid = typeid
        } else {
            errorList.append("typeid")
        }
        
        if let typename = data["typename"] as? String {
            self.typename = typename
        } else {
            errorList.append("typename")
        }
        
        if let content = data["content"] as? String {
            self.content = content
        } else {
            errorList.append("content")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("InjuryModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
 
}
