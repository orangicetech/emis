//
//  UserModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

import UIKit

class UserModel: NSObject {
    
    var user_id: Int = 0
    var user_name: String = ""
    var phone: String = ""
    var dept_no: String = ""
    var dept_name: String = ""
    var session_token: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        if let user_id = data["user_id"] as? Int {
            self.user_id = user_id
        } else {
            errorList.append("user_id")
        }
        
        if let user_name = data["user_name"] as? String {
            self.user_name = user_name
        } else {
            errorList.append("user_name")
        }
        
        if let phone = data["phone"] as? String {
            self.phone = phone
        } else {
            errorList.append("phone")
        }
        
        if let dept_no = data["dept_no"] as? String {
            self.dept_no = dept_no
        } else {
            errorList.append("dept_no")
        }
        
        if let dept_name = data["dept_name"] as? String {
            self.dept_name = dept_name
        } else {
            errorList.append("dept_name")
        }
        
        if let session_token = data["session_token"] as? String {
            self.session_token = session_token
        } else {
            errorList.append("session_token")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("UserModel fail to retrieve fields: \(errorList)")
            }
        } else {
            if session_token.count == 0 {
                isValid = false
            } else {
                isValid = true
            }
            
        }
    }
    
    public func dictionary() -> [String : Any] {
        var dic: [String : Any] = [:]
        dic["user_id"] = self.user_id
        dic["user_name"] = self.user_name
        dic["phone"] = self.phone
        dic["dept_no"] = self.dept_no
        dic["dept_name"] = self.dept_name
        dic["session_token"] = self.session_token
        return dic
    }
}
