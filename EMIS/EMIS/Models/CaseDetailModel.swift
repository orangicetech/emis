//
//  CaseDetailModel.swift
//  EMIS
//
//  Created by Polowu on 2021/3/8.
//

import Foundation
struct CaseDetailModel {
    var no: String = ""
    var receive_date: String = ""
    var category: String = ""
    var category_id: String = ""
    var situation: String = ""
    var fb_grouds_id: String = ""
    var influence_scope: [ScopeModel] = []
    var latest_comment: String = ""
    var proc_pic: [Any] = []
    var case_pic: [String] = []
    var location: MapInfo = MapInfo(data: [:])
    var nearby_case_distance_desc: String = ""
    var related_dept_list: [RelatedModel] = []
    var nearby_case_list: [NearCaseModel] = []
    var smart_replymsg: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let no = data["no"] as? String {
            self.no = no
        } else {
            errorList.append("no")
        }
        
        if let receive_date = data["receive_date"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            let date = dateFormatter.date(from: receive_date)
            self.receive_date = DateManager.dateChinaTransformTimestamp(Int(date!.timeIntervalSince1970))!
        } else {
            errorList.append("receive_date")
        }
        
        if let category = data["category"] as? String {
            self.category = category
        } else {
            errorList.append("category")
        }
        
        if let category_id = data["category_id"] as? String {
            self.category_id = category_id
        } else {
            errorList.append("category_id")
        }
        
        if let situation = data["situation"] as? String {
            self.situation = situation
        } else {
            errorList.append("situation")
        }
        
        if let fb_grouds_id = data["fb_grouds_id"] as? String {
            self.fb_grouds_id = fb_grouds_id
        } else {
            errorList.append("fb_grouds_id")
        }
        
        var scope:[ScopeModel] = []
        if let scopeList = data["influence_scope"] as? [[String : Any]] {
            scopeList.forEach {
                scope.append(ScopeModel(data: $0))
            }
            
            influence_scope = scope
        } else {
            errorList.append("influence_scope")
        }
        
        if let latest_comment = data["latest_comment"] as? String {
            self.latest_comment = latest_comment
        } else {
            errorList.append("latest_comment")
        }
        
        var case_pic_list:[String] = []
        if let case_pic = data["case_pic"] as? [String] {
            case_pic.forEach {
                case_pic_list.append($0)
            }
            
            self.case_pic = case_pic_list
        } else {
            errorList.append("case_pic")
        }
        
        var proc_pic_list:[String] = []
        if let proc_pic = data["proc_pic"] as? [String] {
            proc_pic.forEach {
                proc_pic_list.append($0)
            }
            
            self.proc_pic = proc_pic_list
        } else {
            errorList.append("proc_pic")
        }
        
        if let locationInfo = data["location"] as? [String : Any] {
            var info = MapInfo(data: [:])
            if let district = locationInfo["district"] as? String {
                info.district = district
            }
            if let address = locationInfo["address"] as? String {
                info.address = address
            }
            if let latitude = locationInfo["latitude"] as? Double {
                info.latitude = latitude
            }
            if let longitude = locationInfo["longitude"] as? Double {
                info.longitude = longitude
            }
            
            self.location = info
        } else {
            errorList.append("location")
        }
        
        var related:[RelatedModel] = []
        if let relatedList = data["related_dept_list"] as? [[String : Any]] {
            relatedList.forEach {
                related.append(RelatedModel(data: $0))
            }
            
            related_dept_list = related
        } else {
            errorList.append("related_dept_list")
        }
        
        var nearCase:[NearCaseModel] = []
        if let nearCaseList = data["nearby_case_list"] as? [[String : Any]] {
            nearCaseList.forEach {
                nearCase.append(NearCaseModel(data: $0))
            }
            
            nearby_case_list = nearCase
        } else {
            errorList.append("related_dept_list")
        }
        
        if let distanceDesc = data["nearby_case_distance_desc"] as? String {
            self.nearby_case_distance_desc = distanceDesc
        } else {
            errorList.append("nearby_case_distance_desc")
        }
        
        if let smart_replymsg = data["smart_replymsg"] as? String {
            self.smart_replymsg = smart_replymsg
        } else {
            errorList.append("smart_replymsg")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("CaseDetailModel fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}

struct RelatedModel {
    var name: String = ""
    var relation: String = ""
    var dept: String = ""
    var phone: String = ""
    var execution_time: String = ""
    var latitude: Double = 0
    var longitude: Double = 0
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let name = data["name"] as? String {
            self.name = name
        } else {
            errorList.append("name")
        }
        
        if let relation = data["relation"] as? String {
            self.relation = relation
        } else {
            errorList.append("relation")
        }
        
        if let dept = data["dept"] as? String {
            self.dept = dept
        } else {
            errorList.append("dept")
        }
        
        if let phone = data["phone"] as? String {
            self.phone = phone
        } else {
            errorList.append("phone")
        }
        
        if let latitude = data["latitude"] as? Double {
            self.latitude = latitude
        } else {
            errorList.append("latitude")
        }
        
        if let longitude = data["longitude"] as? Double {
            self.longitude = longitude
        } else {
            errorList.append("longitude")
        }
        
        if let execution_time = data["execution_time"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            let date = dateFormatter.date(from: execution_time)
            self.execution_time = DateManager.dateChinaTransformTimestamp(Int(date!.timeIntervalSince1970))!
        } else {
            errorList.append("execution_time")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("RelatedModel fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
}

struct NearCaseModel {
    var no: String = ""
    var receive_date: String = ""
    var category: String = ""
    var situation: String = ""
    var influence_scope: [ScopeModel] = []
    var location: MapInfo!
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let no = data["no"] as? String {
            self.no = no
        } else {
            errorList.append("no")
        }
        
        if let receive_date = data["receive_date"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            let date = dateFormatter.date(from: receive_date)
            self.receive_date = DateManager.dateChinaTransformTimestamp(Int(date!.timeIntervalSince1970))!
        } else {
            errorList.append("receive_date")
        }
        
        if let category = data["category"] as? String {
            self.category = category
        } else {
            errorList.append("category")
        }
        
        if let situation = data["situation"] as? String {
            self.situation = situation
        } else {
            errorList.append("situation")
        }
        
        var scope:[ScopeModel] = []
        if let scopeList = data["influence_scope"] as? [[String : Any]] {
            scopeList.forEach {
                scope.append(ScopeModel(data: $0))
            }
            
            influence_scope = scope
        } else {
            errorList.append("influence_scope")
        }
        
        if let locationInfo = data["location"] as? [String : Any] {
            var info = MapInfo(data: [:])
            if let district = locationInfo["district"] as? String {
                info.district = district
            }
            if let address = locationInfo["address"] as? String {
                info.address = address
            }
            if let latitude = locationInfo["latitude"] as? Double {
                info.latitude = latitude
            }
            if let longitude = locationInfo["longitude"] as? Double {
                info.longitude = longitude
            }
            
            self.location = info
        } else {
            errorList.append("location")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("NearCaseModel fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}
