//
//  DistrictModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/27.
//

import UIKit

struct DistrictModel {
    var id: String = ""
    var text: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let id = data["id"] as? String {
            self.id = id
        } else {
            errorList.append("id")
        }
        
        if let text = data["text"] as? String {
            self.text = text
        } else {
            errorList.append("text")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("DistrictModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}
