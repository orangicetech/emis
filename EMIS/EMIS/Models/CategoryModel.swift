//
//  CategoryModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/27.
//

import UIKit

struct CategoryModel {
    var id: String = ""
    var category: String = ""
    var sorting: Int = 0
    var serious: Bool = false //是否嚴重
    var scope: [ScopeModel] = []// 引響範圍
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let id = data["id"] as? String {
            self.id = id
        } else {
            errorList.append("id")
        }
        
        if let category = data["category"] as? String {
            self.category = category
        } else {
            errorList.append("category")
        }
        
        if let sorting = data["sorting"] as? Int {
            self.sorting = sorting
        } else {
            errorList.append("sorting")
        }
        
        if let serious = data["serious"] as? Bool {
            self.serious = serious
        } else {
            errorList.append("serious")
        }
        
        if let datas = data["scope"] as? [[String : Any]] {
            var scope: [ScopeModel] = []
            datas.forEach {
                scope.append(ScopeModel(data: $0))
            }
            self.scope = scope
        } else {
            errorList.append("scope")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("CategoryModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}

struct ScopeModel:Codable {
    var sno: Int = 0
    var description: String = ""
    var value: String = ""
    var unit: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let sno = data["sno"] as? Int {
            self.sno = sno
        } else {
            errorList.append("sno")
        }
        
        if let description = data["description"] as? String {
            self.description = description
        } else {
            errorList.append("description")
        }
        
        if let value = data["value"] as? Int {
            self.value = String(value)
        }
        
        if let unit = data["unit"] as? String {
            self.unit = unit
        } else {
            errorList.append("unit")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("ScopeModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["sno"] = self.sno
        dictionary["description"] = self.description
        dictionary["value"] = self.value
        dictionary["unit"] = self.unit
        return dictionary
    }
}
