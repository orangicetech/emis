//
//  ProjectListModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/26.
//

import UIKit

struct ProjectListModel {
    var id: String = ""
    var name: String = ""
    var category: String = ""
    var level: String = ""
    var creation_date: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let id = data["id"] as? String {
            self.id = id
        } else {
            errorList.append("id")
        }
        
        if let name = data["name"] as? String {
            self.name = name
        } else {
            errorList.append("name")
        }
        
        if let category = data["category"] as? String {
            self.category = category
        } else {
            errorList.append("category")
        }
        
        if let level = data["level"] as? String {
            self.level = level
        } else {
            errorList.append("dept_name")
        }
        
        if let creation_date = data["creation_date"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            let date = dateFormatter.date(from: creation_date)
            self.creation_date = DateManager.dateChinaTransformTimestamp(Int(date!.timeIntervalSince1970))!
        } else {
            errorList.append("creation_date")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("ProjectListModel fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}
