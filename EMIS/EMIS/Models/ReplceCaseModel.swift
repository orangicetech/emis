//
//  PeplceCaseModel.swift
//  EMIS
//
//  Created by Polowu on 2021/3/13.
//

import Foundation

struct ReplceCaseModel {
    var case_no: String = ""
    var receive_date: String = ""
    var request_person: String = ""
    var request_person_phone: String = ""
    var degree: String = ""
    var category: String = ""
    var description: String = ""
    var influence_scope: [ScopeModel] = []
    var location: MapInfo!
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["case_no"] = self.case_no
        if self.receive_date.count > 0 {
            dictionary["receive_date"] = self.receive_date
        }
        if self.request_person.count > 0 {
            dictionary["request_person"] = self.request_person
        }
        if self.request_person_phone.count > 0 {
            dictionary["request_person_phone"] = self.request_person_phone
        }
        if self.degree.count > 0 {
            dictionary["degree"] = self.degree
        }
        if self.category.count > 0 {
            dictionary["category"] = self.category
        }
        if self.description.count > 0 {
            dictionary["description"] = self.description
        }
        
        if influence_scope.count > 0 {
            var scope: [[String: Any]] = []
            self.influence_scope.forEach {
                scope.append($0.dictionary())
            }
            
            dictionary["influence_scope"] = scope
        }
        
        if location != nil {
            dictionary["location"] = location.dictionary()
        }
        return dictionary
    }
}
