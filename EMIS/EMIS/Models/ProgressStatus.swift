//
//  ProgressModel.swift
//  EMIS
//
//  Created by Polowu on 2021/3/13.
//

import UIKit

class ProgressStatus: NSObject {
    var value: Double = 0
    var type: FileType = .Defaults
}
