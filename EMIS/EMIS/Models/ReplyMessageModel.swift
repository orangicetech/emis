//
//  MessageModel.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/31.
//

import Foundation

struct ReplyMessageModel {
    var cdate: String = ""
    var msg: String = ""
    var from: Int = 0
    var fromname: String = ""
    var sno: Int = 0
    var pic: [String] = []
    var video: [String] = []

    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
                
        if let from = data["from"] as? String {
            self.from = Int(from)!
        } else {
            errorList.append("from")
        }

        if let sno = data["sno"] as? Int {
            self.sno = sno
        } else {
            errorList.append("sno")
        }
        
        if let cdate = data["cdate"] as? String {
            self.cdate = cdate
        } else {
            errorList.append("cdate")
        }
        
        if let msg = data["msg"] as? String {
            self.msg = msg
        } else {
            errorList.append("msg")
        }
        
        if let fromname = data["fromname"] as? String {
            self.fromname = fromname
        } else {
            errorList.append("fromname")
        }

        if let pic = data["pic"] as? [String] {
            self.pic = pic
        } else {
            errorList.append("pic")
        }
        
        if let video = data["video"] as? [String] {
            self.video = video
        } else {
            errorList.append("video")
        }
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("ReplyMessageModel(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
}


