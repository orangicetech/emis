//
//  AddCasualtyDateModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/8.
//

import Foundation

struct AddCasualtyModel {
    var case_no: String = ""
    var casualties_list: [CasualtyModel] = []
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["case_no"] = self.case_no

        var casualties_list: [[String: Any]] = []
        self.casualties_list.forEach {
            casualties_list.append($0.dictionary())
        }
        
        dictionary["casualties_list"] = casualties_list
        return dictionary
    }
}
