//
//  UploadModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/19.
//

import Foundation
enum UploadType: Int {
    case Waiting
    case Uploading
}

struct UploadModel {
    var projectId: String = ""
    var no: String = ""
    var category: String = ""
    var receive_date: String = ""
    var location_address: String = ""
    var location_district: String = ""
    var situation: String = ""
    var replyModel: ReplyModel = ReplyModel()
    var imageList: [FileModel] = [FileModel()]
    var videoList: [FileModel] = [FileModel()]
    var type: UploadType = .Waiting
    var casualtyOldList: [CasualtyModel] = []
    var casualtyList: [CasualtyModel] = []
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["projectId"] = self.projectId
        dictionary["no"] = self.no
        dictionary["category"] = self.category
        dictionary["receive_date"] = self.receive_date
        dictionary["location_address"] = self.location_address
        dictionary["location_district"] = self.location_district
        dictionary["situation"] = self.situation
        dictionary["replyModel"] = self.replyModel.dictionary()

        var list: [[String: Any]] = []
        self.imageList.forEach {
            let data = $0.dictionary()
            list.append(data)
        }
        dictionary["imageList"] = list
        
        var videoList: [[String: Any]] = []
        self.videoList.forEach {
            let data = $0.dictionary()
            videoList.append(data)
        }
        dictionary["videoList"] = videoList
        
        var casualtyOldList: [[String: Any]] = []
        self.casualtyOldList.forEach {
            let data = $0.dictionary()
            casualtyOldList.append(data)
        }
        dictionary["casualtyOldList"] = casualtyOldList
        
        var casualtyList: [[String: Any]] = []
        self.casualtyList.forEach {
            let data = $0.dictionary()
            casualtyList.append(data)
        }
        dictionary["casualtyList"] = casualtyList

        return dictionary
    }
    
    static func conver(data: [String: Any]) -> UploadModel{
        var model = UploadModel()
        if let projectId = data["projectId"] as? String {
            model.projectId = projectId
        }
        
        if let no = data["no"] as? String {
            model.no = no
        }
        
        if let category = data["category"] as? String {
            model.category = category
        }
        
        if let receive_date = data["receive_date"] as? String {
            model.receive_date = receive_date
        }
        
        if let location_address = data["location_address"] as? String {
            model.location_address = location_address
        }
        
        if let location_district = data["location_district"] as? String {
            model.location_district = location_district
        }
        
        if let situation = data["situation"] as? String {
            model.situation = situation
        }
        
        if let replyModel = data["replyModel"] as? [String: Any] {
            model.replyModel = ReplyModel.conver(data: replyModel)
        }

        if let type = data["type"] as? Int {
            model.type = UploadType(rawValue: type) ?? .Waiting
        }
        
        if let imageList = data["imageList"] as? [[String: Any]] {
            var list: [FileModel] = []
            imageList.forEach {
                list.append(FileModel.conver(data: $0))
            }
            model.imageList = list
        }
        
        if let videoList = data["videoList"] as? [[String: Any]] {
            var list: [FileModel] = []
            videoList.forEach {
                list.append(FileModel.conver(data: $0))
            }
            model.videoList = list
        }
        
        if let oldList = data["casualtyOldList"] as? [[String: Any]] {
            var casualtyOldList: [CasualtyModel] = []
            oldList.forEach {
                casualtyOldList.append(CasualtyModel(data: $0))
            }
            model.casualtyOldList = casualtyOldList
        }
        
        if let list = data["casualtyList"] as? [[String: Any]] {
            var casualtyList: [CasualtyModel] = []
            list.forEach {
                casualtyList.append(CasualtyModel(data: $0))
            }
            model.casualtyList = casualtyList
        }
        
        return model
    }
}
