//
//  LocationModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

import UIKit
import CoreLocation
class LocationModel: NSObject {
    var latitude: CLLocationDegrees = 0
    var longitude: CLLocationDegrees = 0
    
    public init(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.latitude = latitude
        self.longitude = longitude
    }
}
