//
//  ReplyModel.swift
//  EMIS
//
//  Created by Polowu on 2021/3/13.
//

import Foundation

struct ReplyModel {
    var case_no: String = ""
    var case_closed: Bool = false
    var comment: String = ""
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["case_no"] = self.case_no
        dictionary["case_closed"] = self.case_closed
        if self.comment.count > 0 {
            dictionary["comment"] = self.comment
        }
        return dictionary
    }
    
    static func conver(data: [String: Any]) -> ReplyModel{
        var model = ReplyModel()
        if let case_no = data["case_no"] as? String {
            model.case_no = case_no
        }
        if let case_closed = data["case_closed"] as? Bool {
            model.case_closed = case_closed
        }
        if let comment = data["comment"] as? String {
            model.comment = comment
        }
        
        return model
    }
}
