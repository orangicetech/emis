//
//  imageModel.swift
//  EMIS
//
//  Created by Polowu on 2021/3/7.
//

enum FileType: Int {
    case Defaults
    case Image
    case Video
}
import UIKit

struct FileModel {
    var type: FileType = .Defaults
    var image: UIImage!
    var url: URL!
    
    static func conver(data: [String: Any]) -> FileModel{
        var model = FileModel()
        if let type = data["type"] as? Int {
            model.type = FileType(rawValue: type) ?? .Defaults
        }
        
        if let url = data["url"] as? String {
            model.url = URL(string: url)
        }
        
        return model
    }
    
    public func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["type"] = self.type.rawValue
        dictionary["url"] = self.url.absoluteString
        return dictionary
    }
}
