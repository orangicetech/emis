//
//  StringExtension.swift
//  EMIS
//
//  Created by Polo iMac on 2021/4/26.
//

import Foundation

extension String {
    // 是否有注音
    func isContainsPhoneticCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if (scalar.value >= 12549 && scalar.value <= 12582) || (scalar.value == 12584 || scalar.value == 12585 || scalar.value == 19968) {
                return true
            }
        }
        return false
    }
    
    // 是否有中文字
    func isContainsChineseCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if scalar.value >= 19968 && scalar.value <= 171941 {
                return true
            }
        }
        return false
    }
    
    // 是否有空白字元
    func isContainsSpaceCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if scalar.value == 32 {
                return true
            }
        }
        return false
    }
        
    /// 是否包含特殊符號
    func hasSpecialCharacter() -> Bool {
        let symbolsParts = self.components(separatedBy: .symbols)
        let punctuationCharactersParts = self.components(separatedBy: .punctuationCharacters)
        return (symbolsParts.count > 1 || punctuationCharactersParts.count > 1)
    }
    
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }

    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

