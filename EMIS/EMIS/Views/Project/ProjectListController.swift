//
//  ProjectListController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/26.
//

import UIKit



class ProjectListController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var notifyButton: UIButton!
    
    @IBOutlet weak var messageButton: UIButton!
    
    @IBOutlet weak var offlineButton: UIButton!
    lazy var manager: EMISManager = {
        return EMISManager.shared
    }()
    
    var data: [ProjectListModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        EMISManager.shared.offlineMode = false
        if (EMISManager.shared.replyno != -1) {
            ViewRouter.shared.showMessageListFrom(viewController: self)
        } else if (EMISManager.shared.pushno != -1) {
            ViewRouter.shared.showPushMessageListFrom(viewController: self)
        } else {
            LoadingHUD.shared.show()
            EMISManager.shared.projectList { [weak self] (datas) in
                guard let self = self else { return }
                self.data = datas
                self.tableView.reloadData()
                LoadingHUD.shared.dismiss()
            }
            
            
            var isPushUnread = false
            EMISManager.shared.pushType { [weak self] (datas) in
                guard let self = self else { return }
                var typeId : [String] = []
                
                for data in datas {
                    typeId.append(String(data.id))
                }
                
                let allId = typeId.joined(separator:",")
                
                EMISManager.shared.pushLog(no: "", typeid: allId) { [weak self] (datas) in
                    guard let self = self else { return }
                    for data in datas {
                        if !(data.read) {
                            isPushUnread = true
                            break
                        }
                    }
                    self.notifyButton.imageView?.image = isPushUnread ? UIImage(named: "ic_notice_a") : UIImage(named: "ic_notice_n")
                }
            }
            
            var isMessageUnread = false
            EMISManager.shared.messageLog(no: "") { [weak self] (datas) in
                guard let self = self else { return }
                for data in datas {
                    if !(data.read) {
                        isMessageUnread = true
                        break
                    }
                }
                self.messageButton.imageView?.image = isMessageUnread ? UIImage(named: "ic_message_a") : UIImage(named: "ic_message_n")
            }
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        ViewRouter.shared.presentWarningFrom(viewController: self, title:"提示訊息", message: "確認是否登出系統?", type: .YesAndNo) { (buttonEvent) in
            if buttonEvent == .confim {

                LoadingHUD.shared.show()
                EMISManager.shared.logout { (responseModel) in
                    LoadingHUD.shared.dismiss()
                    if responseModel.status {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        AlertManager.shared.showMessage(message: responseModel.message, title: "")
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
        

    }
    
    @IBAction func onClickOffline(_ sender: Any) {
        EMISManager.shared.offlineMode = true
        ViewRouter.shared.showOfflineCaseListFrom(viewController: self)
    }
    
    @IBAction func onClickNotify(_ sender: Any) {
        ViewRouter.shared.showPushMessageListFrom(viewController: self)
    }
    
    @IBAction func onClickMessage(_ sender: Any) {
        ViewRouter.shared.showMessageListFrom(viewController: self)
    }
}

//MARK: - Observable handling
extension ProjectListController {
    func addObservable() {
        manager.isLogin.addObserver(fireNow: false) { (isLogin) in
            if !isLogin {
                self.dismiss(animated: false) {
                    LoadingHUD.shared.dismiss()
                }
            }
        }
    }
    
}

// MARK: - UITableViewDataSource
extension ProjectListController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProjectListCell = tableView.dequeueReusableCell(withIdentifier: "ProjectListCell", for: indexPath) as! ProjectListCell
        cell.configureCellForModel(model: data[indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ProjectListController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ViewRouter.shared.showFunctionFrom(viewController: self, model: data[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ProjectListCell {
            cell.isButtonHighlighted = true
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ProjectListCell {
            cell.isButtonHighlighted = false
        }
    }
}
