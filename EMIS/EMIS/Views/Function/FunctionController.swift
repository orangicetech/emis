//
//  FunctionController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/3.
//

import UIKit
enum FilterType: Int {
    case district = 0
    case category
    case status
}
class FunctionController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var projectView: CustomView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var renkView: CustomView!
    @IBOutlet weak var renkLabel: UILabel!
    @IBOutlet weak var categoryView: CustomView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var buttonImageView: UIImageView!
    @IBOutlet weak var buttonLabel: UILabel!
    @IBOutlet weak var searchTextField: CustomTextField!
    
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    
    @IBOutlet weak var caseTableView: UITableView!
    @IBOutlet weak var newButton: CustomButton!
    @IBOutlet weak var changeButton: CustomButton!
    
    var projectModel: ProjectListModel!
    
    var filterType: FilterType = .district
    
    var filterCategory: CategoryModel = CategoryModel(data: ["category": "全部"])
    var filterＤistrict: DistrictModel = DistrictModel(data: ["text": "全部"])
    
    var categoryList: [CategoryModel] = []
    var districtList: [DistrictModel] = []
    
    var selectedIndex: Int = 0

    lazy var manager: EMISManager = {
        return EMISManager.shared
    }()
    
    var search_caseList: [CaseListModel] = []
    
    var caseList: [CaseListModel] = [] {
        didSet {
            search_caseList = caseList
            caseTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
        EMISManager.shared.categoryList { [weak self] (datas, injurys, disasters) in
            guard let self = self else { return }
            self.categoryList = datas
            self.categoryList.insert(CategoryModel(data: ["category": "全部"]), at: 0)
        }
        EMISManager.shared.districtList { [weak self] (datas) in
            guard let self = self else { return }
            self.districtList = datas
            self.districtList.insert(DistrictModel(data: ["text": "全部"]), at: 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LoadingHUD.shared.show()
        setupInfo()
        manager.caseList(project_id: projectModel.id) { [weak self] (models) in
            guard let self = self else { return }
            self.caseList = models
            self.search_caseList = self.caseList
            LoadingHUD.shared.dismiss()
        }
        setupButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchTextField.addHandler(hasChinese: true, long: 30)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        changeButton.buttonHighlighted = nil
        newButton.buttonHighlighted = nil
    }
    
    //MARK: - Init
    func initLayout() {
        setupInfo()
        setupTextField(districtTextField, placeholderColor: UIColor.white)
        setupTextField(categoryTextField, placeholderColor: UIColor.white)
        searchTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    private func setupButtons() {
        newButton.buttonHighlighted = { [weak self] (isHighlighted) in
            guard let self = self else { return }
            self.newButton.backgroundColor = isHighlighted ? UIColor(named: "AlgaeGreenColor_a") : UIColor(named: "AlgaeGreenColor")
        }
        
        changeButton.buttonHighlighted = { [weak self] (isHighlighted) in
            guard let self = self else { return }
            self.changeButton.setImage(isHighlighted ? UIImage(named: "list_btn_go_bg_a") : UIImage(named: "list_btn_go_bg_n"), for: .normal)
            self.buttonLabel.textColor = isHighlighted ? UIColor(named: "TextHighlightedColor") : UIColor.white
            
        }
    }
    
    private func setupInfo() {
        userNameLabel.text = "\(manager.userModel.value.dept_name) \(manager.userModel.value.user_name)"
        titleLabel.text = projectModel.name
        dateTimeLabel.text = projectModel.creation_date
        renkLabel.text = projectModel.level
        categoryLabel.text = projectModel.category
        dateTimeLabel.text = "成立時間：\(projectModel.creation_date)"
        searchTextField.attributedPlaceholder = NSAttributedString(string: "請輸入地址進行搜尋", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.darkGray])
        
    }
    
    private func setupTextField(_ textField: UITextField, placeholderColor: UIColor) {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        textField.inputView = picker
        textField.delegate = self
        
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
    }
    
    //MARK: - IBAction
    @IBAction func onClickFilter(_ sender: Any) {
        let btn = sender as! UIButton
        filterType = FilterType(rawValue: btn.tag)!
        switch filterType {
        case .district:
            districtTextField.becomeFirstResponder()
        case .category:
            categoryTextField.becomeFirstResponder()
        case .status:
            break
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickLogout(_ sender: Any) {
        LoadingHUD.shared.show()
        EMISManager.shared.logout { (responseModel) in
            LoadingHUD.shared.dismiss()
            if responseModel.status {
                self.dismiss(animated: true, completion: nil)
            } else {
                AlertManager.shared.showMessage(message: responseModel.message, title: "")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onClickNewCase(_ sender: Any) {
        ViewRouter.shared.showNewCaseFrom(viewController: self, model: projectModel)
    }
    
    @IBAction func onClickCloneAll(_ sender: Any) {
        searchTextField.text = ""
        autocCmplete()
    }
    
    @IBAction func onClickLive(_ sender: Any) {
        UIApplication.tryURL(urls: [
                        "fb://groups/426241551840753", // App
                        "https://www.facebook.com/groups/426241551840753" // Website if app fails
                        ])
    }
    
    @IBAction func onClickUploadList(_ sender: Any) {
        ViewRouter.shared.showUploadListFrom(viewController: self, projectModel: projectModel)
    }
    
    //MARK: - Private
    @objc func textFieldDidChange(_ textField: UITextField) {
        autocCmplete()
    }
    
    
    func autocCmplete() {
        var districtData: [CaseListModel] = []
        if filterＤistrict.text == "全部" {
            districtData = self.caseList
        } else {
            for caseModel in self.caseList {
                if caseModel.location_district == filterＤistrict.text {
                    districtData.append(caseModel)
                }
            }
        }

        var categoryData: [CaseListModel] = []
        if filterCategory.category == "全部" {
            categoryData = districtData
        } else {
            for caseModel in districtData {
                if caseModel.category == filterCategory.category {
                    categoryData.append(caseModel)
                }
            }
        }
        
        let searchText = searchTextField.text!.uppercased(with: NSLocale.current)
        if search_caseList.count > 0 {
            search_caseList.removeAll()
        }
        
        if searchText.count > 0 {
            for caseModel in categoryData {
                if caseModel.location_address.contains(searchText) {
                    search_caseList.append(caseModel)
                }
            }
        } else {
            search_caseList.append(contentsOf: categoryData)
        }
        
        caseTableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension FunctionController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return search_caseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let caseListCell: CaseListCell = tableView.dequeueReusableCell(withIdentifier: "CaseListCell", for: indexPath) as! CaseListCell
        let model = search_caseList[indexPath.row]
        caseListCell.noLabel.text = model.no
        caseListCell.categoryLabel.text = model.category
        caseListCell.dateLabel.text = model.receive_date
        caseListCell.addressLabel.text = model.location_district + model.location_address
        caseListCell.status = model.status
        return caseListCell
    }
    
    
}

extension FunctionController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 156
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = search_caseList[indexPath.row]
        ViewRouter.shared.showCaseDetalFrom(viewController: self, model: model, projectModel: projectModel)
    }
}

//MARK: - UIPickerView Delegate
extension FunctionController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch filterType {
        case .district:
            return districtList.count
        case .category:
            return categoryList.count
        case .status:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch filterType {
        case .district:
            let text = districtList[row].text
            return text
        case .category:
            let text = categoryList[row].category
            return text
        case .status:
            return ""
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedIndex = row
        switch filterType {
        case .district:
            filterＤistrict = districtList[row]
            let text = districtList[row].text
            districtTextField.text = text
        case .category:
            filterCategory = categoryList[row]
            let text = categoryList[row].category
            categoryTextField.text = text
        case .status:
            break
        }
        autocCmplete()
    }
}
