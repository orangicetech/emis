//
//  MessageContentViewController.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/31.
//

import UIKit
import AVFoundation

class MessageContentViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var model: PushLogModel!
//    var replyno: Int = 0
    var models: [ReplyMessageModel] = []
    let imagePicker = UIImagePickerController()
//    var videoMaxDuration = 300
    var videoMaxSize = 10 * 1024 * 1024

    var imageList: [FileModel] = []
    var videoList: [FileModel] = []
    var imageMaxNum = 3

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.backgroundColor = UIColor.white
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        
        imagePicker.delegate = self
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        EMISManager.shared.readPush(no: String(model.no)) { (responseModel) in
        }
    }
    
    func updateUI() {
        EMISManager.shared.pushGetReply(replyno: String(model.replyno)) { [weak self] (datas) in
            guard let self = self else { return }
            self.models.removeAll()
            for data in datas {
                self.models.append(data)
            }
            self.imageList.removeAll()
            self.videoList.removeAll()
            self.tableView.reloadData()
        }
    }
    
    func addMedia(_ image: UIImage, isVideo: Bool, url: URL?) {
        if isVideo {
            var model = FileModel()
            model.type = .Video
            model.image = image
            model.url = url
            videoList.removeAll()
            videoList.insert(model, at: 0)
            tableView.reloadData()
        } else {
            var model = FileModel()
            model.type = .Image
            model.image = image
            model.url = url
            if imageList.count ==  imageMaxNum {
                imageList.remove(at: imageMaxNum - 1)
            }
            imageList.insert(model, at: 0)
            tableView.reloadData()
        }
    }
    
    func deleteMedia(index: Int, isVideo: Bool) {
        if isVideo {
//            if videoList.count == 1 {
//                if videoList.last?.type != .Defaults {
//                    videoList.append(FileModel())
//                }
//            }
            // cuz video list only 1
            self.videoList.remove(at: 0)
            self.tableView.reloadData()
        } else {
//            if imageList.count == imageMaxNum {
//                if imageList.last?.type != .Defaults {
//                    imageList.append(FileModel())
//                }
//            }
            self.imageList.remove(at: index)
            self.tableView.reloadData()
        }
    }
    
    func showPickPhoto() {
        let alertController = UIAlertController(
            title: "",
            message: "請選擇圖片...",
            preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(
            title: "取消",
            style: .cancel,
            handler: nil)
        
        alertController.addAction(cancelAction)
        let cameraAction = UIAlertAction(title: "拍照", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(cameraAction)
        
        let photoAction = UIAlertAction(title: "從相簿挑選", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(photoAction)
        self.present( alertController, animated: true, completion: nil)
    }
    
    func showPickVideo() {
        
        let alertController = UIAlertController(
            title: "",
            message: "請選擇影片...",
            preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(
            title: "取消",
            style: .cancel,
            handler: nil)
        
        alertController.addAction(cancelAction)
        let cameraAction = UIAlertAction(title: "拍攝", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.mediaTypes = ["public.movie"]
//            self.imagePicker.videoMaximumDuration = TimeInterval(self.videoMaxDuration)
            self.imagePicker.videoQuality = .typeMedium
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(cameraAction)
        
        let photoAction = UIAlertAction(title: "從相簿挑選", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = ["public.movie"]
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(photoAction)
        self.present( alertController, animated: true, completion: nil)
        
    }
    
    
    func uploadMessage(text: String) {
        LoadingHUD.shared.show()

        EMISManager.shared.pushReply(replyno: String(model.replyno), msg: text) { [weak self] (data) in
            guard let self = self else { return }
            var imageFinished = false
            var videoFinished = false
            
            if (self.imageList.isEmpty) {
                imageFinished = true
            } else {
                EMISManager.shared.pushUploadImages(sno: String(data.sno), images: self.imageList) { [weak self] (data) in
                    guard let self = self else { return }
                    imageFinished = true
                    if (imageFinished && videoFinished) {
                        LoadingHUD.shared.dismiss()
                        self.updateUI()
                    }
                }
            }
   
            if (self.videoList.isEmpty) {
                videoFinished = true
            } else {
                EMISManager.shared.pushUploadVideos(sno: String(data.sno), pic_sno: "4", videos: self.videoList) { [weak self] (data) in
                    guard let self = self else { return }
                    videoFinished = true
                    if (imageFinished && videoFinished) {
                        LoadingHUD.shared.dismiss()
                        self.updateUI()
                    }
                }
            }
            
            if (imageFinished && videoFinished) {
                LoadingHUD.shared.dismiss()
                self.updateUI()
            }
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onClickedAddMedia(sender:UIButton) {
        if (sender.tag < imageMaxNum) {
            self.showPickPhoto()
        } else {
            self.showPickVideo()
        }
    }

    @objc func onClickedDelMedia(sender:UIButton) {
        if (sender.tag < imageMaxNum) {
            self.deleteMedia(index: sender.tag, isVideo: false)
        } else {
            self.deleteMedia(index: sender.tag, isVideo: true)
        }
    }
    
}

// MARK: - UITableViewDataSource
extension MessageContentViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == models.count) {
            let cell: MessageReplyCell = tableView.dequeueReusableCell(withIdentifier: "MessageReplyCell", for: indexPath) as! MessageReplyCell

            cell.imageList = imageList
            cell.videoList = videoList
            
            cell.updateUI()
            
            cell.addPicButton0.addTarget(self, action:#selector(onClickedAddMedia), for: .touchUpInside)
            cell.delPicButton0.addTarget(self, action:#selector(onClickedDelMedia), for: .touchUpInside)
            cell.addPicButton1.addTarget(self, action:#selector(onClickedAddMedia), for: .touchUpInside)
            cell.delPicButton1.addTarget(self, action:#selector(onClickedDelMedia), for: .touchUpInside)
            cell.addPicButton2.addTarget(self, action:#selector(onClickedAddMedia), for: .touchUpInside)
            cell.delPicButton2.addTarget(self, action:#selector(onClickedDelMedia), for: .touchUpInside)
            cell.addVideoButton0.addTarget(self, action:#selector(onClickedAddMedia), for: .touchUpInside)
            cell.delVideoButton0.addTarget(self, action:#selector(onClickedDelMedia), for: .touchUpInside)


            cell.pickMediaHandle = { [weak self] (num) in
                guard let self = self else { return }
                self.showPickPhoto()
            }
            
            cell.confirmHandle = {
                
                if (cell.replyTextView.text == "") {
                    ViewRouter.shared.presentWarningFrom(viewController: self, title:"錯誤訊息", message: "請填寫留言內容", type: .Alert) { (buttonEvent) in
                        print("請填寫留言內容")
                    }
                } else {
                    self.uploadMessage(text: cell.replyTextView.text)
                    cell.replyTextView.text = ""
                }
            }
            
            return cell
        } else {
            let cell: MessageContentCell = tableView.dequeueReusableCell(withIdentifier: "MessageContentCell", for: indexPath) as! MessageContentCell
            let model = models[indexPath.row]
            cell.setMsgContent(data: model)
            return cell
        }
    }
}

// MARK: - UITableViewDelegate
extension MessageContentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == models.count) {
            return 520
        } else {
            return UITableView.automaticDimension
        }
    }
}

extension MessageContentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String else {return}
        
        if mediaType == "public.movie" {
            guard let mediaURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL else { return }
            let asset = AVAsset(url: mediaURL)
            let file = AVURLAsset(url: mediaURL)
            // must call asset.duration, otherwise asset will be nil, Magic!
            print("影片長度: \(CMTimeGetSeconds(asset.duration)), 大小: \(file.fileSize)")
            
            if file.fileSize! < videoMaxSize {
                let url = MediaManager.shared.saveVideo(mediaURL: mediaURL)
                self.addMedia(self.generateThumbnail(for: asset)!, isVideo: true, url: url)
                //取得照片後將imagePickercontroller dismiss
                picker.dismiss(animated: true, completion: nil)
            } else {
                //取得照片後將imagePickercontroller dismiss
                picker.dismiss(animated: true, completion: nil)
                ViewRouter.shared.presentPromptFrom(viewController: self, message: "影片大小超過限制", type: .Alert) { (buttonEvent) in
                }
            }
        } else {
            let image: UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let url = MediaManager.shared.saveImage(image: image)
            self.addMedia(image, isVideo: false, url: url)
            //取得照片後將imagePickercontroller dismiss
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func generateThumbnail(for asset:AVAsset) -> UIImage? {
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 2)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        if img != nil {
            let frameImg  = UIImage(cgImage: img!)
            return frameImg
        }
        return nil
    }
}

extension AVURLAsset {
    var fileSize: Int? {
        let keys: Set<URLResourceKey> = [.totalFileSizeKey, .fileSizeKey]
        let resourceValues = try? url.resourceValues(forKeys: keys)

        return resourceValues?.fileSize ?? resourceValues?.totalFileSize
    }
}
