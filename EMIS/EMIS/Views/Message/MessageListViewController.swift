//
//  MessageListViewController.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/31.
//

import UIKit

class MessageListViewController: UIViewController {

    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var models: [PushLogModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.isHidden = true
        noDataLabel.isHidden = false
        tableView.backgroundColor = UIColor.white
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        EMISManager.shared.messageLog(no: "") { [weak self] (datas) in
            guard let self = self else { return }
            self.models.removeAll()
            if (datas.count > 0) {
                for data in datas {
                    self.models.append(data)
                }
                self.tableView.isHidden = false
                self.noDataLabel.isHidden = true
            } else {
                self.tableView.isHidden = true
                self.noDataLabel.isHidden = false
            }
            self.tableView.reloadData()
            LoadingHUD.shared.dismiss()
            
            if (EMISManager.shared.replyno != -1) {
                for model in self.models {
                    if (model.replyno == EMISManager.shared.replyno) {
                        EMISManager.shared.replyno = -1
                        ViewRouter.shared.showMessageContentFrom(viewController: self, model: model)
                        break
                    }
                }
            }
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource
extension MessageListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PushMessageCell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! PushMessageCell
        let model = models[indexPath.row]
        cell.titleLabel.text = model.title
        cell.contentLabel.text = model.message
        cell.dateLabel.text = model.cdate
        cell.isReaded = model.read
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MessageListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = models[indexPath.row]        
        ViewRouter.shared.showMessageContentFrom(viewController: self, model: model)
    }
}
