//
//  PromptController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/9.
//

import UIKit
enum PromptType: Int {
    case Confirm
    case Alert
    case YesAndNo
}

class PromptController: UIViewController {
    var confimHandle: ButtonConfimHandle!
    var message: String = ""
    var titleString: String = ""
    var type: PromptType = .Alert
    
    var force: Bool = false
    var hideClose: Bool = false
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cancelButtonView: UIView!
    @IBOutlet weak var confirmButton: CustomButton!
    
    @IBOutlet weak var closeButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        switch type {
        case .Alert:
            confirmButton.setTitle("了解", for: .normal)
            cancelButtonView.isHidden = true
        case .Confirm:
            confirmButton.setTitle("確認", for: .normal)
            cancelButtonView.isHidden = false
        case .YesAndNo:
            confirmButton.setTitle("是", for: .normal)
            cancelButtonView.isHidden = false
        }
        
        if (titleString != "") {
            titleLabel.text = titleString
        }
        messageLabel.text = message
        
        closeButton.isHidden = hideClose
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        if (force) {
            self.confimHandle?(.cancel)
        } else {
            dismiss(animated: false) {
                self.confimHandle?(.cancel)
            }
        }
    }
    
    @IBAction func onClickOK(_ sender: Any) {
        if (force) {
            self.confimHandle?(.confim)
        } else {
            dismiss(animated: false) {
                self.confimHandle?(.confim)
            }
        }
    }
    
}
