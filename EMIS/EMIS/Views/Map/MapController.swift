//
//  MapController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/24.
//

import UIKit
import GoogleMaps
import CoreLocation

struct MapInfo:Codable {
    var district: String = ""
    var address: String = ""
    var latitude: CLLocationDegrees = 0
    var longitude: CLLocationDegrees = 0
    var district_id: String = ""
    
    public var isValid = false
    public var errorList: [String] = []
    
    public init(data: [String: Any]) {
        
        if let district = data["district"] as? String {
            self.district = district
        } else {
            errorList.append("district")
        }
        
        if let address = data["address"] as? String {
            self.address = address
        } else {
            errorList.append("address")
        }
        
        if let latitude = data["latitude"] as? CLLocationDegrees {
            self.latitude = latitude
        } else {
            errorList.append("latitude")
        }
        
        
        if let longitude = data["longitude"] as? CLLocationDegrees {
            self.longitude = longitude
        } else {
            errorList.append("longitude")
        }
        
        if let district_id = data["district_id"] as? String {
            self.district_id = district_id
        } else {
            errorList.append("district_id")
        }
        
        if errorList.count > 0 {
            if data.keys.count > 0 {
                log.warning("MapInfo(\(data) fail to retrieve fields: \(errorList)")
            }
        } else {
            isValid = true
        }
    }
    
    func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["district"] = self.district
        dictionary["address"] = self.address
        dictionary["latitude"] = self.latitude
        dictionary["longitude"] = self.longitude
        dictionary["district_id"] = self.district_id
        return dictionary
    }
}

class MapController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var latTextFiled: CustomTextField!
    @IBOutlet weak var longTextFiled: CustomTextField!
    @IBOutlet weak var districtTextFiled: UITextField!
    @IBOutlet weak var addressTextFiled: CustomTextField!

    public var confimHandle: MapInfoConfimHandle!
    public var mapInfo: MapInfo!
    private var camera: GMSCameraPosition!
    private var marker: GMSMarker!
    private var latitude: CLLocationDegrees = 0
    private var longitude: CLLocationDegrees = 0
    
    private var districtList: [DistrictModel] = []
    private var selectedDistrictModel: DistrictModel!
    
    lazy var mapManager: MapManager = {
        return MapManager.shared
    }()
    
    lazy var manager: EMISManager = {
        return EMISManager.shared
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadingHUD.shared.show()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupMapView()
        setupDistrictTextField()
        addressTextFiled.addHandler(hasChinese: true, long: 30)
        latTextFiled.addHandler(hasChinese: false, long: 15)
        longTextFiled.addHandler(hasChinese: false, long: 15)
        LoadingHUD.shared.dismiss()
    }
    
    // MARK: - Private
    func setupDistrictTextField() {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        districtTextFiled.inputView = picker
        districtTextFiled.delegate = self
        addressTextFiled.attributedPlaceholder = NSAttributedString(string: "請輸入地址", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.darkGray])
    }
    
    private func setupMapView() {
        if mapInfo != nil {
            latitude = mapInfo.latitude
            longitude = mapInfo.longitude
            addressTextFiled.text = mapInfo.address
            manager.districtList { [weak self] (districtList) in
                guard let self = self else { return }
                self.districtList = districtList
                districtList.forEach {
                    if $0.text == self.mapInfo.district {
                        self.selectedDistrictModel = $0
                        self.districtTextFiled.text = self.mapInfo.district
                    }
                }
            }
        } else {
            latitude = manager.locationModel.value.latitude
            longitude = manager.locationModel.value.longitude
            mapManager.convertLocationToDistrict(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude)) { (district) in
                self.manager.districtList { [weak self] (districtList) in
                    guard let self = self else { return }
                    self.districtList = districtList
                    districtList.forEach {
                        if $0.text == district {
                            self.selectedDistrictModel = $0
                            self.districtTextFiled.text = district
                        }
                    }
                }
            }
        }
        
        updateCoordinateTextField()
        
        camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15)
        mapView.camera = camera
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        mapView.delegate = self
        
        marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.map = mapView
    }
    
    private func updateCoordinateTextField() {
        latTextFiled.text = "\(latitude)"
        longTextFiled.text = "\(longitude)"
    }
    
    func verifi() -> Bool {
        var verifi = true
        if districtTextFiled.text!.count == 0 {
            verifi = false
            AlertManager.shared.showMessage(message: "請選擇地區", title: "")
        } else if addressTextFiled.text!.count == 0 {
            verifi = false
            AlertManager.shared.showMessage(message: "請輸入地址", title: "")
        } else if latTextFiled.text!.count == 0 {
            verifi = false
            AlertManager.shared.showMessage(message: "請輸入Ｘ座標", title: "")
        } else if longTextFiled.text!.count == 0 {
            verifi = false
            AlertManager.shared.showMessage(message: "請輸入Ｙ座標", title: "")
        }
        
        return verifi
    }
    
    // MARK: - IBAction
    @IBAction func onClickConfim(_ sender: Any) {
        if verifi() {
            var info = MapInfo(data: [:])
            info.district = districtTextFiled.text!
            info.address = addressTextFiled.text!
            info.latitude = latitude
            info.longitude = longitude
            info.district_id = selectedDistrictModel.id
            confimHandle?(true, info)
            dismiss(animated: true) {
                
            }
        }
    }
    
    @IBAction func onClickClose(_ sender: Any) {
        confimHandle?(false, nil)
        dismiss(animated: true) {
            
        }
    }
    
    @IBAction func onClickDistrict(_ sender: Any) {
        districtTextFiled.becomeFirstResponder()
    }
    
    @IBAction func onClickLocation(_ sender: Any) {
        mapManager.convertAddressToLocation("\(districtTextFiled.text!)\(addressTextFiled!)") { (lat, lng, city) in
            self.latitude = CLLocationDegrees(lat)
            self.longitude = CLLocationDegrees(lng)
            self.updateCoordinateTextField()
            self.marker.position =  CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
            self.mapView.animate(toLocation:  CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude))
            
            self.districtList.forEach {
                if $0.text == city {
                    self.selectedDistrictModel = $0
                    self.districtTextFiled.text = city
                }
            }
        }
        
//        latitude = manager.locationModel.value.latitude
//        longitude = manager.locationModel.value.longitude
//        updateCoordinateTextField()
//        marker.position =  CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//        mapView.animate(toLocation:  CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
    }
}

extension MapController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
//        latitude = coordinate.latitude
//        longitude = coordinate.longitude
//        
//        marker.position = coordinate
//        mapView.animate(toLocation: coordinate)
//        
//        updateCoordinateTextField()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        latitude = position.target.latitude
        longitude = position.target.longitude
        marker.position = position.target
        updateCoordinateTextField()
    }
}

extension MapController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return districtList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return districtList[row].text
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if districtList.count > 0 {
            selectedDistrictModel = districtList[row]
            districtTextFiled.text = districtList[row].text
        }
    }
}

extension MapController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            if districtList.count > 0 {
                selectedDistrictModel = districtList.first
                districtTextFiled.text = districtList.first?.text
            }
        }
    }
}
