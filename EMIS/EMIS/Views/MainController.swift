//
//  MainController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/6.
//

import UIKit
import SwiftyGif
import FirebaseCrashlytics

class MainController: UIViewController {
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var cloudImage: UIImageView!
    @IBOutlet weak var cityImage: UIImageView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var accountTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var logoCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var cloudTopConstraint: NSLayoutConstraint!
    @IBOutlet var launchConstraints: [NSLayoutConstraint]!
    @IBOutlet var loginConstraints: [NSLayoutConstraint]!
    @IBOutlet weak var infoView: UIStackView!
    
    @IBOutlet weak var rememberImage: UIImageView!
    @IBOutlet weak var rememberButton: UIButton!
    @IBOutlet weak var loginButton: CustomButton!
    
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var offlineView: UIView!
    
    
    lazy var manager: EMISManager = {
        return EMISManager.shared
    }()
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        accountTextField.attributedPlaceholder = NSAttributedString(string: "帳號", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.darkGray])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "密碼", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.darkGray])
        startAnimating()
        
        MapManager.shared.startUpdatingLocation()
        addObservable()
        rememberImage.isHidden = !UserManager.shared.isRemember()
        rememberButton.isSelected = UserManager.shared.isRemember()
        accountTextField.text = UserManager.shared.account()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        infoView.isHidden = true
        offlineView.isHidden = true
        loginLabel.isHidden = true
        setupButtons()
        passwordTextField.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        accountTextField.addHandler(hasChinese: false, long: 20)
        passwordTextField.addHandler(hasChinese: false, long: 25)
        passwordTextField.hasAllSpecial = true
        
        let status = EMISManager.shared.checkUpdate()
        
        if (status == .Update) {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let promptController = storyBoard.instantiateViewController(withIdentifier: "PromptController") as! PromptController
            promptController.modalPresentationStyle = .overCurrentContext
            promptController.modalTransitionStyle = .crossDissolve
            promptController.titleString = "更新通知"
            promptController.message = "目前App有最新版本，請進行更新!"
            promptController.type = .Confirm
            promptController.hideClose = true
            promptController.confimHandle = { (buttonEvent) in
                if buttonEvent == .confim {
                    let url = EMISManager.shared.downloadUrl
                    if let url = URL(string: url), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    }
                }
            }

            self.present(promptController, animated: true, completion: nil)
            
        } else if (status == .ForceUpdate) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let promptController = storyBoard.instantiateViewController(withIdentifier: "PromptController") as! PromptController
            promptController.modalPresentationStyle = .overCurrentContext
            promptController.modalTransitionStyle = .crossDissolve
            promptController.titleString = "更新通知"
            promptController.message = "目前App有最新版本，請進行更新!"
            promptController.type = .Alert
            promptController.force = true
            promptController.hideClose = true
            promptController.confimHandle = { (buttonEvent) in
                let url = EMISManager.shared.downloadUrl
                if let url = URL(string: url), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
                
            }
            
            self.present(promptController, animated: true, completion: nil)

        }

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        loginButton.buttonHighlighted = nil
    }
    
    //MARK: - Private
    func startAnimating() {
        do {
            let gif = try UIImage(gifName: "emis_logo.gif")
            logoImage.setGifImage(gif, loopCount: 1)
            logoImage.delegate = self
            logoImage.startAnimating()
        } catch {
            log.error("Start animating error: \(error)")
        }
    }
    
    func changeToLoginScreen() {
        infoView.isHidden = false
        offlineView.isHidden = false
        loginLabel.isHidden = false
        
        launchConstraints.forEach {
            $0.priority = .defaultLow
        }
        loginConstraints.forEach {
            $0.priority = .required
        }
        let move = -(cloudImage.bounds.height - (view.bounds.height * 0.54))
        cloudTopConstraint.constant = move
        
        UIView.animate(withDuration: 0.3) {
            self.loginView.alpha = 1
            self.view.layoutIfNeeded()
        }
        
    }
    
    private func setupButtons() {
        loginButton.buttonHighlighted = { [weak self] (isHighlighted) in
            guard let self = self else { return }
            self.loginButton.setImage(isHighlighted ? UIImage(named: "login_btn_login_bg_a") : UIImage(named: "login_btn_login_bg_n"), for: .normal)
        }
        
    }
    
    func showProjectView() {
        EMISManager.shared.offlineMode = false
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            Crashlytics.crashlytics().setCustomValue(uuid, forKey: "device_id")
        }
        Crashlytics.crashlytics().setUserID(self.accountTextField.text!)
        
        if self.rememberButton.isSelected {
            UserManager.shared.saveAccount(self.accountTextField.text!)
        } else {
            UserManager.shared.saveAccount("")
        }
        
        ViewRouter.shared.presentToProjectListFrom(viewController: self)
    }
    //MARK: - IBAction
    @IBAction func onClickLogin(_ sender: Any) {
        LoadingHUD.shared.show()
        EMISManager.shared.login(username: accountTextField.text!, password: passwordTextField.text!) { (responseModel) in
            LoadingHUD.shared.dismiss()
            self.passwordTextField.text = ""
            if responseModel.status {
                self.showProjectView()
            } else {
                ViewRouter.shared.presentPromptFrom(viewController: self, message: responseModel.message, type: .Alert) { (buttonEvent) in
                    
                }
            }
        }
    }
    
    @IBAction func onClickPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: "https://eoc2.ntpc.gov.tw/EMIS_API/Policy.html"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                })
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func onClickAboutMe(_ sender: Any) {
        var message = "若有帳號登入或使用上的問題請撥打\n\n新北市政府消防局整備應變科\n\n(02)2960-3456#1883\n\n"
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        
        message += "版本 : v\(version)(\(build))\n\n"
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            message += "裝置ID : \(uuid)"
        }
        
        ViewRouter.shared.presentPromptFrom(viewController: self, message: message, type: .Alert) { (buttonEvent) in
            
        }
    }
    
    @IBAction func onClickRemember(_ sender: Any) {
        rememberButton.isSelected = !rememberButton.isSelected
        rememberImage.isHidden = !rememberButton.isSelected
        UserManager.shared.saveRemember(rememberButton.isSelected)
    }
    
    @IBAction func onClickOffline(_ sender: Any) {
        EMISManager.shared.offlineMode = true
        ViewRouter.shared.presentToOfflineCaseListFrom(viewController: self)
    }
    
    
}

extension MainController : SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        if manager.checkLogin() {
            showProjectView()
        } else {
            changeToLoginScreen()
        }
    }
}

//MARK: - Extension Observable
extension MainController {
    func addObservable() {
        manager.isLogin.addObserver(fireNow: false) { (isLogin) in
            if !isLogin {
                self.changeToLoginScreen()
            }
        }
        
    }
    
    func showTextFieldErrorWith(errorCode: TextFieldErrorCode) {
        var str = ""
        
        switch errorCode {
        case .chinese:
            str = "此欄位不支援中文"
        case .space:
            str = "此欄位不支援空白"
        case .special:
            str = "此欄位不支援特殊符號"
        case .tooLong:
            str = "輸入字數以超過上限"
        }
        
        ViewRouter.shared.presentPromptFrom(viewController: self, message: str, type: .Alert) { (buttonEvent) in
            
        }
    }
}
