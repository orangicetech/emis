//
//  PushMessageViewController.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/13.
//

import UIKit
import PagingKit

class PushMessageListViewController: UIViewController {

    var menuViewController: PagingMenuViewController!
    var contentViewController: PagingContentViewController!
    
    static var viewController: (UIColor) -> UIViewController = { (color) in
       let vc = UIViewController()
        vc.view.backgroundColor = color
        return vc
    }
        
    var typeId : [Int] = []
    var typeTitles : [String] = []
    var dataSource : [PushMessageTableViewController] = []
    var firstAppear = true

    static var sizingCell = TitleLabelMenuViewCell(frame: CGRect(x: 0, y: 0, width: 1, height: 1))

    override func viewDidLoad() {
        super.viewDidLoad()

        menuViewController?.register(type: TitleLabelMenuViewCell.self, forCellWithReuseIdentifier: "identifier")
        
        let focusView = UnderlineFocusView()
        focusView.underlineColor = UIColor(named: "MedBlueColor")!
        menuViewController?.registerFocusView(view: focusView)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LoadingHUD.shared.show()
        
        EMISManager.shared.pushType { [weak self] (datas) in
            guard let self = self else { return }
            LoadingHUD.shared.dismiss()
            
            if (self.firstAppear) {
                self.typeId.removeAll()
                self.typeTitles.removeAll()
                self.dataSource.removeAll()
                
                for data in datas {
                    self.typeId.append(data.id)
                    self.typeTitles.append(data.name)
                    let vc = PushMessageTableViewController.loadFromStoryboard()
                    vc.rootVC = self
                    vc.typeId = data.id
                    self.dataSource.append(vc)
                }
                self.firstAppear = false
            }
            
            self.menuViewController.reloadData()
            self.contentViewController.reloadData()
        }
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if let vc = segue.destination as? PagingMenuViewController {
                menuViewController = vc
                menuViewController.dataSource = self
                menuViewController.delegate = self
            } else if let vc = segue.destination as? PagingContentViewController {
                contentViewController = vc
                contentViewController.dataSource = self
                contentViewController.delegate = self
            }
        }
}

extension PushMessageListViewController: PagingMenuViewControllerDataSource {
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return dataSource.count
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        PushMessageListViewController.sizingCell.titleLabel.text = typeTitles[index]
        return self.view.frame.width / CGFloat(typeId.count)
    }
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "identifier", for: index)  as! TitleLabelMenuViewCell
        cell.titleLabel.text = typeTitles[index]
        cell.normalColor = UIColor(named: "104Color")!
        cell.focusColor = UIColor(named: "MedBlueColor")!
        return cell

    }
}

extension PushMessageListViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController.scroll(to: page, animated: true)
    }
}

extension PushMessageListViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return dataSource.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        return dataSource[index]
    }
}

extension PushMessageListViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController.scroll(index: index, percent: percent, animated: true)
    }
}
