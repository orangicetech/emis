//
//  PushMessageTableViewController.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/17.
//

import UIKit

class PushMessageTableViewController: UIViewController {
    
    static func loadFromStoryboard() -> PushMessageTableViewController {
        let storyboard = UIStoryboard(name: "PushMessage", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PushMessageTableViewController") as! PushMessageTableViewController
    }
    
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var typeId: Int = -1
    var models: [PushLogModel] = []
    var rootVC: PushMessageListViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.backgroundColor = UIColor.white
        
        tableView.isHidden = true
        noDataLabel.isHidden = false
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        EMISManager.shared.pushLog(no: "", typeid: String(self.typeId)) { [weak self] (datas) in
            guard let self = self else { return }
            self.models.removeAll()
            if (datas.count > 0) {
                for data in datas {
                    self.models.append(data)
                }
                self.tableView.isHidden = false
                self.noDataLabel.isHidden = true
            } else {
                self.tableView.isHidden = true
                self.noDataLabel.isHidden = false
            }
            self.tableView.reloadData()
            LoadingHUD.shared.dismiss()
            
            if (EMISManager.shared.pushtype != -1 && EMISManager.shared.pushno != -1) {
                for i in 0..<self.rootVC.typeId.count {
                    if (self.rootVC.typeId[i] == EMISManager.shared.pushtype) {
                        self.rootVC.menuViewController.scroll(index: i, percent: 0, animated: false)
                        self.rootVC.contentViewController.scroll(to: i, animated: false)
                        break
                    }
                }
                
                for model in self.models {
                    if (model.no == EMISManager.shared.pushno) {
                        EMISManager.shared.pushtype = -1
                        EMISManager.shared.pushno = -1
                        ViewRouter.shared.showPushMessageContentFrom(viewController: self, model: model)
                        break
                    }
                }
            }
        }
    }

}

// MARK: - UITableViewDataSource
extension PushMessageTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PushMessageCell = tableView.dequeueReusableCell(withIdentifier: "PushMessageCell", for: indexPath) as! PushMessageCell
        let model = models[indexPath.row]
        cell.titleLabel.text = model.title
        cell.contentLabel.text = model.message
        cell.dateLabel.text = model.cdate
        cell.isReaded = model.read
        return cell
    }
}

// MARK: - UITableViewDelegate
extension PushMessageTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = models[indexPath.row]
        
        ViewRouter.shared.showPushMessageContentFrom(viewController: self, model: model)
    }
}
