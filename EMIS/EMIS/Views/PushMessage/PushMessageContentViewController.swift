//
//  PushMessageContentViewController.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/17.
//

import UIKit

class PushMessageContentViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
//    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UITextView!
    var model: PushLogModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleLabel.text = model.title
        dateLabel.text = model.cdate
        typeLabel.text = model.typename
        contentLabel.text = model.message
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        EMISManager.shared.readPush(no: String(model.no)) { (responseModel) in
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
