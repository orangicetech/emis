//
//  CaseReportController.swift
//  EMIS
//
//  Created by Polowu on 2021/3/12.
//

import UIKit
import GoogleMaps
import CoreLocation
import AVFoundation

class CaseReportController: UIViewController {
    // MARK: - Public property
    var projectModel: ProjectListModel!
    var caseModel: CaseListModel!
    var caseDetailModel: CaseDetailModel = CaseDetailModel(data: [:])
    
    // MARK: - Private property
    private var inSite = false
    
    // MARK: - Info
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var renkLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    @IBOutlet weak var caseIdLabel: UILabel!
    @IBOutlet weak var caseDateTimeLabel: UILabel!
    @IBOutlet weak var caseCategoryLabel: UILabel!
    @IBOutlet weak var scopeLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var injury_1Label: UILabel!
    @IBOutlet weak var injury_2Label: UILabel!
    @IBOutlet weak var injury_3Label: UILabel!
    @IBOutlet weak var injury_4Label: UILabel!
    @IBOutlet weak var injury_5Label: UILabel!
    
    @IBOutlet weak var siteView: UIView!
    
    // MARK: - Map
    @IBOutlet weak var mapView: GMSMapView!
    private var camera: GMSCameraPosition!
    private var marker: GMSMarker!
    private var latitude: CLLocationDegrees = 0
    private var longitude: CLLocationDegrees = 0
    var mapInfo: MapInfo!
    
    // MARK: - TableView
    @IBOutlet weak var relatedTableView: UITableView!
    @IBOutlet weak var relatedTableViewHeight: NSLayoutConstraint!
    var expandArray = [ExpandType]()
    let related_section_height: CGFloat = 48
    let related_cell_height: CGFloat = 140
    @IBOutlet weak var casualtyTableView: UITableView!
    @IBOutlet weak var casualtyTableViewHeight: NSLayoutConstraint!
    var casualtyOldList: [CasualtyModel] = []
    var casualtyList: [CasualtyModel] = []
    var newCasualtyList: [CasualtyModel] = []
    let casualty_cell_height: CGFloat = 160
    
    // MARK: - TextView
    @IBOutlet weak var allSituationTextView: UITextView!
    @IBOutlet weak var situationTextView: CustomTextView!
    @IBOutlet weak var allCommentTextView: UITextView!
    @IBOutlet weak var commentTextView: CustomTextView!
    var situationText: String = ""
    var commentText: String = ""
    
    // MARK: - Upload
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var videoCollectionView: UICollectionView!
    var imageList: [FileModel] = [FileModel()]
    var videoList: [FileModel] = [FileModel()]
    let imagePicker = UIImagePickerController()
    var imageIndex: Int = 0
    var videoIndex: Int = 0
    var videoMaxDuration = 300
    
    // MARK: - Button View
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var completeTimeLabel: UILabel!
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var confimButton: CustomButton!
    var isComplete = false {
        didSet {
            buttonView.isHidden = isComplete
            completeView.isHidden = !isComplete
            situationTextView.isEditable = !isComplete
            commentTextView.isEditable = !isComplete
        }
    }
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Table View
        relatedTableView.backgroundColor = UIColor(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1)
        relatedTableView.reloadData()
        relatedTableViewHeight.constant = CGFloat(CGFloat(caseDetailModel.related_dept_list.count) * related_cell_height) + related_section_height
        // projectModel
        titleLabel.text = projectModel.name
        dateTimeLabel.text = projectModel.creation_date
        renkLabel.text = projectModel.level
        categoryLabel.text = projectModel.category
        dateTimeLabel.text = "成立時間：\(projectModel.creation_date)"
        
        // caseModel
        caseIdLabel.text = caseModel.no
        caseDateTimeLabel.text = caseModel.receive_date
        caseCategoryLabel.text = caseModel.category
        
        // caseDetailModel
        let scopes = caseDetailModel.influence_scope
        if caseDetailModel.influence_scope.count == 1 {
            self.scopeLabel.text = "招牌 \(scopes[0].value) 個"
        } else if caseDetailModel.influence_scope.count == 2 {
            let string = "\(scopes[0].description) \(scopes[0].value) \(scopes[0].unit)"
            let string_2 = "\(scopes[1].description) \(scopes[1].value) \(scopes[1].unit)"
            self.scopeLabel.text = "\(string) / \(string_2)"
        } else if caseDetailModel.influence_scope.count == 3 {
            let string = "\(scopes[0].description) \(scopes[0].value) \(scopes[0].unit)"
            let string_2 = "\(scopes[1].description) \(scopes[1].value) \(scopes[1].unit)"
            let string_3 = "\(scopes[2].description) \(scopes[2].value) \(scopes[2].unit)"
            self.scopeLabel.text = String(format: "%@ / %@ / %@", string, string_2, string_3)
        }
        
        mapInfo = MapInfo(data: [:])
        mapInfo.address = caseDetailModel.location.address
        mapInfo.district = caseDetailModel.location.district
        mapInfo.latitude = caseDetailModel.location.latitude
        mapInfo.longitude = caseDetailModel.location.longitude
        
        allSituationTextView.text = caseDetailModel.situation
        allCommentTextView.text = caseDetailModel.latest_comment
        commentTextView.text = caseDetailModel.smart_replymsg
        situationTextView.delegate = self
        commentTextView.delegate = self
        addressLabel.text = caseModel.location_district + caseModel.location_address
        
        EMISManager.shared.casualtyList(case_no: caseDetailModel.no) { (casualtys) in
            self.casualtyOldList = casualtys
            self.casualtyList = casualtys
            self.updateCasualtyView()
            self.casualtyTableView.reloadData()
        }
        
        EMISManager.shared.categoryList {(datas, injurys, disasters) in
            
        }
        
        //Setup
        setupMapView()
        setupLayout()
        imagePicker.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        situationTextView.addHandler(hasChinese: true, long: 200)
        situationTextView.hasSpecial = true
        commentTextView.addHandler(hasChinese: true, long: 200)
        commentTextView.hasSpecial = true
    }
    
    //MARK: - Privates
    private func setupLayout() {
        caseDetailModel.related_dept_list.forEach {
            if "\($0.name)" == "\(EMISManager.shared.userModel.value.user_name)" {
                inSite = true
            }
        }
        
        siteView.isHidden = inSite
        
        if !inSite {
            arrive()
        }
        
    }
    
    private func setupMapView() {
        latitude = caseDetailModel.location.latitude
        longitude = caseDetailModel.location.longitude
        
        camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15)
        mapView.camera = camera
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        mapView.delegate = self
        marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        marker.map = mapView
        
        for model in caseDetailModel.nearby_case_list {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: model.location.latitude, longitude: model.location.longitude)
            marker.map = mapView
            marker.userData = model
            marker.icon = UIImage(named: "step1_ic_map_nearby")
        }
        
        for model in caseDetailModel.related_dept_list {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: model.latitude, longitude: model.longitude)
            marker.map = mapView
            marker.userData = model
            marker.icon = UIImage(named: "ic_user")
        }
    }
    
    func generateThumbnail(for asset:AVAsset) -> UIImage? {
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 2)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        if img != nil {
            let frameImg  = UIImage(cgImage: img!)
            return frameImg
        }
        return nil
    }
    
    private func updateCasualtyView() {
        var injury_1 = 0
        var injury_2 = 0
        var injury_3 = 0
        var injury_4 = 0
        var injury_5 = 0
        injury_1Label.text = "\(injury_1)人"
        injury_2Label.text = "\(injury_2)人"
        injury_3Label.text = "\(injury_3)人"
        injury_4Label.text = "\(injury_4)人"
        injury_5Label.text = "\(injury_5)人"
        casualtyList.forEach {
            if $0.injury_id == "1" {
                injury_1 += 1
                injury_1Label.text = "\(injury_1)人"
            } else if $0.injury_id == "2" {
                injury_2 += 1
                injury_2Label.text = "\(injury_2)人"
            } else if $0.injury_id == "3" {
                injury_3 += 1
                injury_3Label.text = "\(injury_3)人"
            } else if $0.injury_id == "4" {
                injury_4 += 1
                injury_4Label.text = "\(injury_4)人"
            } else if $0.injury_id == "5" {
                injury_5 += 1
                injury_5Label.text = "\(injury_5)人"
            }
        }
        casualtyTableView.reloadData()
        DispatchQueue.main.async {
            self.casualtyTableViewHeight.constant = CGFloat(self.casualtyList.count * 160)
        }
    }
    
    func update() {
        var replyModel = ReplyModel()
        replyModel.case_no = self.caseDetailModel.no
        replyModel.case_closed = false
        replyModel.comment = commentTextView.text
        commentText = commentTextView.text
        
        var removeIndex = 0
        for model in imageList {
            if model.type == .Defaults {
                imageList.remove(at: removeIndex)
            }
            removeIndex += 1
        }
        
        var removeVideoIndex = 0
        for model in videoList {
            if model.type == .Defaults {
                videoList.remove(at: removeVideoIndex)
            }
            removeVideoIndex += 1
        }
        
        var uploadModel = UploadModel()
        uploadModel.projectId = projectModel.id
        uploadModel.no = caseModel.no
        uploadModel.category = caseModel.category
        uploadModel.receive_date = caseModel.receive_date
        uploadModel.location_address = caseModel.location_address
        uploadModel.location_district = caseModel.location_district
        uploadModel.situation = situationTextView.text
        uploadModel.replyModel = replyModel
        uploadModel.imageList = imageList
        uploadModel.videoList = videoList
        uploadModel.casualtyOldList = casualtyOldList
        uploadModel.casualtyList = casualtyList
        
        ViewRouter.shared.presentPromptFrom(viewController: self, message: "回報案件完成，並加入至待上傳清單進行上傳作業!", type: .Alert) { (buttonEvent) in
            self.completeView.isHidden = false
            self.buttonView.isHidden = true
            self.isComplete = true
            self.completeTimeLabel.text = "上傳時間：\(DateManager.dateChinaTransformTimestamp(Int(Date().timeIntervalSince1970))!)"
        }
        
        
//        let data: [String: Any] = uploadModel.dictionary()
//        print("upload data:\(data)")
//        let model: UploadModel = UploadModel.conver(data: data)
//        print("upload model:\(model)")
//        UserManager.shared.saveUploadModels([model])
        
        UploadManager.shared.addCase(model: uploadModel)
        
//        EMISManager.shared.reply(model: replyModel, images: imageList, videos: videoList) { (progressStatus) in
//
//        } result: { (responseModel) in
//            LoadingHUD.shared.dismiss()
//            if responseModel.status {
//                self.completeView.isHidden = false
//                self.buttonView.isHidden = true
//                self.isComplete = true
//                self.completeTimeLabel.text = "上傳時間：\(DateManager.dateChinaTransformTimestamp(Int(Date().timeIntervalSince1970))!)"
//                ViewRouter.shared.presentPromptFrom(viewController: self, message: "案件上傳成功", type: .Alert) { (buttonEvent) in
//
//                }
//            } else {
//                ViewRouter.shared.presentPromptFrom(viewController: self, message: responseModel.message, type: .Alert) { (buttonEvent) in
//
//                }
//            }
//        }
    }
    
    func arrive() {
        ViewRouter.shared.presentPromptFrom(viewController: self, message: "是否抵達現場？", type: .Confirm) { [weak self] (buttonEvent) in
            guard let self = self else { return }
            if buttonEvent == .confim {
                LoadingHUD.shared.show()
                let dateTime = DateManager.ApiDateTransformTimestamp(Int(Date().timeIntervalSince1970))!
                var locationModel: CaseLocationModel = CaseLocationModel()
                locationModel.case_no = self.caseDetailModel.no
                locationModel.execution_date = dateTime
//                locationModel.latitude = self.caseDetailModel.location.latitude
//                locationModel.longitude = self.caseDetailModel.location.longitude
                locationModel.latitude = EMISManager.shared.locationModel.value.latitude
                locationModel.longitude = EMISManager.shared.locationModel.value.longitude
                EMISManager.shared.arrive(model: locationModel) { (pesponseModel) in
                    LoadingHUD.shared.dismiss()
                    if pesponseModel.status {
                        ViewRouter.shared.presentPromptFrom(viewController: self, message: "回報抵達現場完成", type: .Alert, confimHandle: nil)
                        self.updateCaseDetail()
                    } else {
                        ViewRouter.shared.presentPromptFrom(viewController: self, message: pesponseModel.message, type: .Alert, confimHandle: nil)
                    }
                }
            }
        }
    }
    
    func verifi() -> Bool {
        var verifi = true
        if commentTextView.text?.count == 0 {
            verifi = false
            AlertManager.shared.showMessage(message: "請回覆處理情況", title: "")
        }
        
        return verifi
    }
    
    func updateCaseDetail() {
        LoadingHUD.shared.show()
        EMISManager.shared.caseDetail(case_no: caseModel.no) { [weak self] (caseDetailModel) in
            guard let self = self else { return }
            print("caseDetailModel:\(caseDetailModel.related_dept_list)")
            self.caseDetailModel = caseDetailModel
            self.setupLayout()
            LoadingHUD.shared.dismiss()
        }
    }
    
    //MARK: - IBAction
    @IBAction func onClickBack(_ sender: Any) {
        ViewRouter.shared.presentPromptFrom(viewController: self, message: "按下「確定」將返回案件清單，您所輸入的資料將全部捨棄；若想繼續進行作業，請按「取消」。", type: .Confirm) { (buttonEvent) in
            if buttonEvent == .confim {
                ViewRouter.shared.backToFunctionFrom(viewController: self)
            }
        }
    }
    
    @IBAction func onClickBackFunction(_ sender: Any) {
        ViewRouter.shared.backToFunctionFrom(viewController: self)
    }
    
    @IBAction func onClickAddPhoto(_ sender: Any) {
        let btn = sender as! UIButton
        imageIndex = btn.tag
        
        let alertController = UIAlertController(
            title: "",
            message: "請選擇圖片...",
            preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(
            title: "取消",
            style: .cancel,
            handler: nil)
        
        alertController.addAction(cancelAction)
        let cameraAction = UIAlertAction(title: "拍照", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(cameraAction)
        
        let photoAction = UIAlertAction(title: "從相簿挑選", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(photoAction)
        if !isComplete {
            self.present( alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickAddVideo(_ sender: Any) {
        let btn = sender as! UIButton
        imageIndex = btn.tag
        
        let alertController = UIAlertController(
            title: "",
            message: "請選擇影片...",
            preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(
            title: "取消",
            style: .cancel,
            handler: nil)
        
        alertController.addAction(cancelAction)
        let cameraAction = UIAlertAction(title: "拍攝", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.mediaTypes = ["public.movie"]
            self.imagePicker.videoMaximumDuration = TimeInterval(self.videoMaxDuration)
            self.imagePicker.videoQuality = .typeMedium
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(cameraAction)
        
        let photoAction = UIAlertAction(title: "從相簿挑選", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = ["public.movie"]
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(photoAction)
        if !isComplete {
            self.present( alertController, animated: true, completion: nil)
        }
    }
    
    @objc func onClickExpand(_ sender: Any) {
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        
        let type = ExpandType(rawValue: btn.tag)!
        if expandArray.contains(type) {
            if let index = expandArray.firstIndex(of: type) {
                expandArray.remove(at: index)
            }
        } else {
            expandArray.append(type)
        }
        
        relatedTableView.reloadData()
        
        var height: CGFloat = related_section_height
        if !expandArray.contains(.related) {
            height += CGFloat(CGFloat(caseDetailModel.related_dept_list.count) * self.related_cell_height)
        }
        
        relatedTableViewHeight.constant = height
    }
    
    @IBAction func onClickUpdateGps(_ sender: Any) {
        ViewRouter.shared.presentToMapViewFrom(viewController: self, mapInfo: mapInfo) { (isConfim, mapInfo) in
            if isConfim {
                if mapInfo != nil {
                    self.mapInfo = mapInfo
                    LoadingHUD.shared.show()
                    var model = ReplceCaseModel()
                    model.case_no = self.caseDetailModel.no
                    if mapInfo != nil {
                        var info = mapInfo!
                        info.district = info.district_id
                        model.location = info
                    }
                    EMISManager.shared.updateCase(model: model) { (pesponseModel) in
                        LoadingHUD.shared.dismiss()
                        if pesponseModel.status {
                            self.addressLabel.text = mapInfo!.district + mapInfo!.address
                            self.latitude = mapInfo!.latitude
                            self.longitude = mapInfo!.longitude
                            let coordinate = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                            self.marker.position = coordinate
                            self.mapView.animate(toLocation: coordinate)
                        }
                        ViewRouter.shared.presentPromptFrom(viewController: self, message: pesponseModel.message, type: .Alert) { (buttonEvent) in
                            
                        }
                    }
                }
            }
        }
    }
    
    
    @IBAction func onClickArrive(_ sender: Any) {
        arrive()
    }
    
    @IBAction func onClickConfim(_ sender: Any) {
        if verifi() {
            self.update()
        }
    }
    
    @IBAction func onClickLive(_ sender: Any) {
        if !isComplete {
            ViewRouter.shared.presentPromptFrom(viewController: self, message: "是否先上傳處理結果，再進行「直播」？", type: .Confirm) { (buttonEvent) in
                if buttonEvent == .confim {
                    self.onClickConfim(self.confimButton as Any)
                }
            }
        } else {
            UIApplication.tryURL(urls: [
                "fb://groups/426241551840753", // App
                "https://www.facebook.com/groups/426241551840753" // Website if app fails
            ])
        }
    }
    
    @IBAction func onClickCasualty(_ sender: Any) {
        if !isComplete {
            ViewRouter.shared.presentToCasualtyViewFrom(viewController: self, model: nil) { [weak self] (isConfim, casualtyModel) in
                guard let self = self else { return }
                if isConfim {
                    self.isComplete = false
                    self.casualtyList.append(casualtyModel!)
                    self.updateCasualtyView()
                }
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension CaseReportController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == relatedTableView {
            return related_section_height
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == relatedTableView {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.relatedTableView.bounds.size.width, height: 48))
            headerView.backgroundColor = UIColor(named: "Rank1Color")
            
            let label = UILabel(frame: CGRect(x: 17, y: 10, width: 200, height: 20))
            label.textColor = UIColor.white
            label.text = "現場執行人員"
            label.font = UIFont.systemFont(ofSize: 19)
            
            let button = UIButton(frame: CGRect(x: self.relatedTableView.bounds.size.width - 55 , y: 0, width: 48, height: 48))
            button.backgroundColor = UIColor.clear
            if expandArray.contains(.related) {
                button.setImage(UIImage(named: "icon_expand_more_w"), for: .normal)
            } else {
                button.setImage(UIImage(named: "icon_expand_less_w"), for: .normal)
            }
            button.addTarget(
                self,
                action: #selector(CaseDetalController.onClickExpand),
                for: .touchUpInside)
            button.tag = ExpandType.related.rawValue
            
            headerView.addSubview(label)
            headerView.addSubview(button)
            return headerView
        } else {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.casualtyTableView.bounds.size.width, height: 0))
            return headerView
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == relatedTableView {
            if expandArray.contains(.related) {
                return 0
            } else {
                return caseDetailModel.related_dept_list.count
            }
        } else {
            return casualtyList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == relatedTableView {
            let relatedCell: RelatedCell = tableView.dequeueReusableCell(withIdentifier: "RelatedCell", for: indexPath) as! RelatedCell
            let model = caseDetailModel.related_dept_list[indexPath.row]
            relatedCell.nameLabel.text = model.name
            relatedCell.deptLabel.text = model.dept
            relatedCell.phoneLabel.text = model.phone
            relatedCell.dateTimeLabel.text = model.execution_time
            return relatedCell
        } else {
            let casualtyCell: CasualtyCell = tableView.dequeueReusableCell(withIdentifier: "CasualtyCell", for: indexPath) as! CasualtyCell
            let model = casualtyList[indexPath.row]
            casualtyCell.nameLabel.text = model.name
            casualtyCell.genderLabel.text = model.gender
            casualtyCell.ageLabel.text = model.age + "歲"
            casualtyCell.injuryLabel.text = model.injury
            casualtyCell.descriptionLabel.text = model.description
            casualtyCell.casualtyDeleteHandle = { [weak self] in
                guard let self = self else { return }
                if !self.isComplete {
                    if model.seqno.count > 0 {
                        self.isComplete = false
                        self.casualtyList.remove(at: indexPath.row)
                        self.updateCasualtyView()
                    }
                }
            }
            
            casualtyCell.casualtyEditHandle = {
                ViewRouter.shared.presentToCasualtyViewFrom(viewController: self, model: model) { [weak self] (isConfim, casualtyModel) in
                    guard let self = self else { return }
                    if !self.isComplete {
                        if isConfim {
                            self.isComplete = false
                            self.casualtyList[indexPath.row] = casualtyModel!
                            self.updateCasualtyView()
                        }
                    }
                }
            }
            return casualtyCell
        }
        
    }
}

extension CaseReportController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == relatedTableView {
            return related_cell_height
        } else {
            return casualty_cell_height
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == relatedTableView {
            let model = caseDetailModel.related_dept_list[indexPath.row]
            if let url = URL(string: "tel://\(model.phone)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:],
                                              completionHandler: {
                                                (success) in
                                              })
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        } else {
            let model = casualtyList[indexPath.row]
            ViewRouter.shared.presentToCasualtyDetalFrom(viewController: self, text: model.description)
        }
        
    }
}

//MARK: - UICollectionView Delegate
extension CaseReportController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

//MARK: - UICollectionViewDataSource
extension CaseReportController:  UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == imageCollectionView {
            return imageList.count
        } else if collectionView == videoCollectionView {
            return videoList.count
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == imageCollectionView {
            if imageList[indexPath.row].type == .Defaults {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCell", for: indexPath) as! AddCell
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
                cell.imageView.image = imageList[indexPath.row].image
                cell.deleteHandler = { [weak self] in
                    guard let self = self else { return }
                    if !self.isComplete {
                        self.deleteImage(index: indexPath.row, isVideo: false)
                    }
                }
                return cell
            }
        } else {
            if videoList[indexPath.row].type == .Defaults {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddVideoCell", for: indexPath) as! AddCell
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
                cell.imageView.image = videoList[indexPath.row].image
                cell.deleteHandler = { [weak self] in
                    guard let self = self else { return }
                    if !self.isComplete {
                        self.deleteImage(index: indexPath.row, isVideo: true)
                    }
                }
                return cell
            }
        }
        
    }
    
    func addImage(_ image: UIImage, isVideo: Bool, url: URL?) {
        if isVideo {
            var model = FileModel()
            model.type = .Video
            model.image = image
            model.url = url
            videoList.removeAll()
            videoList.insert(model, at: 0)
            videoCollectionView.reloadData()
        } else {
            var model = FileModel()
            model.type = .Image
            model.image = image
            model.url = url
            if imageList.count ==  3 {
                imageList.remove(at: 2)
            }
            imageList.insert(model, at: 0)
            imageCollectionView.reloadData()
        }
    }
    
    func deleteImage(index: Int, isVideo: Bool) {
        if isVideo {
            if videoList.count == 1 {
                if videoList.last?.type != .Defaults {
                    videoList.append(FileModel())
                }
            }
            self.videoList.remove(at: index)
            self.videoCollectionView.reloadData()
        } else {
            if imageList.count == 3 {
                if imageList.last?.type != .Defaults {
                    imageList.append(FileModel())
                }
            }
            self.imageList.remove(at: index)
            self.imageCollectionView.reloadData()
        }
    }
}

extension CaseReportController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        isComplete = false
        guard let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String else {return}
        if mediaType == "public.movie" {
            guard let mediaURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL else { return }
            let asset = AVAsset(url: mediaURL)
            if Int(CMTimeGetSeconds(asset.duration)) < videoMaxDuration {
                let url = MediaManager.shared.saveVideo(mediaURL: mediaURL)
                self.addImage(self.generateThumbnail(for: asset)!, isVideo: true, url: url)
            } else {
                ViewRouter.shared.presentPromptFrom(viewController: self, message: "影片長度超過限制", type: .Alert) { (buttonEvent) in
                    
                }
            }
        } else {
            let image: UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let url = MediaManager.shared.saveImage(image: image)
            self.addImage(image, isVideo: false, url: url)
        }
        
        //取得照片後將imagePickercontroller dismiss
        picker.dismiss(animated: true, completion: nil)
    }
}

extension CaseReportController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
        if situationTextView.text != situationText {
            isComplete = false
        }
        
        if commentTextView.text != commentText {
            isComplete = false
        }
    }
}

extension CaseReportController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let model = marker.userData as? NearCaseModel {
            ViewRouter.shared.presentToNearDetailFrom(viewController: self, nearCaseModel: model)
        }
        return true
    }
}
