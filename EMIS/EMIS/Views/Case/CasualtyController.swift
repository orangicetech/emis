//
//  CasualtyController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/3.
//

import UIKit

class CasualtyController: UIViewController {
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var ageTextField: CustomTextField!
    @IBOutlet var injuryButtons: [CustomButton]!
    @IBOutlet var genderButtons: [CustomButton]!
    @IBOutlet weak var descriptionTextView: CustomTextView!
    public var model: CasualtyModel!
    public var confimHandle: CasualtyConfimHandle!
    
    private var injurysList: [InjuryModel] = []
    
    private var selectedInjuryIndex: Int = 0 {
        didSet {
            updateInjuryButtons()
        }
    }
    
    private var selectedGenderIndex: Int = 0 {
        didSet {
            updateGenderButtons()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionTextView.backgroundColor = UIColor.white
        injurysList = EMISManager.shared.injurysList
        if model != nil {
            nameTextField.text = model.name
            ageTextField.text = model.age
            injurysList.forEach {
                if String($0.id) == model.injury_id {
                    self.selectedInjuryIndex = $0.id - 1
                }
            }
            
            if model.gender == "男" {
                selectedGenderIndex = 0
            } else {
                selectedGenderIndex = 1
            }
            
            descriptionTextView.text = model.description
        } else {
            updateInjuryButtons()
            updateGenderButtons()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        descriptionTextView.addHandler(hasChinese: true, long: 200)
        descriptionTextView.hasSpecial = true
        nameTextField.addHandler(hasChinese: true, long: 15)
        ageTextField.addHandler(hasChinese: true, long: 3)
    }
    // MARK: - Private
    func verifi() -> Bool {
        var verifi = true
        if nameTextField.text?.count == 0 {
            verifi = false
            AlertManager.shared.showMessage(message: "請輸入姓名", title: "")
        } else if ageTextField.text?.count == 0 {
            AlertManager.shared.showMessage(message: "請輸入年齡", title: "")
            verifi = false
        }
        return verifi
    }
    private func updateInjuryButtons() {
        injuryButtons.forEach {
            let btn = $0
            btn.backgroundColor = btn.tag == selectedInjuryIndex ? UIColor(named: "Rank2Color") : UIColor(named: "138Color")
        }
    }
    
    private func updateGenderButtons() {
        genderButtons.forEach {
            let btn = $0
            btn.backgroundColor = btn.tag == selectedGenderIndex ? UIColor(named: "Rank2Color") : UIColor(named: "138Color")
        }
    }
    
    @IBAction func onClickInjury(_ sender: Any) {
        let btn = sender as! UIButton
        selectedInjuryIndex = btn.tag
    }
    
    @IBAction func onClickGender(_ sender: Any) {
        let btn = sender as! UIButton
        selectedGenderIndex = btn.tag
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        confimHandle?(false, nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickConfim(_ sender: Any) {
        if verifi() {
            var model = CasualtyModel(data: [:])
            
            if self.model != nil {
                model = self.model
            }
            
            model = CasualtyModel(data: [:])
            model.name = nameTextField.text!
            model.age = ageTextField.text!
            model.description = descriptionTextView.text
            model.injury = injurysList[selectedInjuryIndex].text
            model.injury_id = String(injurysList[selectedInjuryIndex].id)
            model.gender = selectedGenderIndex == 0 ? "男" : "女"
            confimHandle?(true, model)
            
            dismiss(animated: true, completion: nil)
        }
    }
}
