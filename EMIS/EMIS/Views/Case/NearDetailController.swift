//
//  NearDetailController.swift
//  EMIS
//
//  Created by Polowu on 2021/3/13.
//

import UIKit

class NearDetailController: UIViewController {
    var model: NearCaseModel!
    @IBOutlet weak var caseIdLabel: UILabel!
    @IBOutlet weak var caseDateTimeLabel: UILabel!
    @IBOutlet weak var caseCategoryLabel: UILabel!
    @IBOutlet weak var caseAddressLabel: UILabel!
    @IBOutlet weak var caseDescriptionLabel: UILabel!
    @IBOutlet weak var scopeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        caseIdLabel.text = model.no
        caseDateTimeLabel.text = model.receive_date
        caseCategoryLabel.text = model.category
        caseAddressLabel.text = model.location.district + model.location.address
        caseDescriptionLabel.text = model.situation
        let scopes = model.influence_scope
        if model.influence_scope.count == 1 {
            self.scopeLabel.text = "招牌 \(scopes[0].value) 個"
        } else if model.influence_scope.count == 2 {
            let string = "\(scopes[0].description) \(scopes[0].value) \(scopes[0].unit)"
            let string_2 = "\(scopes[1].description) \(scopes[1].value) \(scopes[1].unit)"
            self.scopeLabel.text = "\(string) / \(string_2)"
        } else if model.influence_scope.count == 3 {
            let string = "\(scopes[0].description) \(scopes[0].value) \(scopes[0].unit)"
            let string_2 = "\(scopes[1].description) \(scopes[1].value) \(scopes[1].unit)"
            let string_3 = "\(scopes[2].description) \(scopes[2].value) \(scopes[2].unit)"
            self.scopeLabel.text = String(format: "%@ / %@ / %@", string, string_2, string_3)
        }
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
