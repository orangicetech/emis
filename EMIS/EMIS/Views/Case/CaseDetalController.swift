//
//  CaseDetalController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/10.
//

import UIKit
import GoogleMaps
import CoreLocation

enum ExpandType: Int {
    case related = 0
    case near
}

class CaseDetalController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var renkLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    @IBOutlet weak var caseIdLabel: UILabel!
    @IBOutlet weak var caseDateTimeLabel: UILabel!
    @IBOutlet weak var caseCategoryLabel: UILabel!
    @IBOutlet weak var caseAddressLabel: UILabel!
    @IBOutlet weak var caseDescriptionLabel: UILabel!
    
    @IBOutlet weak var scopeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var camera: GMSCameraPosition!
    private var marker: GMSMarker!
    private var latitude: CLLocationDegrees = 0
    private var longitude: CLLocationDegrees = 0
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    var projectModel: ProjectListModel!
    var caseModel: CaseListModel!
    var caseDetailModel: CaseDetailModel = CaseDetailModel(data: [:])
    @IBOutlet weak var mapView: GMSMapView!
    
    var expandArray: [ExpandType] = [.near, .related]
    
    let section_height: CGFloat = 48
    let cell_height: CGFloat = 140
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1)
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        titleLabel.text = projectModel.name
        dateTimeLabel.text = projectModel.creation_date
        renkLabel.text = projectModel.level
        categoryLabel.text = projectModel.category
        dateTimeLabel.text = "成立時間：\(projectModel.creation_date)"
        
        caseIdLabel.text = caseModel.no
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LoadingHUD.shared.show()
        EMISManager.shared.caseDetail(case_no: caseModel.no) { [weak self] (caseDetailModel) in
            guard let self = self else { return }
            self.caseDetailModel = caseDetailModel
            self.caseDateTimeLabel.text = caseDetailModel.receive_date
            self.caseCategoryLabel.text = caseDetailModel.category
            self.caseDescriptionLabel.text = caseDetailModel.situation
            self.caseAddressLabel.text = caseDetailModel.location.district + caseDetailModel.location.address
            LoadingHUD.shared.dismiss()
            let scopes = caseDetailModel.influence_scope
            if caseDetailModel.influence_scope.count == 1 {
                self.scopeLabel.text = "招牌 \(scopes[0].value) 個"
            } else if caseDetailModel.influence_scope.count == 2 {
                let string = "\(scopes[0].description) \(scopes[0].value) \(scopes[0].unit)"
                let string_2 = "\(scopes[1].description) \(scopes[1].value) \(scopes[1].unit)"
                self.scopeLabel.text = "\(string) / \(string_2)"
            } else if caseDetailModel.influence_scope.count == 3 {
                let string = "\(scopes[0].description) \(scopes[0].value) \(scopes[0].unit)"
                let string_2 = "\(scopes[1].description) \(scopes[1].value) \(scopes[1].unit)"
                let string_3 = "\(scopes[2].description) \(scopes[2].value) \(scopes[2].unit)"
                self.scopeLabel.text = String(format: "%@ / %@ / %@", string, string_2, string_3)
            }
            self.tableView.reloadData()
            var height: CGFloat = (self.section_height * 2.0)
            if !self.expandArray.contains(.related) {
                height += CGFloat(CGFloat(caseDetailModel.related_dept_list.count) * self.cell_height)
            }
            
            if !self.expandArray.contains(.near) {
                height += CGFloat(CGFloat(caseDetailModel.nearby_case_list.count) * self.cell_height)
            }
            
            self.tableViewHeight.constant = height
            self.setupMapView()
        }
    }
    private func setupMapView() {
        latitude = caseDetailModel.location.latitude
        longitude = caseDetailModel.location.longitude

        camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15)
        mapView.camera = camera
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        mapView.delegate = self
        marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        marker.map = mapView
        for model in caseDetailModel.nearby_case_list {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: model.location.latitude, longitude: model.location.longitude)
            marker.map = mapView
            marker.userData = model
            marker.icon = UIImage(named: "step1_ic_map_nearby")
        }
        
        for model in caseDetailModel.related_dept_list {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: model.latitude, longitude: model.longitude)
            marker.map = mapView
            marker.userData = model
            marker.icon = UIImage(named: "ic_user")
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickMergerCase(_ sender: Any) {
        ViewRouter.shared.presentToMergerCaseFrom(viewController: self, caseDetailModel: self.caseDetailModel)
    }
    
    @IBAction func onClickConfim(_ sender: Any) {
        var inUploading = false
        EMISManager.shared.uploadingList.value.forEach {
            if $0.no == caseDetailModel.no {
                inUploading = true
            }
        }
        
        if (caseDetailModel.latest_comment.count == 0) && !inUploading {
            ViewRouter.shared.presentPromptFrom(viewController: self, message: "確定回報該案件？", type: .Confirm) { (buttonEvent) in
                if buttonEvent == .confim {
                    ViewRouter.shared.showCaseReportFrom(viewController: self, model: self.caseModel, projectModel: self.projectModel, caseDetailModel: self.caseDetailModel)
                }
            }
        } else {
            ViewRouter.shared.showCaseReportFrom(viewController: self, model: self.caseModel, projectModel: self.projectModel, caseDetailModel: self.caseDetailModel)
        }
    }
    
    @objc func onClickExpand(_ sender: Any) {
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected

        let type = ExpandType(rawValue: btn.tag)!
        if expandArray.contains(type) {
            if let index = expandArray.firstIndex(of: type) {
                expandArray.remove(at: index)
            }
        } else {
            expandArray.append(type)
        }
        
        tableView.reloadData()
        
        var height: CGFloat = (self.section_height * 2.0)
        if !expandArray.contains(.related) {
            height += CGFloat(CGFloat(caseDetailModel.related_dept_list.count) * self.cell_height)
        }
        
        if !expandArray.contains(.near) {
            height += CGFloat(CGFloat(caseDetailModel.nearby_case_list.count) * self.cell_height)
        }
        
        tableViewHeight.constant = height
    }

}

// MARK: - UITableViewDataSource
extension CaseDetalController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section_height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.tableView.bounds.size.width, height: 48))
            headerView.backgroundColor = UIColor(named: "Rank1Color")
            
            let label = UILabel(frame: CGRect(x: 17, y: 10, width: 200, height: 20))
            label.textColor = UIColor.white
            label.text = "現場執行人員"
            label.font = UIFont.systemFont(ofSize: 19)
            
            let button = UIButton(frame: CGRect(x: self.tableView.bounds.size.width - 55 , y: 0, width: 48, height: 48))
            button.backgroundColor = UIColor.clear
            if expandArray.contains(.related) {
                button.setImage(UIImage(named: "icon_expand_more_w"), for: .normal)
            } else {
                button.setImage(UIImage(named: "icon_expand_less_w"), for: .normal)
            }
            button.addTarget(
                self,
                action: #selector(CaseDetalController.onClickExpand),
                for: .touchUpInside)
            button.tag = ExpandType.related.rawValue
            
            headerView.addSubview(label)
            headerView.addSubview(button)
            return headerView
        } else {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.tableView.bounds.size.width, height: 48))
            headerView.backgroundColor = UIColor(named: "DSSColor")
            
            let label = UILabel(frame: CGRect(x: 17, y: 10, width: 200, height: 20))
            label.textColor = UIColor.white
            if (caseDetailModel.nearby_case_distance_desc != "") {
                label.text = "鄰近案件" + "( " + caseDetailModel.nearby_case_distance_desc + " )"
            } else {
                label.text = "鄰近案件"
            }
            label.font = UIFont.systemFont(ofSize: 19)
            
            let button = UIButton(frame: CGRect(x: self.tableView.bounds.size.width - 55 , y: 0, width: 48, height: 48))
            button.backgroundColor = UIColor.clear
            if expandArray.contains(.near) {
                button.setImage(UIImage(named: "icon_expand_more_w"), for: .normal)
            } else {
                button.setImage(UIImage(named: "icon_expand_less_w"), for: .normal)
            }
            button.addTarget(
                self,
                action: #selector(CaseDetalController.onClickExpand),
                for: .touchUpInside)
            button.tag = ExpandType.near.rawValue
            
            headerView.addSubview(label)
            headerView.addSubview(button)
            
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if expandArray.contains(.related) {
                return 0
            } else {
                return caseDetailModel.related_dept_list.count
            }
            
        } else {
            if expandArray.contains(.near) {
                return 0
            } else {
                return caseDetailModel.nearby_case_list.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let relatedCell: RelatedCell = tableView.dequeueReusableCell(withIdentifier: "RelatedCell", for: indexPath) as! RelatedCell
            let model = caseDetailModel.related_dept_list[indexPath.row]
            relatedCell.nameLabel.text = model.name
            relatedCell.deptLabel.text = model.dept
            relatedCell.phoneLabel.text = model.phone
            relatedCell.dateTimeLabel.text = model.execution_time
            return relatedCell
        } else {
            let nearCell: NearCell = tableView.dequeueReusableCell(withIdentifier: "NearCell", for: indexPath) as! NearCell
            let model = caseDetailModel.nearby_case_list[indexPath.row]
            nearCell.noLabel.text = model.no
            nearCell.categoryLabel.text = model.category
            nearCell.dateTimeLabel.text = "執行時間：\(model.receive_date)"
            nearCell.addressLabel.text = model.location.district + model.location.address
            return nearCell
        }
    }
}

extension CaseDetalController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cell_height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let model = caseDetailModel.related_dept_list[indexPath.row]
            if let url = URL(string: "tel://\(model.phone)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:],
                                              completionHandler: {
                                                (success) in
                                              })
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        } else {
            let model = caseDetailModel.nearby_case_list[indexPath.row]
            ViewRouter.shared.presentToNearDetailFrom(viewController: self, nearCaseModel: model)
        }
    }
}

extension CaseDetalController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let model = marker.userData as? NearCaseModel {
            ViewRouter.shared.presentToNearDetailFrom(viewController: self, nearCaseModel: model)
        }
        return true
    }
}
