//
//  MergerCaseController.swift
//  EMIS
//
//  Created by Polowu on 2021/3/12.
//

import UIKit
import GoogleMaps
import CoreLocation

class MergerCaseController: UIViewController {
    var caseDetailModel: CaseDetailModel = CaseDetailModel(data: [:])
    var selected_case_list: [NearCaseModel] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    var latitude: CLLocationDegrees = 0
    var longitude: CLLocationDegrees = 0
    private var caseMarker: GMSMarker!
    private var camera: GMSCameraPosition!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.white
        LoadingHUD.shared.show()
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupMapView()
        LoadingHUD.shared.dismiss()
    }
    
    @IBAction func onClickClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickConfim(_ sender: Any) {
    var model = MergerCaseModel()
        model.case_no = caseDetailModel.no
        var mergerCaseNoList: [String] = []
        for model in selected_case_list {
            mergerCaseNoList.append(model.no)
        }
        model.mergerCaseNoList = mergerCaseNoList
        LoadingHUD.shared.show()
        EMISManager.shared.mergerCase(model: model) { (responseModel) in
            LoadingHUD.shared.dismiss()
            if responseModel.status {
                ViewRouter.shared.presentPromptFrom(viewController: self, message: "建議併案送出申請成功", type: .Alert) { [weak self] (buttonEvent) in
                    guard let self = self else { return }
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    private func setupMapView() {
        latitude = caseDetailModel.location.latitude
        longitude = caseDetailModel.location.longitude
        camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15)
        mapView.camera = camera
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        mapView.delegate = self
        
        caseMarker = GMSMarker()
        caseMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        caseMarker.map = mapView

        for model in caseDetailModel.nearby_case_list {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: model.location.latitude, longitude: model.location.longitude)
            marker.map = mapView
            marker.userData = model
            marker.icon = UIImage(named: "step1_ic_map_nearby")
        }
        
        for model in caseDetailModel.related_dept_list {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: model.latitude, longitude: model.longitude)
            marker.map = mapView
            marker.userData = model
            marker.icon = UIImage(named: "ic_user")
        }
    }
}
// MARK: - UITableViewDataSource
extension MergerCaseController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selected_case_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mergerCaseCell: MergerCaseCell = tableView.dequeueReusableCell(withIdentifier: "MergerCaseCell", for: indexPath) as! MergerCaseCell
        let model = selected_case_list[indexPath.row]
        mergerCaseCell.noLabel.text = model.no
        mergerCaseCell.categoryLabel.text = model.category
        mergerCaseCell.addressLabel.text = model.location.district + model.location.address
        mergerCaseCell.dateTimeLabel.text = model.receive_date
        mergerCaseCell.deleteHandler = {
            self.selected_case_list.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
        return mergerCaseCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension MergerCaseController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = selected_case_list[indexPath.row] as? NearCaseModel {
            ViewRouter.shared.presentToNearDetailFrom(viewController: self, nearCaseModel: model)
        }
    }
}

extension MergerCaseController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let model = marker.userData as? NearCaseModel {
            var isAddModel: Bool = true
            for nearCase in selected_case_list {
                if model.no == nearCase.no {
                    isAddModel = false
                }
            }
            
            if isAddModel {
                selected_case_list.append(model)
                tableView.reloadData()
            }
        }
        return true
    }
}
