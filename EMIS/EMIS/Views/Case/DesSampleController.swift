//
//  DesSampleController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/2.
//

import UIKit

class DesSampleController: UIViewController {
    public var confimHandle: SampleConfimHandle!
    
    @IBOutlet weak var tableView: UITableView!
    public var category_id = ""
    public var selectedDesSampleModel: DesSampleModel!
    public var sampleList: [DesSampleModel] = []
    private var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.white
        self.selectedDesSampleModel = self.sampleList.first
        self.tableView.reloadData()
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        confimHandle?(.cancel, nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickConfim(_ sender: Any) {
        confimHandle?(.confim, selectedDesSampleModel)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickExit(_ sender: Any) {
        confimHandle?(.cancel, nil)
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension DesSampleController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sampleCell: DesSampleCell = tableView.dequeueReusableCell(withIdentifier: "DesSampleCell", for: indexPath) as! DesSampleCell
        let selected = selectedIndex == indexPath.row
        sampleCell.cellSelected = selected
        sampleCell.titleLabel.text = sampleList[indexPath.row].situ
        return sampleCell
    }

}

// MARK: - UITableViewDelegate
extension DesSampleController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        selectedDesSampleModel = self.sampleList[selectedIndex]
        tableView.reloadData()
    }
}
