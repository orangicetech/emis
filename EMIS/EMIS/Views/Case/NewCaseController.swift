//
//  NewCaseController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/3.
//

import UIKit
import Alamofire
typealias NewCaseFinidhedHandle  = (Bool) -> Void
class NewCaseController: UIViewController {
    var offlineModel: OfflineCaseModel?
    var projectModel: ProjectListModel?
    @IBOutlet weak var projectTextField: UITextField!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var phoneTextField: CustomTextField!
    @IBOutlet weak var disasterCollectionView: UICollectionView!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var districtTextFiled: UITextField!
    @IBOutlet weak var latTextFiled: CustomTextField!
    @IBOutlet weak var longTextFiled: CustomTextField!
    @IBOutlet weak var addressTextFiled: CustomTextField!
    
    @IBOutlet weak var line: UIView!
    @IBOutlet var scopeViews_1: [UIView]!
    @IBOutlet weak var scopeView_1: UIView!
    @IBOutlet weak var textField_s1_1: CustomTextField!
    
    @IBOutlet var scopeViews_2: [UIView]!
    @IBOutlet weak var scopeView_2: UIView!
    @IBOutlet weak var label_s2_1: UILabel!
    @IBOutlet weak var label_s2_2: UILabel!
    @IBOutlet weak var label_s2_1_unit: UILabel!
    @IBOutlet weak var label_s2_2_unit: UILabel!
    @IBOutlet weak var textField_s2_1: CustomTextField!
    @IBOutlet weak var textField_s2_2: CustomTextField!
    
    @IBOutlet var scopeViews_3: [UIView]!
    @IBOutlet weak var scopeView_3: UIView!
    @IBOutlet weak var label_s3_1: UILabel!
    @IBOutlet weak var label_s3_2: UILabel!
    @IBOutlet weak var label_s3_3: UILabel!
    @IBOutlet weak var label_s3_1_unit: UILabel!
    @IBOutlet weak var label_s3_2_unit: UILabel!
    @IBOutlet weak var label_s3_3_unit: UILabel!
    @IBOutlet weak var textField_s3_1: CustomTextField!
    @IBOutlet weak var textField_s3_2: CustomTextField!
    @IBOutlet weak var textField_s3_3: CustomTextField!
    
    @IBOutlet weak var sampleTextView: CustomTextView!
    
    @IBOutlet weak var casualtyTableView: UITableView!
    @IBOutlet weak var casualtyTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var injury_1Label: UILabel!
    @IBOutlet weak var injury_2Label: UILabel!
    @IBOutlet weak var injury_3Label: UILabel!
    @IBOutlet weak var injury_4Label: UILabel!
    @IBOutlet weak var injury_5Label: UILabel!
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    @IBOutlet weak var confimButton: CustomButton!
    
    var isCompleted: Bool = false
    var isLiveFlow: Bool = false
    var hasLive: Bool = false
    private var categorySelectedIndex = 0
    private var injurySelectedIndex = 0
    private var disasterSelectedIndex = 0
    private var projectSelectedIndex = 0

    var categoryList: [CategoryModel] = []
    var injuryList: [InjuryModel] = []
    var disasterList: [DisasterModel] = []
    var casualtyList: [CasualtyModel] = []
    var smartMsgList: [CaseSmartMsg] = []
    var scope: [ScopeModel] = []
    var location: MapInfo!
    var timestamp: Int = 0
    
    let imagePicker = UIImagePickerController()
    var imageIndex: Int = 0
    var imageList: [FileModel] = [FileModel()]
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var completeView: UIView!
    
    // offline
    
    @IBOutlet weak var projectNameView: UIView!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var userPhoneView: UIView!
    @IBOutlet weak var deleteCaseButton: UIButton!
    @IBOutlet weak var uploadButton: CustomButton!
    
    @IBOutlet weak var xPositionView: UIView!
    @IBOutlet weak var yPositionView: UIView!
    
    @IBOutlet weak var desSampleButton: CustomButton!
    @IBOutlet weak var locationHeightConstraint: NSLayoutConstraint!
    
    var districtSelectedIndex = 0
    var districtList: [DistrictModel] = []
    var offlineDataIndex = -1
    var categoryPicker = UIPickerView()
    var districtPicker = UIPickerView()
    var projectPicker = UIPickerView()

    var projectLists: [ProjectListModel] = []
    var isLoginEdit = false
    var firstAppear = true

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initCategoryTextField()
        setupDistrictTextField()

        if (EMISManager.shared.offlineMode) {
            projectTextField.isEnabled = true
            setupProjectTextField()
        } else {
            projectTextField.isEnabled = false
        }
        
        nameTextField.text = EMISManager.shared.userModel.value.user_name
        phoneTextField.text = EMISManager.shared.userModel.value.phone
        if projectModel != nil {
            projectTextField.text = projectModel!.name
        }
        timestamp = Int(Date().timeIntervalSince1970)
        dateTimeLabel.text = DateManager.dateChinaTransformTimestamp(timestamp)!
        addressTextFiled.attributedPlaceholder = NSAttributedString(string: "請輸入地址", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.darkGray])
        sampleTextView.backgroundColor = UIColor.white
        imagePicker.delegate = self
        buttonView.isHidden = false
        completeView.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (self.firstAppear) {
            
            LoadingHUD.shared.show()
            
            // 沒登入, 新增離線案件
            if (EMISManager.shared.offlineMode) {
                projectNameView.isHidden = true
                userNameView.isHidden = true
                userPhoneView.isHidden = true
                
                xPositionView.isHidden = true
                yPositionView.isHidden = true
                locationHeightConstraint.constant = 110
                desSampleButton.isHidden = true
                confimButton.setTitle("儲存", for: .normal)
            } else {
                projectNameView.isHidden = false
                userNameView.isHidden = false
                userPhoneView.isHidden = false
                
                xPositionView.isHidden = false
                yPositionView.isHidden = false
                locationHeightConstraint.constant = 190
                desSampleButton.isHidden = false
                confimButton.setTitle("確定送出", for: .normal)
            }
            
            // 登入後編輯離線案件
            if (isLoginEdit) {
                EMISManager.shared.projectList { [weak self] (datas) in
                    guard let self = self else { return }
                    self.projectLists = datas
                    if (!self.projectNameView.isHidden && self.projectTextField.text == "") {
                        self.projectTextField.text = self.projectLists.first?.name
                    }
                }
                
                uploadButton.isHidden = false
                
                projectNameView.isHidden = false
                userNameView.isHidden = false
                userPhoneView.isHidden = false
            }
            
            
            EMISManager.shared.categoryList { [weak self] (datas, injurys, disasters) in
                guard let self = self else { return }
                self.categoryList = datas
                if (self.categoryTextField.text == "") {
                    self.categoryTextField.text = self.categoryList.first?.category
                }
                self.showScopeFromModel(self.categoryList.first!.scope)
                
                self.injuryList = injurys
                self.disasterList = disasters
                
                
                if (EMISManager.shared.offlineMode) {
                    self.loadOfflineCaswData()
                }
                self.disasterCollectionView.reloadData()
                LoadingHUD.shared.dismiss()
            }
            
            EMISManager.shared.smartMsg() { [weak self] (datas) in
                guard let self = self else { return }
                self.smartMsgList = datas
            }
            
            EMISManager.shared.districtList { [weak self] (districtList) in
                guard let self = self else { return }
                self.districtList = districtList
                if (self.districtTextFiled.text == "") {
                    self.districtTextFiled.text = self.districtList.first?.text
                }
                
                if (EMISManager.shared.offlineMode) {
                    self.loadOfflineCaswData()
                }
            }
            
            let ret = EMISManager.shared.isConnectedToNetwork()
            if (!ret) {
                projectNameView.isHidden = true
                uploadButton.isHidden = true
            }
            firstAppear = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateCasualtyView()
        sampleTextView.addHandler(hasChinese: true, long: 200)
        sampleTextView.hasSpecial = true
        nameTextField.addHandler(hasChinese: true, long: 15)
        phoneTextField.addHandler(hasChinese: false, long: 15)
        latTextFiled.addHandler(hasChinese: false, long: 15)
        longTextFiled.addHandler(hasChinese: false, long: 15)
        addressTextFiled.addHandler(hasChinese: true, long: 30)
        textField_s1_1.addHandler(hasChinese: false, long: 20)
        textField_s2_1.addHandler(hasChinese: false, long: 20)
        textField_s2_2.addHandler(hasChinese: false, long: 20)
        textField_s3_1.addHandler(hasChinese: false, long: 20)
        textField_s3_2.addHandler(hasChinese: false, long: 20)
        textField_s3_3.addHandler(hasChinese: false, long: 20)
        textField_s1_1.addHandler(hasChinese: false, long: 5)
        textField_s2_1.addHandler(hasChinese: false, long: 5)
        textField_s2_2.addHandler(hasChinese: false, long: 5)
        textField_s3_1.addHandler(hasChinese: false, long: 5)
        textField_s3_2.addHandler(hasChinese: false, long: 5)
        textField_s3_3.addHandler(hasChinese: false, long: 5)
    }
    
    //MARK: - Privates
    private func showSmartSmg() {
        let model = categoryList[categorySelectedIndex]
        for data in smartMsgList {
            if (model.id == data.typeid) {
                sampleTextView.text = data.content
                break
            }
        }
    }
    
    private func showScopeFromModel(_ models: [ScopeModel]) {

        scopeViews_1.forEach {
            $0.isHidden = true
        }
        scopeViews_2.forEach {
            $0.isHidden = true
        }
        scopeViews_3.forEach {
            $0.isHidden = true
        }
        
        guard !EMISManager.shared.offlineMode else {
            return
        }
        
        textField_s1_1.text = ""
        textField_s2_1.text = ""
        textField_s2_2.text = ""
        textField_s3_1.text = ""
        textField_s3_2.text = ""
        textField_s3_3.text = ""
        
        if models.count == 1 {
            scopeViews_1.forEach {
                $0.isHidden = false
            }
        } else if models.count == 2 {
            scopeViews_2.forEach {
                $0.isHidden = false
            }
            label_s2_1.text = models[0].description
            label_s2_2.text = models[1].description
            label_s2_1_unit.text = models[0].unit
            label_s2_2_unit.text = models[1].unit
        } else if models.count == 3 {
            scopeViews_3.forEach {
                $0.isHidden = false
            }
            label_s3_1.text = models[0].description
            label_s3_2.text = models[1].description
            label_s3_3.text = models[2].description
            label_s3_1_unit.text = models[0].unit
            label_s3_2_unit.text = models[1].unit
            label_s3_3_unit.text = models[2].unit
        }
    }
    
    private func setupProjectTextField() {
        projectPicker.delegate = self
        projectPicker.dataSource = self

        projectTextField.inputView = projectPicker
        projectTextField.delegate = self
    }
    
    private func initCategoryTextField() {
        categoryPicker.delegate = self
        categoryPicker.dataSource = self

        
        categoryTextField.inputView = categoryPicker
        categoryTextField.delegate = self
    }
    
    private func setupDistrictTextField() {
        districtPicker.delegate = self
        districtPicker.dataSource = self
        
        districtTextFiled.inputView = districtPicker
        districtTextFiled.delegate = self
        addressTextFiled.attributedPlaceholder = NSAttributedString(string: "請輸入地址", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.darkGray])
    }

    
    
    func verifi() -> Bool {
        var verifi = true
        if projectTextField.text?.count == 0 && !projectNameView.isHidden {
            AlertManager.shared.showMessage(message: "請輸入專案名稱", title: "")
            verifi = false
        } else if nameTextField.text?.count == 0 && !EMISManager.shared.offlineMode {
            AlertManager.shared.showMessage(message: "請輸入報案人姓名", title: "")
            verifi = false
        } else if phoneTextField.text?.count == 0 && !EMISManager.shared.offlineMode {
            AlertManager.shared.showMessage(message: "請輸入聯絡電話", title: "")
            verifi = false
        } else if districtTextFiled.text?.count == 0 {
            AlertManager.shared.showMessage(message: "請輸入案件地點", title: "")
            verifi = false
        } else if addressTextFiled.text?.count == 0 {
            AlertManager.shared.showMessage(message: "請輸入案件地址", title: "")
            verifi = false
        } else if latTextFiled.text?.count == 0 && !EMISManager.shared.offlineMode {
            AlertManager.shared.showMessage(message: "請輸入案件Ｘ座標", title: "")
            verifi = false
        } else if longTextFiled.text?.count == 0 && !EMISManager.shared.offlineMode {
            AlertManager.shared.showMessage(message: "請輸入案件Ｙ座標", title: "")
            verifi = false
        } else if sampleTextView.text?.count == 0 {
            AlertManager.shared.showMessage(message: "請輸入現場狀況", title: "")
            verifi = false
        }
        return verifi
    }
    
    private func updateCasualtyView() {
        var injury_1 = 0
        var injury_2 = 0
        var injury_3 = 0
        var injury_4 = 0
        var injury_5 = 0
        injury_1Label.text = "\(injury_1)人"
        injury_2Label.text = "\(injury_2)人"
        injury_3Label.text = "\(injury_3)人"
        injury_4Label.text = "\(injury_4)人"
        injury_5Label.text = "\(injury_5)人"
        casualtyList.forEach {
            if $0.injury_id == "1" {
                injury_1 += 1
                injury_1Label.text = "\(injury_1)人"
            } else if $0.injury_id == "2" {
                injury_2 += 1
                injury_2Label.text = "\(injury_2)人"
            } else if $0.injury_id == "3" {
                injury_3 += 1
                injury_3Label.text = "\(injury_3)人"
            } else if $0.injury_id == "4" {
                injury_4 += 1
                injury_4Label.text = "\(injury_4)人"
            } else if $0.injury_id == "5" {
                injury_5 += 1
                injury_5Label.text = "\(injury_5)人"
            }
        }
        casualtyTableView.reloadData()
        DispatchQueue.main.async {
            self.casualtyTableViewHeight.constant = CGFloat(self.casualtyList.count * 160)
        }
    }
    
    func openLive() {
        self.navigationController?.popViewController(animated: true)
        UIApplication.tryURL(urls: [
            "fb://groups/426241551840753", // App
            "https://www.facebook.com/groups/426241551840753" // Website if app fails
        ])
    }
    
    func addNewCase(upload:Bool = false, handle: @escaping NewCaseFinidhedHandle) {
        if verifi() {
            if !isCompleted {
                var models = categoryList[categorySelectedIndex].scope
                if models.count == 1 {
                    models[0].value = textField_s1_1.text!
                } else if models.count == 2 {
                    models[0].value = textField_s2_1.text!
                    models[1].value = textField_s2_2.text!
                } else if models.count == 3 {
                    models[0].value = textField_s3_1.text!
                    models[1].value = textField_s3_2.text!
                    models[2].value = textField_s3_3.text!
                }
                scope = models
                
                var add_case_model = AddCaseModel(data: [:])
                if projectModel != nil {
                    add_case_model.project_id = projectModel!.id
                    add_case_model.projectString = projectModel!.name
                } else {
                    for project in projectLists {
                        if (projectTextField.text == project.name) {
                            add_case_model.project_id = project.id
                            add_case_model.projectString = project.name
                            break
                        }
                    }
                }
                add_case_model.receive_date = DateManager.ApiDateTransformTimestamp(self.timestamp)!
                add_case_model.request_person = nameTextField.text!
                add_case_model.request_person_phone = phoneTextField.text!
                add_case_model.degree = disasterList[disasterSelectedIndex].id
                add_case_model.category = categoryList[categorySelectedIndex].id
                add_case_model.categoryString = categoryList[categorySelectedIndex].category
                add_case_model.description = sampleTextView.text
                add_case_model.influence_scope = scope
                if location != nil {
                    add_case_model.location = location
                } else {
                    location = MapInfo(data: [:])
                    
                    for dis in districtList {
                        if (dis.text == districtTextFiled.text) {
                            location.district_id = dis.id
                            break
                        }
                    }
                    
                    location.district = self.districtTextFiled.text ?? ""
                    location.address = self.addressTextFiled.text ?? ""
                    add_case_model.location = location
                }
                
                var removeIndex = 0
                for model in imageList {
                    if model.type == .Defaults {
                        imageList.remove(at: removeIndex)
                    }
                    removeIndex += 1
                }
                
                
                if (EMISManager.shared.offlineMode && !upload) {
                    saveOfflineCaseData(model: add_case_model)
                    handle(true)
                    
                } else {
                    
                    LoadingHUD.shared.show()
                                        
                    EMISManager.shared.addCase(add_case_model: add_case_model, casualty_models: casualtyList, imageList: imageList) { [weak self] (responseModel) in
                        guard let self = self else { return }
                        LoadingHUD.shared.dismiss()
                        if responseModel.status {
                            self.isCompleted = true
                            self.buttonView.isHidden = false
                            self.completeView.isHidden = true
                            if (upload) {
                                if let case_no = responseModel.data["case_no"] as? String {
                                    add_case_model.case_no = case_no
                                    add_case_model.upload = true
                                    self.saveOfflineCaseData(model: add_case_model)
                                }
                            }
                            handle(true)
                        } else {
                            handle(false)
                            AlertManager.shared.showMessage(message: "新增案件失敗，請聯絡相關部門", title: "")
                        }
                    }
                }
            }
        }
    }
    
    func saveOfflineCaseData(model: AddCaseModel?) {
        var newData = OfflineCaseModel(data: [:])
        var offlineDataList : [OfflineCaseModel]  = []
        
        if let dataList = UserDefaults.standard.object(forKey: OFFLINE_KEY) as? [[String : Any]]
        {
            dataList.forEach {
                offlineDataList.append(OfflineCaseModel(data: $0))
            }
        }
        
//        print("## ============== local offline case ==============")
//        for data in offlineDataList {
//            print("## addCaseModel:\(data.addCaseModel)")
//            print("## casualtyModels:\(data.casualtyModels)")
//            print("## imageList:\(data.imageList)")
//        }
//        print("## ==========================================")
        
        // nil mean delete case
        guard model != nil else {
            offlineDataList.remove(at: offlineDataIndex)
            var saveDataList = [[String:Any]]()
            for data in offlineDataList {
                saveDataList.append(data.dictionary())
            }
            UserDefaults.standard.setValue(saveDataList, forKey: OFFLINE_KEY)
            
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        var offlineImageList: [String] = []
        for file in imageList {
            let url = file.url
            if let path = url?.absoluteString {
                let fileName = (path as NSString).lastPathComponent
                offlineImageList.append(fileName)
            }
        }
        
        newData.addCaseModel = model

        newData.casualtyModels = casualtyList
        newData.imageList = offlineImageList
        
        if (offlineDataIndex >= 0) {
            offlineDataList[offlineDataIndex] = newData
        } else {
            offlineDataList.insert(newData, at: 0)
        }
        
        var saveDataList = [[String:Any]]()
        for data in offlineDataList {
            saveDataList.append(data.dictionary())
        }
        UserDefaults.standard.setValue(saveDataList, forKey: OFFLINE_KEY)
    }
    
    private func loadOfflineCaswData() {
        guard let offlineModel = offlineModel else {
            deleteCaseButton.isHidden = true
            return
        }
        deleteCaseButton.isHidden = false


        if (offlineModel.addCaseModel.project_id != "" && offlineModel.addCaseModel.projectString != "") {
            var model = ProjectListModel(data: [:])
            model.id = offlineModel.addCaseModel.project_id
            model.name = offlineModel.addCaseModel.projectString
            projectModel = model
            
            projectTextField.text = projectModel!.name
        }
        

//        add_case_model.receive_date = DateManager.ApiDateTransformTimestamp(self.timestamp)!
        dateTimeLabel.text = offlineModel.addCaseModel.receive_date

//        add_case_model.request_person = nameTextField.text!
//        nameTextField.text = offlineModel.addCaseModel.request_person
       
//        add_case_model.request_person_phone = phoneTextField.text!
//        phoneTextField.text = offlineModel.addCaseModel.request_person_phone
        
//        add_case_model.degree = disasterList[disasterSelectedIndex].id
        for i in 0..<disasterList.count {
            if (disasterList[i].id == offlineModel.addCaseModel.degree) {
                disasterSelectedIndex = i
                disasterCollectionView.reloadData()
                break
            }
        }
//        add_case_model.category = categoryList[categorySelectedIndex].id
        for i in 0..<categoryList.count {
            if (categoryList[i].id == offlineModel.addCaseModel.category) {
                categorySelectedIndex = i
                categoryTextField.text = categoryList[categorySelectedIndex].category
                break
            }
        }
        
        // no need, just for UI
//        add_case_model.categoryString = categoryList[categorySelectedIndex].category
        
//        add_case_model.description = sampleTextView.text
        sampleTextView.text = offlineModel.addCaseModel.description
        
        // no need
//        add_case_model.influence_scope = scope
        
//        add_case_model.location = location
        self.districtTextFiled.text = offlineModel.addCaseModel.location.district
        self.addressTextFiled.text = offlineModel.addCaseModel.location.address
//        location = offlineModel.addCaseModel.location
        
        districtTextFiled.text = offlineModel.addCaseModel.location.district
        addressTextFiled.text = offlineModel.addCaseModel.location.address
        
        casualtyList = offlineModel.casualtyModels
        updateCasualtyView()

        imageList = [FileModel()]

        for fileName in offlineModel.imageList {
            if let image = MediaManager.shared.getImage(name: fileName) {
                addImage(image)
            }
        }
    }
    //MARK: - IBAction
    @IBAction func onClickDeleteCase(_ sender: Any) {
        ViewRouter.shared.presentPromptFrom(viewController: self, title:"刪除案件" , message: "確認要刪除該案件嗎?", type: .Confirm) { (buttonEvent) in
            if buttonEvent == .confim {
               print("刪除離線案件")
                self.saveOfflineCaseData(model: nil)
            }
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        if (EMISManager.shared.offlineMode) {
            ViewRouter.shared.presentPromptFrom(viewController: self, message: "按下「確認」將返回離線作業清單，您所輸入的資料將全部捨棄；若想繼續進行作業，請按「取消」。", type: .Confirm) { (buttonEvent) in
                if buttonEvent == .confim {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onClickUpload(_ sender: Any) {
        print("上傳離線案件")
        if (EMISManager.shared.offlineMode && EMISManager.shared.isLogin.value) {
            let name = projectTextField.text!
            ViewRouter.shared.presentPromptFrom(viewController: self, title:"上傳案件" , message: "該案件將上傳至「\(name)」專案中，確認無誤請按確認上傳", type: .Confirm) { (buttonEvent) in
                if buttonEvent == .confim {
                    self.addNewCase(upload: true) { (isSuccess) in
                        if isSuccess {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onClickCategory(_ sender: Any) {
        categoryTextField.becomeFirstResponder()
    }
    
    @IBAction func onClickLocation(_ sender: Any) {
        if (EMISManager.shared.offlineMode) {
            districtTextFiled.becomeFirstResponder()
        } else {
            ViewRouter.shared.presentToMapViewFrom(viewController: self, mapInfo: location) { [weak self] (isConfim, mapInfo) in
                guard let self = self else { return }
                if isConfim {
                    if mapInfo != nil {
                        self.location = mapInfo
                        self.latTextFiled.text = "\(mapInfo?.latitude ?? 0)"
                        self.longTextFiled.text = "\(mapInfo?.longitude ?? 0)"
                        self.districtTextFiled.text = "\(mapInfo?.district ?? "")"
                        self.addressTextFiled.text = "\(mapInfo?.address ?? "")"
                    }
                }
            }
        }
    }
    
    @IBAction func onClickDesSample(_ sender: Any) {
        LoadingHUD.shared.show()
        EMISManager.shared.sampleList(category_id: categoryList[categorySelectedIndex].id) { [weak self] (sampleList) in
            guard let self = self else { return }
            LoadingHUD.shared.dismiss()
            if sampleList.count > 0 {
                ViewRouter.shared.presentToSampleViewFrom(viewController: self, sampleList: sampleList) { (event, sampleModel) in
                    if event == .confim {
                        if sampleModel != nil {
                            self.sampleTextView.text = sampleModel?.situ
                        }
                    }
                }
            } else {
                ViewRouter.shared.presentPromptFrom(viewController: self, message: "該案件類別沒有現場狀況範本", type: .Alert) { (buttonEvent) in

                }
            }
        }
    }
    
    @IBAction func onClickCasualty(_ sender: Any) {
        ViewRouter.shared.presentToCasualtyViewFrom(viewController: self, model: nil) { (isConfim, casualtyModel) in
            if isConfim {
                self.casualtyList.append(casualtyModel!)
                self.updateCasualtyView()
            }
        }
    }
    
    @IBAction func onClickAddPhoto(_ sender: Any) {
        let btn = sender as! UIButton
        imageIndex = btn.tag
        
        let alertController = UIAlertController(
            title: "",
            message: "請選擇圖片...",
            preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(
            title: "取消",
            style: .cancel,
            handler: nil)
        
        alertController.addAction(cancelAction)
        let cameraAction = UIAlertAction(title: "拍照", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(cameraAction)
        
        let photoAction = UIAlertAction(title: "從相簿挑選", style: .default) { (alertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true)
        }
        
        alertController.addAction(photoAction)
        
        self.present( alertController, animated: true, completion: nil)
    }
    
    @IBAction func onClickConfim(_ sender: Any) {
        if (EMISManager.shared.offlineMode) {
            self.addNewCase { (isSuccess) in
                if isSuccess {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            ViewRouter.shared.presentPromptFrom(viewController: self, message: "上傳新增檔案後，是否進行「直播」？", type: .YesAndNo) { (buttonEvent) in
                self.hasLive = buttonEvent == .confim
                self.addNewCase { (isSuccess) in
                    if isSuccess{
                        ViewRouter.shared.presentPromptFrom(viewController: self, message: "新增案件成功", type: .Alert) { (buttonEvent) in
                            if self.hasLive {
                                self.openLive()
                            } else {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onClickLive(_ sender: Any) {
        if !isCompleted {
            ViewRouter.shared.presentPromptFrom(viewController: self, message: "是否先上傳案件資訊，再進行「直播」？", type: .Confirm) { (buttonEvent) in
                if buttonEvent == .confim {
                    self.addNewCase { (isSuccess) in
                        if isSuccess{
                            ViewRouter.shared.presentPromptFrom(viewController: self, message: "新增案件成功", type: .Alert) { (buttonEvent) in
                                self.openLive()
                            }
                        }
                    }
                }
            }
        } else {
            self.openLive()
        }
    }
}
//MARK: - UIPickerView Delegate
extension NewCaseController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView == categoryPicker) {
            return categoryList.count
        } else if (pickerView == districtPicker) {
            return districtList.count
        } else {
            return projectLists.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView == categoryPicker) {
            return categoryList[row].category
        } else if (pickerView == districtPicker) {
            return districtList[row].text
        } else {
            return projectLists[row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView == categoryPicker) {
            categorySelectedIndex = row
            categoryTextField.text = categoryList[row].category
            showScopeFromModel(categoryList[row].scope)
            showSmartSmg()
        } else if (pickerView == districtPicker) {
            districtSelectedIndex = row
            districtTextFiled.text = districtList[row].text
        } else {
            projectSelectedIndex = row
            projectTextField.text = projectLists[row].name
        }

    }
}

extension NewCaseController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            if (textField == categoryTextField) {
                categorySelectedIndex = 0
                categoryTextField.text = categoryList.first?.category
                showScopeFromModel(categoryList.first!.scope)
            } else if (textField == districtTextFiled){
                districtSelectedIndex = 0
                districtTextFiled.text = districtList.first?.text
            } else if (textField == projectTextField) {
                projectSelectedIndex = 0
                projectTextField.text = projectLists.first?.name
            }
        }
    }
}

//MARK: - UICollectionView Delegate
extension NewCaseController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == disasterCollectionView {
            disasterSelectedIndex = indexPath.row
            collectionView.reloadData()
        }
    }
}

//MARK: - UICollectionViewDataSource
extension NewCaseController:  UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == disasterCollectionView {
            return disasterList.count
        } else if collectionView == imageCollectionView {
            return imageList.count
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == disasterCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DisasterCell", for: indexPath) as! DisasterCell
            let model = disasterList[indexPath.row]
            
            cell.titleLabel.text = model.text
            cell.setSelected = disasterSelectedIndex == indexPath.row
            return cell
        }
        
        if collectionView == imageCollectionView {
            if imageList[indexPath.row].type == .Defaults {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCell", for: indexPath) as! AddCell
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
                cell.imageView.image = imageList[indexPath.row].image
                cell.deleteHandler = { [weak self] in
                    guard let self = self else { return }
                    self.deleteImage(index: indexPath.row)
                }
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    func addImage(_ image: UIImage) {
        let url = MediaManager.shared.saveImage(image: image)

        var model = FileModel()
        model.type = .Image
        model.image = image
        model.url = url
        if imageList.count ==  3 {
            imageList.remove(at: 2)
        }
        imageList.insert(model, at: 0)
        imageCollectionView.reloadData()
    }
    
    func deleteImage(index: Int) {
        if imageList.count == 3 {
            if imageList.last?.type != .Defaults {
                imageList.append(FileModel())
            }
        }
        self.imageList.remove(at: index)
        self.imageCollectionView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension NewCaseController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return casualtyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let casualtyCell: CasualtyCell = tableView.dequeueReusableCell(withIdentifier: "CasualtyCell", for: indexPath) as! CasualtyCell
        let model = casualtyList[indexPath.row]
        casualtyCell.nameLabel.text = model.name
        casualtyCell.genderLabel.text = model.gender
        casualtyCell.ageLabel.text = model.age + "歲"
        casualtyCell.injuryLabel.text = model.injury
        casualtyCell.descriptionLabel.text = model.description
        casualtyCell.casualtyDeleteHandle = {
            self.casualtyList.remove(at: indexPath.row)
            self.updateCasualtyView()
        }
        
        casualtyCell.casualtyEditHandle = {
            ViewRouter.shared.presentToCasualtyViewFrom(viewController: self, model: model) { [weak self] (isConfim, casualtyModel) in
                guard let self = self else { return }
                if isConfim {
                    self.casualtyList[indexPath.row] = casualtyModel!
                    self.updateCasualtyView()
                }
            }
        }
        return casualtyCell
    }
    
    
}

extension NewCaseController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = casualtyList[indexPath.row]
        ViewRouter.shared.presentToCasualtyDetalFrom(viewController: self, text: model.description)
    }
}

extension NewCaseController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image: UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        addImage(image)
        //取得照片後將imagePickercontroller dismiss
        picker.dismiss(animated: true, completion: nil)
    }
}
