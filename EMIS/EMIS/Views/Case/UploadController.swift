//
//  UploadController.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/22.
//

import UIKit

class UploadController: UIViewController {
    var projectModel: ProjectListModel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var renkLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var caseList: [UploadModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    lazy var manager: EMISManager = {
        return EMISManager.shared
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = projectModel.name
        dateTimeLabel.text = projectModel.creation_date
        renkLabel.text = projectModel.level
        categoryLabel.text = projectModel.category
        dateTimeLabel.text = "成立時間：\(projectModel.creation_date)"
        addObservable()
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}

//MARK: - Observable handling
extension UploadController {
    func addObservable() {
        manager.uploadingList.addObserver(fireNow: true) { [weak self] (list) in
            guard let self = self else { return }
            var caseList: [UploadModel] = []
            list.forEach {
                if $0.projectId == self.projectModel.id {
                    caseList.append($0)
                }
            }
            self.caseList = caseList
        }
    }
}

// MARK: - UITableViewDataSource
extension UploadController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return caseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let caseListCell: CaseListCell = tableView.dequeueReusableCell(withIdentifier: "CaseListCell", for: indexPath) as! CaseListCell
        let model = caseList[indexPath.row]
        caseListCell.noLabel.text = model.no
        caseListCell.categoryLabel.text = model.category
        caseListCell.dateLabel.text = model.receive_date
        caseListCell.addressLabel.text = model.location_district + model.location_address
        caseListCell.statusLabel.text = indexPath.row == 0 ? "上傳中" : "待上傳"
        caseListCell.maskImageView.image = indexPath.row == 0 ? UIImage(named: "list_img_ing") : UIImage(named: "list_img_under")
        return caseListCell
    }
    
    
}

extension UploadController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 156
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
