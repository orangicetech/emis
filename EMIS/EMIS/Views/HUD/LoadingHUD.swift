//
//  LoadingHUD.swift
//  Cupola360DashCam
//
//  Created by Polo iMac on 2020/2/21.
//  Copyright © 2020 Cupola360 Inc. All rights reserved.
//

import UIKit
enum LoadingLevel: Int {
    case normal = 10
    case ui
    case router
    case manager
}
class LoadingHUD: HUDView {
    static let shared = LoadingHUD()
    
    var loadingImageView: UIImageView!
    var level: LoadingLevel = .normal
    
    func show(level: LoadingLevel) {
        self.level = level
        show()
    }
    
    func dismiss(level: LoadingLevel) {
        if level == .router {
            self.level = .normal
        }
        dismiss()
    }
    
    func show() {
        setupLoadingImageView()
        present(contentView: loadingImageView)
    }
    
    func dismiss() {
        if self.level == .normal {
            close(delay: 0)
        }
    }
    
    func setProgressStatus(status: ProgressStatus) {
        
    }
    
    private func setupLoadingImageView() {
        if loadingImageView == nil {
            loadingImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 56, height: 56))
            loadingImageView.image = UIImage(named: "loading_img_loading_1")
            var images = [UIImage]()
            for i in 1...24 {
                let imgName = String(format: "loading_img_loading_%2d", i)
                let img = UIImage(named: imgName)
                if let uiImage = img {
                    images.append(uiImage)
                }
            }
            
            loadingImageView.animationImages = images
            loadingImageView.animationDuration = 0.4
            loadingImageView.startAnimating()
        }
    }
}
