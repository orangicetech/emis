//
//  HUDView.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/1.
//

import UIKit

class HUDView: UIView {
    var contentView: UIView!
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var backgroundView: UIView!
    
    func present(contentView: UIView) {
        let shareWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        self.alpha = 1
        
        self.contentView = contentView
        if shareWindow != nil {
            let huds = shareWindow!.subviews.filter{$0 is HUDView}
            if huds.count > 0 {
                huds.forEach{
                    $0.removeFromSuperview()
                }
            }
            
            shareWindow!.addSubview(self)
            
            subviews.forEach{
                $0.removeFromSuperview()
            }
            
            contentView.alpha = 0
            contentView.center = center
            addSubview(contentView)
            
            UIView.animate(withDuration: 0.3) {
                contentView.alpha = 1
            }
        }
    }
    
    func close(delay: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            UIView.animate(withDuration: 0.2, animations: {
                self.contentView?.alpha = 0
            }) { (completion) in
                self.contentView?.removeFromSuperview()
                self.contentView = nil
                
                UIView.animate(withDuration: 0.1, animations: {
                    self.alpha = 0
                }) { (completion) in
                    self.removeFromSuperview()
                }
            }
        }
    }
    
    private func topMostController() -> UIViewController? {
        var topController = UIApplication.shared.windows[0].rootViewController
        while let presentedViewController = topController?.presentedViewController {
            topController = presentedViewController
        }
        return topController
    }
}
