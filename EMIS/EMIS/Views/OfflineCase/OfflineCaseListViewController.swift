//
//  OfflineCaseListViewController.swift
//  EMIS
//
//  Created by Jimmy on 2022/11/10.
//

import UIKit
let OFFLINE_KEY = "OFFLINE_DATA"
class OfflineCaseListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    //var projectModel: ProjectListModel!
    
    var offlineDataList : [OfflineCaseModel]  = []
    var categoryList: [CategoryModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        EMISManager.shared.categoryList { [weak self] (datas, injurys, disasters) in
            guard let self = self else { return }
            self.categoryList = datas
            self.loadOfflineData()

            self.tableView.reloadData()
         
        }
    }
    
    func loadOfflineData() {
        offlineDataList.removeAll()

        if let dataList = UserDefaults.standard.object(forKey: OFFLINE_KEY) as? [[String : Any]]
        {
            dataList.forEach {
                offlineDataList.append(OfflineCaseModel(data: $0))
            }
            tableView.reloadData()
        }
        
//        print("## ============== local offline case \(offlineDataList.count) ==============")
//        for data in offlineDataList {
//            print("## addCaseModel:\(data.addCaseModel)")
//            print("## casualtyModels:\(data.casualtyModels)")
//            print("## imageList:\(data.imageList)")
//        }
//        print("## ==========================================")
    }
    
    @IBAction func onClickNewCase(_ sender: Any) {
        ViewRouter.shared.showNewCaseFrom(viewController: self, model: nil)

        
//        if (!EMISManager.shared.isLogin.value) {
//            ViewRouter.shared.showNewCaseFrom(viewController: self, model: nil)
//        } else {
//            let ret = EMISManager.shared.isConnectedToNetwork()
//            if (ret) {
//                ViewRouter.shared.presentWarningFrom(viewController: self, title:"錯誤訊息", message: "離線作業「新增案件」只能在無網路情況下使用!", type: .Alert) { (buttonEvent) in
//                    print("離線作業「新增案件」只能在無網路情況下使用!")
//                }
//            } else {
//                ViewRouter.shared.showNewCaseFrom(viewController: self, model: nil)
//            }
//        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        EMISManager.shared.offlineMode = false
        if (EMISManager.shared.isLogin.value) {
            self.navigationController?.popViewController(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
}

// MARK: - UITableViewDataSource
extension OfflineCaseListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offlineDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfflineCaseListCell", for: indexPath) as! OfflineCaseListCell
        let model = offlineDataList[indexPath.row]
        
        cell.noLabel.text = model.addCaseModel.case_no
        cell.projectLabel.text = model.addCaseModel.projectString
        
        cell.categoryLabel.text = model.addCaseModel.categoryString
        cell.dateLabel.text = model.addCaseModel.receive_date
        cell.addressLabel.text = model.addCaseModel.location.district + model.addCaseModel.location.address
        cell.upload = model.addCaseModel.upload
        return cell
    }
}

extension OfflineCaseListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = offlineDataList[indexPath.row]
        if (!model.addCaseModel.upload) {
            ViewRouter.shared.showOfflineCaseFrom(viewController: self,index: indexPath.row,model: model)
        }
    }
}
