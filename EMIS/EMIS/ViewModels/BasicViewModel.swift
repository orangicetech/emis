//
//  BasicViewModel.swift
//  Cupola360
//
//  Created by Polo iMac on 2020/7/8.
//  Copyright © 2020 Cupola360 Inc. All rights reserved.
//

import UIKit

enum ViewModelStatus: Int {
    case Idle
    case Loading
    case LoadingComplete
    case Error
}

class BasicViewModel: NSObject {
    var status = Observable<ViewModelStatus>(value: .Idle)
}
