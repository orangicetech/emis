//
//  ProjectViewModel.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/26.
//

import UIKit

class ProjectListViewModel: BasicViewModel {
    static let shared = ProjectListViewModel()
    
    var data: [ProjectListModel] = []
    
    func getDateList() {
        status.value = .Loading
        
        data.removeAll()

        EMISManager.shared.projectList { [weak self] (datas) in
            guard let self = self else { return }
            self.data = datas
            self.status.value = .LoadingComplete
        }
    }
}
