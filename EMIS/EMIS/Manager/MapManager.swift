//
//  MapManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

typealias ConvertCompleted = (_ lat: Float, _ lng: Float, _ city: String) -> Void
typealias ConvertDistrictCompleted = (_ district: String) -> Void

class MapManager: NSObject {
    
    static let shared = MapManager()

    let API_KEY: String = "AIzaSyDhDSgoMpsVe9RRap3LqdJcIx47Nx8VJUg"
    let geocode_API: String = "https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=%@"
    let geocode_district_API: String = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=%@"
    
    var locationManager = CLLocationManager()
    
    func startUpdatingLocation() {
        locationManager.delegate = self  //委派給ViewController
        locationManager.desiredAccuracy = kCLLocationAccuracyBest  //設定為最佳精度
        locationManager.requestWhenInUseAuthorization()  //user授權
        locationManager.startUpdatingLocation()
    }
    
    func convertAddressToLocation(_ address: String, complete: @escaping ConvertCompleted) {
        let url: String = String(format: geocode_API, address, API_KEY).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let request = Alamofire.request(url)
            request.responseJSON { (response) in
                if response.result.isSuccess {
                    log.debug("Convert address to location success")
                    do {
                        let json = try JSON(data: response.data!)
                        let location = json["results"][0]["geometry"]["location"]
                        let lat: Float = location["lat"].floatValue
                        let lng: Float = location["lng"].floatValue
                        
                        var name: String = ""
                        let components = json["results"][0]["address_components"]
                        for info in components.arrayValue {
                            for types in info["types"].arrayValue {
                                if types == "administrative_area_level_3" {
                                    name = info["long_name"].string!
                                }
                            }
                        }
                        
                        complete(lat, lng, name)
                    } catch {
                        log.error("Convert address to location failure")
                    }
                } else {
                    log.warning("Convert address to location failure")
                }
            }
    }
    
    func convertLocationToDistrict(coordinate: CLLocationCoordinate2D, complete: @escaping ConvertDistrictCompleted) {
        let url: String = String(format: geocode_district_API, coordinate.latitude, coordinate.longitude, API_KEY).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let request = Alamofire.request(url)
            request.responseJSON { (response) in
                if response.result.isSuccess {
                    log.debug("Convert location to district success")
                    do {
                        let json = try JSON(data: response.data!)
                        let components = json["results"][0]["address_components"]
                        for info in components.arrayValue {
                            for types in info["types"].arrayValue {
                                if types == "administrative_area_level_3" {
                                    let name = info["long_name"].string
                                    complete(name!)
                                }
                            }
                        }
                    } catch {
                        log.error("Convert location to district failure")
                    }
                } else {
                    log.warning("Convert address to location failure")
                }
            }
    }
}

extension MapManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //An array of CLLocation objects containing the location data.
    let userLocation: CLLocation = locations[0] //最新的位置在[0]
        EMISManager.shared.locationModel.value = LocationModel(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
//    self.latitube.text = String(userLocation.coordinate.latitude)
//    self.longitube.text = String(userLocation.coordinate.longitude)
//    self.course.text = String(userLocation.course)
//    self.speed.text = String(userLocation.speed)
//        self.altitube.text = String(userLocation.altitude)
        
    }

}
