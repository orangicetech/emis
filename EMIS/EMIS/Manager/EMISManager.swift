//
//  EMISManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

import UIKit
import Alamofire
import CoreLocation
import AVFoundation
import FirebaseRemoteConfig
import SystemConfiguration

enum UpdateStatus: Int {
    case Nothing
    case Update
    case ForceUpdate
}

typealias ResultCompletion = (_ success: ResponseModel) -> Void
typealias ProjectListCompletion = (_ data: [ProjectListModel]) -> Void
typealias CaseListCompletion = (_ data: [CaseListModel]) -> Void
typealias CaseDetailCompletion = (_ data: CaseDetailModel) -> Void
typealias CategoryListCompletion = (_ datas: [CategoryModel], _ injurys: [InjuryModel], _ disasters: [DisasterModel]) -> Void
typealias DistrictListCompletion = (_ data: [DistrictModel]) -> Void
typealias DesSampleListCompletion = (_ data: [DesSampleModel]) -> Void
typealias CasualtyListCompletion = (_ data: [CasualtyModel]) -> Void
typealias PushTypeCompletion = (_ data: [PushTypeModel]) -> Void
typealias PushLogCompletion = (_ data: [PushLogModel]) -> Void
typealias ReplyMessageCompletion = (_ data: [ReplyMessageModel]) -> Void
typealias ReplyCompletion = (_ data: ReplyMessageModel) -> Void
typealias CaseSmartMsgCompletion = (_ data: [CaseSmartMsg]) -> Void

class EMISManager: NSObject {
    static let shared = EMISManager()
    
    var userModel = Observable<UserModel>(value: UserModel(data: [:]))
    
    var isLogin = Observable<Bool>(value: false)
    
    var locationModel = Observable<LocationModel>(value: LocationModel(latitude: 0, longitude: 0))
    
    var districtList: [DistrictModel] = []
    
    var injurysList: [InjuryModel] = []
    
    var push_token: String = ""
    
    let heartbeatInterval = DispatchTimeInterval.seconds(600)
    
    var heartbeatTimer: DispatchSourceTimer!
    
    var progressResult: Progress!
    
    var uploadingList = Observable<[UploadModel]>(value: [])
    
    var remoteConfig: RemoteConfig!
    
    var offlineMode: Bool = false
    var offlineEditMode: Bool = false
    var replyno: Int = -1
    var pushno: Int = -1
    var pushtype: Int = -1
    var downloadUrl: String = ""
    func setupRemoteConfig() {
        
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        
        remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(fromPlist: "remote_config_defaults")
        
        remoteConfig.fetch(withExpirationDuration: 0, completionHandler: { [weak self] (status, error) in
            guard let self = self else { return }

            if status == .success {
                self.remoteConfig.activate { changed, error in
                    print("remoteConfig changed:\(changed)")
//                    let a = remoteConfig.configValue(forKey: "case_category").jsonValue
//                    let b = remoteConfig.configValue(forKey: "case_address_district").jsonValue
                }
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
        })
    }
    
    func checkUpdate() -> UpdateStatus {
        var status: UpdateStatus = .Nothing
        
        let dictionary = Bundle.main.infoDictionary!
        let localBuild = dictionary["CFBundleVersion"] as! String
        
        if let json = remoteConfig.configValue(forKey: "app_info_ios").jsonValue as? [String : Any] {
            if let info = json["data"] as? [String : Any] {
                var build = "0"
                var force = false
                
                if let data = info["version_code"] as? String {
                    build = data
                }
                
                if let data = info["force_update"] as? Bool {
                    force = data
                }
                
                if let data = info["download_url"] as? String {
                    downloadUrl = data
                }
                
                if (build > localBuild) {
                    if (force) {
                        status = .ForceUpdate
                    } else {
                        status = .Update
                    }
                } else {
                    status = .Nothing
                }
            }
        }
        
        return status
    }
    
    func checkLogin() -> Bool {
        if isLogin.value != UserManager.shared.userModel().isValid {
            isLogin.value = UserManager.shared.userModel().isValid
        }
        self.userModel.value = UserModel(data: UserManager.shared.userModel().dictionary())

        return UserManager.shared.userModel().isValid
    }
    
    func login(username: String, password: String, result: @escaping ResultCompletion) {
        EMISAPIManager.shared.login(username: username, password: password) { [weak self] (apiResult) in
            guard let self = self else { return }
            switch apiResult {
            case .success(let response):
                log.info("Login API success")
                if response.status {
                    self.userModel.value = UserModel(data: response.data)
                    UserManager.shared.saveUserModel(UserModel(data: response.data))
                    self.isLogin.value = true
                }
                result(response)
            case .failure(let error):
                log.error("Login API failure: \(error)")
                result(ResponseModel(error: .resultFail))
            }
        }
    }
    
    func logout(result: @escaping ResultCompletion){
        EMISAPIManager.shared.logout { [weak self] (apiResult) in
            guard let self = self else { return }
            switch apiResult {
            case .success(let response):
                log.info("Logout API success")
                self.cleanUser()
                result(response)
            case .failure(let error):
                log.error("Logout API failure: \(error)")
                self.cleanUser()
                result(ResponseModel(error: error))
            }
        }
    }
    
    func cleanUser() {
        self.userModel.value = UserModel(data: [:])
        UserManager.shared.saveUserModel(self.userModel.value)
        self.isLogin.value = false
        self.offlineMode = true
        self.offlineEditMode = false
    }
    
    func projectList(result: @escaping ProjectListCompletion) {
        EMISAPIManager.shared.projectList { (apiResult) in
            var datas: [ProjectListModel] = []
            switch apiResult {
            case .success(let response):
                log.info("Project list API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(ProjectListModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("Project list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func caseList(project_id: String, result: @escaping CaseListCompletion) {
        EMISAPIManager.shared.caseList(project_id: project_id) { (apiResult) in
            var datas: [CaseListModel] = []
            switch apiResult {
            case .success(let response):
                log.info("Case list API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(CaseListModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("Case list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func caseDetail(case_no: String, result: @escaping CaseDetailCompletion) {
        EMISAPIManager.shared.caseDetail(case_no: case_no) { (apiResult) in
            var data: CaseDetailModel = CaseDetailModel(data: [:])
            switch apiResult {
            case .success(let response):
                log.info("Case Detail API success: \(response.status)")
                if response.status {
                    data = CaseDetailModel(data: response.data)
                }
                result(data)

            case .failure(let error):
                log.error("Case Detail API failure: \(error)")
                result(CaseDetailModel(data: [:]))
            }
        }
    }
    
    func categoryList(result: @escaping CategoryListCompletion) {
        if (offlineMode) {
            let json = remoteConfig.configValue(forKey: "case_category").jsonValue
            let response = ResponseModel(data: json as! [String : Any])
            
            var datas: [CategoryModel] = []
            var injurys: [InjuryModel] = []
            var disasters: [DisasterModel] = []
            
            response.datas.forEach {
                if let data = $0 as? [String : Any] {
                    var model = CategoryModel(data: data)
                    var scope:[ScopeModel] = []
                    if let scopeList = data["scope"] as? [[String : Any]] {
                        scopeList.forEach {
                            scope.append(ScopeModel(data: $0))
                        }
                    }
                    model.scope = scope
                    datas.append(model)
                }
            }
            
            response.data_injury.forEach {
                if let data = $0 as? [String : Any] {
                    let model = InjuryModel(data: data)
                    injurys.append(model)
                }
            }
            
            response.data_disaster.forEach {
                if let data = $0 as? [String : Any] {
                    let model = DisasterModel(data: data)
                    disasters.append(model)
                }
            }
            self.injurysList = injurys
            result(datas, injurys, disasters)
            
        } else {
            EMISAPIManager.shared.categoryList { (apiResult) in
                var datas: [CategoryModel] = []
                var injurys: [InjuryModel] = []
                var disasters: [DisasterModel] = []
                switch apiResult {
                case .success(let response):
                    log.info("Category list API success: \(response.status)")
                    if response.isValid {
                        response.datas.forEach {
                            if let data = $0 as? [String : Any] {
                                var model = CategoryModel(data: data)
                                var scope:[ScopeModel] = []
                                if let scopeList = data["scope"] as? [[String : Any]] {
                                    scopeList.forEach {
                                        scope.append(ScopeModel(data: $0))
                                    }
                                }
                                model.scope = scope
                                datas.append(model)
                            }
                        }
                        response.data_injury.forEach {
                            if let data = $0 as? [String : Any] {
                                let model = InjuryModel(data: data)
                                injurys.append(model)
                            }
                        }
                        
                        response.data_disaster.forEach {
                            if let data = $0 as? [String : Any] {
                                let model = DisasterModel(data: data)
                                disasters.append(model)
                            }
                        }
                        self.injurysList = injurys
                        result(datas, injurys, disasters)
                    } else {
                        AlertManager.shared.showMessage(message: response.message, title: "")
                    }
                case .failure(let error):
                    log.error("Category list API failure: \(error)")
                    result(datas, injurys, disasters)
                }
            }
        }
    }
    
    func districtList(result: @escaping DistrictListCompletion) {
        if (offlineMode) {
            let json = remoteConfig.configValue(forKey: "case_address_district").jsonValue
            let response = ResponseModel(data: json as! [String : Any])
            var datas: [DistrictModel] = []

            response.datas.forEach {
                datas.append(DistrictModel(data: $0 as! [String : Any]))
            }
            self.districtList = datas
            result(self.districtList)
            
        } else {
            if districtList.count > 0 {
                result(districtList)
            } else {
                EMISAPIManager.shared.districtList { (apiResult) in
                    var datas: [DistrictModel] = []
                    switch apiResult {
                    case .success(let response):
                        log.info("District list API success: \(response.status)")
                        if response.isValid {
                            response.datas.forEach {
                                datas.append(DistrictModel(data: $0 as! [String : Any]))
                            }
                            self.districtList = datas
                        }
                        result(self.districtList)
                    case .failure(let error):
                        log.error("District list API failure: \(error)")
                        result(datas)
                    }
                }
            }
        }
    }
    
    func casualtyList(case_no: String, result: @escaping CasualtyListCompletion) {
        EMISAPIManager.shared.casualtyList(case_no: case_no) { (apiResult) in
            var datas: [CasualtyModel] = []
            switch apiResult {
            case .success(let response):
                log.info("Casualty list API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(CasualtyModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("Casualty list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func sampleList(category_id: String, result: @escaping DesSampleListCompletion) {
        EMISAPIManager.shared.sampleList(category_id: category_id) { (apiResult) in
            var datas: [DesSampleModel] = []
            switch apiResult {
            case .success(let response):
                log.info("DesSample list API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(DesSampleModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("DesSample list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func addCase(add_case_model: AddCaseModel, casualty_models: [CasualtyModel], imageList: [FileModel], result: @escaping ResultCompletion) {

        EMISAPIManager.shared.addCase(parameterrs: add_case_model.dictionary()) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Add case API success: \(response.status)")
                if response.status {
                    if let case_no = response.data["case_no"] as? String {
                        if casualty_models.count > 0 {
                            var add_casualty_model = AddCasualtyModel()
                            add_casualty_model.case_no = case_no
                            add_casualty_model.casualties_list = casualty_models
                            self.addCasualty(model: add_casualty_model) { (responseModel) in
                                if responseModel.status {
                                    if imageList.count > 0 {
                                        self.uploadImages(case_no: case_no, images: imageList, result: result)
                                    } else {
                                        result(ResponseModel(success: true, data: ["case_no":case_no]))
                                    }
                                } else {
                                    result(ResponseModel(error: .resultFail))
                                }
                            }
                        } else {
                            if imageList.count > 0 {
                                self.uploadImages(case_no: case_no, images: imageList, result: result)
                            } else {
                                result(ResponseModel(success: true, data: ["case_no":case_no]))
                            }
                        }
                    }
                } else {
                    AlertManager.shared.showMessage(message: response.message, title: "")
                    result(ResponseModel(error: .resultFail))
                }
            case .failure(let error):
                log.error("Add case API failure: \(error)")
                result(ResponseModel(error: .resultFail))
                AlertManager.shared.showMessage(message: "\(error)", title: "")
            }
        }
    }
    
    func mergerCase(model: MergerCaseModel, result: @escaping ResultCompletion) {
        EMISAPIManager.shared.mergerCase(parameterrs: model.dictionary()) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Merger case API success: \(response.status)")
                if response.status {
                    result(ResponseModel(success: true))
                } else {
                    AlertManager.shared.showMessage(message: response.message, title: "")
                    result(ResponseModel(error: .resultFail))
                }
            case .failure(let error):
                log.error("Merger case API failure: \(error)")
                result(ResponseModel(error: .resultFail))
                AlertManager.shared.showMessage(message: "\(error)", title: "")
            }
        }
    }
    
    func addCasualty(model: AddCasualtyModel, result: @escaping ResultCompletion) {
        EMISAPIManager.shared.addCasualty(parameterrs: model.dictionary()) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Add casualty API success: \(response.status)")
                if response.status {
                    result(ResponseModel(success: true))
                } else {
                    AlertManager.shared.showMessage(message: response.message, title: "")
                    result(ResponseModel(error: .resultFail))
                }
            case .failure(let error):
                log.error("Add casualty API failure: \(error)")
                result(ResponseModel(error: .resultFail))
                AlertManager.shared.showMessage(message: "\(error)", title: "")
            }
        }
    }
    
    func deleteCasualty(case_no: String, seqnos: [String], result: @escaping ResultCompletion) {
        var parameterrs = [String: Any]()
        var list:[[String: Any]] = []
        seqnos.forEach {
            list.append(["seqno": $0])
        }
        parameterrs["case_no"] = case_no
        parameterrs["casualties_list"] = list
        EMISAPIManager.shared.deleteCasualty(parameterrs: parameterrs) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Delete casualty API success: \(response.status)")
                result(response)
            case .failure(let error):
                log.error("Delete casualty API failure: \(error)")
                result(ResponseModel(error: .resultFail))
                AlertManager.shared.showMessage(message: "\(error)", title: "")
            }
        }
    }
    
    func uploadImages(case_no: String, images: [FileModel], result: @escaping ResultCompletion) {
        let count = images.count
        if count > 0 {
            var headers: [String : String] = [:]
            headers["Content-Type"] = "multipart/form-data"
            headers["key"] = EMISAPIRouter.API_Key
            
            if let session_token = EMISManager.shared.userModel.value.session_token as String? {
                if session_token.count > 0 {
                    headers["session_token"] = session_token
                }
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
                for i in 0..<count{
                    if images[i].image != nil {
                        let imageData = images[i].image.jpegData(compressionQuality: 1)!
                        multipartFormData.append(imageData, withName: "photo[]", fileName: "photo\(i).jpeg" , mimeType: "image/jpeg")
                    } else {
                        print("imageData is nil")
                    }
                }
                
                multipartFormData.append(Data(case_no.utf8), withName: "case_no")
                
            }, to: "\(EMISAPIRouter.baseURLString)/Case/UplPic", method: .post, headers: headers) { (apiResult) in
                switch apiResult {
                case .success(let upload, _ , _):
                    upload.uploadProgress(closure: { (progress) in
                        print("uploding: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        let resp = response.result.value! as! NSDictionary
                        if let status = resp["status"] as? Int {
                            if status == 1 {
                                result(ResponseModel(success: true, data: ["case_no":case_no]))
                            } else {
                                result(ResponseModel(error: .unknowError))
                                AlertManager.shared.showMessage(message: (resp["message"] as! String), title: "")
                            }
                        } else {
                            result(ResponseModel(error: .decodeJSONFail))
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    
                }
            }
        } else {
            result(ResponseModel(success: true, data: ["case_no":case_no]))
        }
    }
    
    func updateGPS(model: CaseLocationModel, result: @escaping ResultCompletion) {
        EMISAPIManager.shared.updateGPS(parameterrs: model.dictionary()) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Update GPS API success: \(response.status)")
                result(response)
            case .failure(let error):
                log.error("Update GPS API failure: \(error)")
                result(ResponseModel(error: .resultFail))
                AlertManager.shared.showMessage(message: "\(error)", title: "")
            }
        }
    }
    
    func updateCase(model: ReplceCaseModel, result: @escaping ResultCompletion) {
        EMISAPIManager.shared.updateCase(parameterrs: model.dictionary()) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Update case API success: \(response.status)")
                result(response)
            case .failure(let error):
                log.error("Update case API failure: \(error)")
                result(ResponseModel(error: .resultFail))
            }
        }
    }
    
    
    func smartMsg(result: @escaping CaseSmartMsgCompletion) {
        EMISAPIManager.shared.smartMsg() { (apiResult) in
            var datas: [CaseSmartMsg] = []
            switch apiResult {
            case .success(let response):
                log.info("smartMsg API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(CaseSmartMsg(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("smartMsg list API failure: \(error)")
                result(datas)
            }
        }
    }
    
//    func reply(model: ReplyModel,images: [FileModel], videos: [FileModel], progressResult: @escaping Progress, result: @escaping ResultCompletion) {
//        self.progressResult = progressResult
//        EMISAPIManager.shared.reply(parameterrs: model.dictionary()) { (apiResult) in
//            switch apiResult {
//            case .success(let response):
//                log.info("Reply case API success: \(response.status)")
//                let seq_no = response.data["seq_no"] as? String
//                self.replyImage(seq_no: seq_no!, images: images) { (responseModel) in
//                    if responseModel.status {
//                        self.replyVideo(seq_no: seq_no!, videos: videos) { (responseModel) in
//                            if responseModel.status {
//                                result(responseModel)
//                            } else {
//                                result(ResponseModel(error: .uploadError))
//                            }
//                        }
//                    } else {
//                        result(ResponseModel(error: .uploadError))
//                    }
//                }
//            case .failure(let error):
//                log.error("Reply case API failure: \(error)")
//                result(ResponseModel(error: .resultFail))
//            }
//        }
//        
//    }
//    
//    func replyImage(seq_no: String, images: [FileModel], result: @escaping ResultCompletion) {
//        if images.count > 0 {
//            var headers: [String : String] = [:]
//            headers["Content-Type"] = "multipart/form-data"
//            headers["key"] = EMISAPIRouter.API_Key
//            
//            if let session_token = EMISManager.shared.userModel.value.session_token as String? {
//                if session_token.count > 0 {
//                    headers["session_token"] = session_token
//                }
//            }
//            var progressStatus = ProgressStatus()
//            progressStatus.type = .Image
//            
//            Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
//                for i in 0..<images.count{
//                    if images[i].image != nil {
//                        let imageData = images[i].image.jpegData(compressionQuality: 1)!
//                        multipartFormData.append(imageData, withName: "photo[]", fileName: "photo\(i).jpeg" , mimeType: "image/jpeg")
//                    } else {
//                        print("imageData is nil")
//                    }
//                }
//                
//                multipartFormData.append(Data(seq_no.utf8), withName: "seq_no")
//                
//            }, to: "\(EMISAPIRouter.baseURLString)/Proc/UplPic", method: .post, headers: headers) { (apiResult) in
//                switch apiResult {
//                case .success(let upload, _ , _):
//                    upload.uploadProgress(closure: { (progress) in
//                        progressStatus.value = progress.fractionCompleted
//                        self.progressResult(progressStatus)
//                        print("Image uploding: \(progress.fractionCompleted)")
//                    })
//                    
//                    upload.responseJSON { response in
//                        let resp = response.result.value! as! NSDictionary
//                        if let status = resp["status"] as? Int {
//                            if status == 1 {
//                                result(ResponseModel(success: true))
//                            } else {
//                                result(ResponseModel(error: .uploadError))
//                            }
//                        } else {
//                            result(ResponseModel(error: .decodeJSONFail))
//                        }
//                    }
//                    
//                case .failure(_ ):
//                    result(ResponseModel(error: .encodingError))
//                }
//            }
//        } else {
//            result(ResponseModel(success: true))
//        }
//    }
//    
//    func replyVideo(seq_no: String, videos: [FileModel], result: @escaping ResultCompletion) {
//        if videos.count > 0 {
//            var headers: [String : String] = [:]
//            headers["Content-Type"] = "multipart/form-data"
//            headers["key"] = EMISAPIRouter.API_Key
//            
//            if let session_token = EMISManager.shared.userModel.value.session_token as String? {
//                if session_token.count > 0 {
//                    headers["session_token"] = session_token
//                }
//            }
//            let progressStatus = ProgressStatus()
//            progressStatus.type = .Video
//            
//            Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
//                for i in 0..<videos.count{
//                    if videos[i].mediaURL != nil {
//                        guard let videoData = try? Data(contentsOf: videos[i].mediaURL) else {
//                            return
//                        }
//                        multipartFormData.append(videoData, withName: "photo[]", fileName: "video\(i).mp4" , mimeType: "video/mp4")
//                    } else {
//                        print("mediaURL is nil")
//                    }
//                }
//                
//                multipartFormData.append(Data(seq_no.utf8), withName: "seq_no")
//                
//            }, to: "\(EMISAPIRouter.baseURLString)/Proc/UplPic", method: .post, headers: headers) { (apiResult) in
//                switch apiResult {
//                case .success(let upload, _ , _):
//                    upload.uploadProgress(closure: { (progress) in
//                        progressStatus.value = progress.fractionCompleted
//                        self.progressResult(progressStatus)
//                        print("Video uploding: \(progress.fractionCompleted)")
//                    })
//                    
//                    upload.responseJSON { response in
//                        if let resp = response.result.value! as? NSDictionary {
//                            if let status = resp["status"] as? Int {
//                                if status == 1 {
//                                    result(ResponseModel(success: true))
//                                } else {
//                                    result(ResponseModel(error: .uploadError))
//                                }
//                            } else {
//                                result(ResponseModel(error: .decodeJSONFail))
//                            }
//                        } else {
//                            result(ResponseModel(error: .uploadError))
//                        }
//                    }
//                    
//                case .failure(_ ):
//                    result(ResponseModel(error: .encodingError))
//                }
//            }
//        } else {
//            result(ResponseModel(success: true))
//        }
//    }

    func startHeartbeat() {
        if heartbeatTimer == nil {
            heartbeatTimer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
            heartbeatTimer.schedule(deadline: DispatchTime.now(), repeating: heartbeatInterval)
            heartbeatTimer.setEventHandler {
                if self.checkLogin() {
                    print("[info] Start Heartbeat")
                    let parameterrs: [String: Any] = ["latitude": self.locationModel.value.latitude, "longitude": self.locationModel.value.longitude]
                    EMISAPIManager.shared.online(parameterrs: parameterrs) { (apiResult) in
                        switch apiResult {
                        case .success(let response):
                            self.isLogin.value = response.status
//                            if !response.status {
//                                self.userModel.value = UserModel(data: [:])
//                                UserManager.shared.saveUserModel(self.userModel.value)
//                            }
                            log.info("Heartbeat API success: \(response.status)")
                        case .failure(let error):
                            self.isLogin.value = false
                            log.error("Heartbeat API failure: \(error)")
                        }
                    }
                }
            }
            
            if (!heartbeatTimer.isCancelled) {
                heartbeatTimer.resume()
            }
        }
    }
    
    func stopHeartbeat() {
        if heartbeatTimer != nil {
            if (!heartbeatTimer.isCancelled) {
                heartbeatTimer.cancel()
                heartbeatTimer = nil
                print("[info] Stop Heartbeat")
                
            }
        }
    }
    
    func arrive(model: CaseLocationModel, result: @escaping ResultCompletion) {
        EMISAPIManager.shared.arrive(parameterrs: model.dictionary()) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Arrive API success: \(response.status)")
                result(response)
            case .failure(let error):
                log.error("Arrive API failure: \(error)")
                result(ResponseModel(error: .resultFail))
                AlertManager.shared.showMessage(message: "\(error)", title: "")
            }
        }
    }
    
    func pushType(result: @escaping PushTypeCompletion) {
        EMISAPIManager.shared.pushType { (apiResult) in
            var datas: [PushTypeModel] = []
            switch apiResult {
            case .success(let response):
                log.info("PushType API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(PushTypeModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("PushType list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func pushLog(no: String, typeid: String, result: @escaping PushLogCompletion) {        
        EMISAPIManager.shared.pushLog(account: UserManager.shared.account(), no: no, typeid: typeid) { (apiResult) in
            var datas: [PushLogModel] = []
            switch apiResult {
            case .success(let response):
                log.info("PushLog API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(PushLogModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("PushLog list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func readPush(no: String, result: @escaping ResultCompletion) {
        var deviceid = ""
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            deviceid = uuid
        }
        EMISAPIManager.shared.readPush(account: UserManager.shared.account(), deviceid: deviceid, ostype: "IOS", no: no) { [weak self] (apiResult) in
            guard let self = self else { return }
            switch apiResult {
            case .success(let response):
                log.info("readPush API success")
                result(response)
            case .failure(let error):
                log.error("readPush API failure: \(error)")
                result(ResponseModel(error: .resultFail))
            }
        }
    }
    
    func messageLog(no: String, result: @escaping PushLogCompletion) {
        EMISAPIManager.shared.pushLog(account: UserManager.shared.account(), no: no, typeid: "4") { (apiResult) in
            var datas: [PushLogModel] = []
            switch apiResult {
            case .success(let response):
                log.info("messageLog API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(PushLogModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)

            case .failure(let error):
                log.error("messageLog list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func pushGetReply(replyno: String, result: @escaping ReplyMessageCompletion) {
        EMISAPIManager.shared.pushGetReply(replyno: replyno) { (apiResult) in
            var datas: [ReplyMessageModel] = []
            switch apiResult {
            case .success(let response):
                log.info("pushGetReply API success: \(response.status)")
                if response.isValid {
                    response.datas.forEach {
                        datas.append(ReplyMessageModel(data: $0 as! [String : Any]))
                    }
                }
                result(datas)
            case .failure(let error):
                log.error("pushGetReply list API failure: \(error)")
                result(datas)
            }
        }
    }
    
    func pushReply(replyno: String, msg: String, result: @escaping ReplyCompletion) {
        EMISAPIManager.shared.pushReply(replyno: replyno, msg: msg) { (apiResult) in
            // only need sno
            var data = ReplyMessageModel(data: [:])
            switch apiResult {
            case .success(let response):
                log.info("pushReply API success: \(response.status)")
                if response.status {
                    data = ReplyMessageModel(data: response.data)
                }
                result(data)
            case .failure(let error):
                log.error("pushReply API failure: \(error)")
                result(ReplyMessageModel(data: [:]))
            }
        }
    }
    
    func pushUploadImages(sno: String, images: [FileModel], result: @escaping ResultCompletion) {
        let count = images.count
        if count > 0 {
            var headers: [String : String] = [:]
            headers["Content-Type"] = "multipart/form-data"
            headers["key"] = EMISAPIRouter.API_Key
            
            if let session_token = EMISManager.shared.userModel.value.session_token as String? {
                if session_token.count > 0 {
                    headers["session_token"] = session_token
                }
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
                for i in 0..<count{
                    if images[i].image != nil {
                        let imageData = images[i].image.jpegData(compressionQuality: 1)!
                        multipartFormData.append(imageData, withName: "photo[]", fileName: "photo\(i).jpeg" , mimeType: "image/jpeg")
                    } else {
                        print("imageData is nil")
                    }
                }
                
                multipartFormData.append(Data(sno.utf8), withName: "sno")

            }, to: "\(EMISAPIRouter.baseURLString)/Push/UplPic", method: .post, headers: headers) { (apiResult) in
                switch apiResult {
                case .success(let upload, _ , _):
                    upload.uploadProgress(closure: { (progress) in
                        print("uploding: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        let resp = response.result.value! as! NSDictionary
                        if let status = resp["status"] as? Int {
                            if status == 1 {
                                print("Image uploaded")
                                result(ResponseModel(success: true))
                            } else {
                                result(ResponseModel(error: .unknowError))
                                AlertManager.shared.showMessage(message: (resp["message"] as! String), title: "")
                            }
                        } else {
                            result(ResponseModel(error: .decodeJSONFail))
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    
                }
            }
        } else {
            result(ResponseModel(success: true))
        }
    }
    
    func pushUploadVideos(sno: String, pic_sno: String, videos: [FileModel], result: @escaping ResultCompletion) {
        let count = videos.count
        if count > 0 {
            var headers: [String : String] = [:]
            headers["Content-Type"] = "multipart/form-data"
            headers["key"] = EMISAPIRouter.API_Key
            
            if let session_token = EMISManager.shared.userModel.value.session_token as String? {
                if session_token.count > 0 {
                    headers["session_token"] = session_token
                }
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
                for i in 0..<videos.count{
                    if videos[i].url != nil {
                        guard let videoData = try? Data(contentsOf: videos[i].url) else {
                            return
                        }
                        multipartFormData.append(videoData, withName: "photo[]", fileName: "video\(i).mp4" , mimeType: "video/mp4")
                    } else {
                        print("mediaURL is nil")
                    }
                }
                
                multipartFormData.append(Data(sno.utf8), withName: "sno")
                multipartFormData.append(Data(pic_sno.utf8), withName: "pic_sno")

            }, to: "\(EMISAPIRouter.baseURLString)/Push/UplPic", method: .post, headers: headers) { (apiResult) in
                switch apiResult {
                case .success(let upload, _ , _):
                    upload.uploadProgress(closure: { (progress) in
                        print("uploding: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        let resp = response.result.value! as! NSDictionary
                        if let status = resp["status"] as? Int {
                            if status == 1 {
                                print("Image uploaded")
                                result(ResponseModel(success: true))
                            } else {
                                result(ResponseModel(error: .unknowError))
                                AlertManager.shared.showMessage(message: (resp["message"] as! String), title: "")
                            }
                        } else {
                            result(ResponseModel(error: .decodeJSONFail))
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    
                }
            }
        } else {
            result(ResponseModel(success: true))
        }
    }
    
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}
