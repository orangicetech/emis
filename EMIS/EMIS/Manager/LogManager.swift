//
//  LogManager.swift
//  Cupola360Kit
//
//  Created by Wu Polo on 2020/9/1.
//  Copyright © 2020 Polo. All rights reserved.
//

import UIKit
public typealias LogHandler = (String) -> Void

let log = LogManager.shared

@objc public class LogManager: NSObject {
    @objc public static let shared = LogManager()
    var verboseLogHandler: LogHandler!
    var debugLogHandler: LogHandler!
    var infoLogHandler: LogHandler!
    var warningLogHandler: LogHandler!
    var errorLogHandler: LogHandler!
    
    @objc public func cancelLogHandlerObservable() {
        verboseLogHandler = nil
        debugLogHandler = nil
        infoLogHandler = nil
        warningLogHandler = nil
        errorLogHandler = nil
    }
    
    @objc public func addVerboseLogHandlerObservable(logHandler: @escaping LogHandler) {
        verboseLogHandler = logHandler
    }
    
    @objc public func addDebugHandlerObservable(logHandler: @escaping LogHandler) {
        debugLogHandler = logHandler
    }
    
    @objc public func addInfoLogHandlerObservable(logHandler: @escaping LogHandler) {
        infoLogHandler = logHandler
    }
    
    @objc public func addWarningLogHandlerObservable(logHandler: @escaping LogHandler) {
        warningLogHandler = logHandler
    }
    
    @objc public func addErrorLogHandlerObservable(logHandler: @escaping LogHandler) {
        errorLogHandler = logHandler
    }
    
    @objc public func verbose(_ message: String) {
        if verboseLogHandler != nil {
            verboseLogHandler("[verbose] \(message)")
        }
        print("[verbose] \(message)")
    }
    
    @objc public func debug(_ message: String) {
        if debugLogHandler != nil {
            debugLogHandler("[debug] \(message)")
        }
        print("[debug] \(message)")
    }
    
    @objc public func info(_ message: String) {
        if infoLogHandler != nil {
            infoLogHandler("[info] \(message)")
        }
        print("[info] \(message)")
    }
    
    @objc public func warning(_ message: String) {
        if warningLogHandler != nil {
            warningLogHandler("[warning] \(message)")
        }
        print("[warning] \(message)")
    }
    
    @objc public func error(_ message: String) {
        if errorLogHandler != nil {
            errorLogHandler("[error] \(message)")
        }
        print("[error] \(message)")
    }
}
