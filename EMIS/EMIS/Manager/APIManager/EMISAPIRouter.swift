//
//  EMISAPIRouter.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

import UIKit
import Foundation
import Alamofire

enum EMISAPIRouter: URLRequestConvertible {
//    static let baseURLString: String = "https://eoc2.ntpc.gov.tw/EMIS_API"
//    static let baseURLString: String = "http://172.18.119.86:82"
//    static let baseURLString: String = "http://122.116.251.52:8068"
    
    //  測試
    //  http://61.216.64.19:8067/EMIS_API
    
    //  正式
    //  https://eoc2.ntpc.gov.tw/EMIS_API/V2
    
    static let baseURLString: String = "http://61.216.64.19:8067/EMIS_API"

    static let API_Key: String = "EOC-API-ANDROID-202001"
    case login(username: String, password: String)
    case logout
    case online(parameterrs: [String: Any])
    case projectList
    case caseList(project_id: String)
    case caseDetail(case_no: String)
    case caseUpdate(parameterrs: [String: Any])
    case categoryList
    case districtList
    case casualtyList(case_no: String)
    case sampleList(category_id: String)
    case addCase(parameterrs: [String: Any])
    case addCasualty(parameterrs: [String: Any])
    case deleteCasualty(parameterrs: [String: Any])
    case updateGPS(parameterrs: [String: Any])
    case arrive(parameterrs: [String: Any])
    case mergerCase(parameterrs: [String: Any])
    case reply(parameterrs: [String: Any])
    case pushLog(account: String, no: String, typeid: String)
    case readPush(account: String, deviceid: String, ostype: String, no: String)
    case pushType
    case pushGetReply(replyno: String)
    case pushReply(replyno: String, msg: String)
    case caseSmartMsg

    func asURLRequest() throws -> URLRequest {
        var method: HTTPMethod {
            switch self {
            case .login, .logout, .projectList, .caseList, .categoryList, .districtList, .sampleList, .addCase, .caseUpdate, .addCasualty, .deleteCasualty, .caseDetail, .updateGPS, .arrive, .mergerCase, .reply, .online , .casualtyList, .pushLog, .readPush, .pushType, .pushGetReply, .pushReply, .caseSmartMsg:
                return .post
            }
        }
        
        let url: URL = {
            let relativePath: String
            switch self {
            case .login:
                relativePath = "/Acc/Login"
            case .logout:
                relativePath = "/Acc/Logout"
            case .online:
                relativePath = "/Acc/Online"
            case .projectList:
                relativePath = "/Proj/List"
            case .caseList:
                relativePath = "/Case/List"
            case .caseDetail:
                relativePath = "/Case/Detail"
            case .caseUpdate:
                relativePath = "/Case/Update"
            case .categoryList:
                relativePath = "/Para/Category"
            case .casualtyList:
                relativePath = "/Casualty/List"
            case .districtList:
                relativePath = "/Para/District"
            case .sampleList:
                relativePath = "/Para/DesSample"
            case .addCase:
                relativePath = "/Case/Add"
            case .addCasualty:
                relativePath = "/Casualty/Add"
            case .deleteCasualty:
                relativePath = "/Casualty/Delete"
            case .updateGPS:
                relativePath = "/Proc/SetGPS"
            case .arrive:
                relativePath = "/Proc/Arrive"
            case .mergerCase:
                relativePath = "/Proc/MergerCase"
            case .reply:
                relativePath = "/Proc/Reply"
            case .pushLog:
                relativePath = "/Push/GetLog"
            case .readPush:
                relativePath = "/Push/Read"
            case .pushType:
                relativePath = "/Push/GetType"
            case .pushGetReply:
                relativePath = "/Push/GetReply"
            case .pushReply:
                relativePath = "/Push/Reply"
            case .caseSmartMsg:
                relativePath = "/Case/SmartMsg"
            }
            
            return URL(string: EMISAPIRouter.baseURLString + relativePath)!
            
        }()
        
        let params: ([String: Any]?) = {
            switch self {
            case .login(let username, let password):
                return ["username": username,"password": password, "push_token": ""]
            case .logout:
                return nil
            case .online(let parameterrs):
                return parameterrs
            case .projectList:
                return nil
            case .caseList(let project_id):
                return ["project_id": project_id]
            case .caseDetail(let case_no):
                return ["case_no": case_no]
            case .caseUpdate(let parameterrs):
                return parameterrs
            case .categoryList:
                return ["category_id": ""]
            case .casualtyList(let case_no):
                return ["case_no": case_no]
            case .districtList:
                return nil
            case .sampleList(let category_id):
                return ["category_id": category_id]
            case .addCase(let parameterrs):
                return parameterrs
            case .addCasualty(let parameterrs):
                return parameterrs
            case .deleteCasualty(let parameterrs):
                return parameterrs
            case .updateGPS(let parameterrs):
                return parameterrs
            case .arrive(let parameterrs):
                return parameterrs
            case .mergerCase(let parameterrs):
                return parameterrs
            case .reply(let parameterrs):
                return parameterrs
            case .pushLog(let account, let no, let typeid):
                return ["acc": account,"no": no, "typeid": typeid]
            case .readPush(let account, let deviceid, let ostype, let no):
                return ["acc": account,"deviceid": deviceid, "ostype": ostype, "no": no]
            case .pushType:
                return nil
            case .pushGetReply(let replyno):
                return ["replyno": replyno]
            case .pushReply(let replyno, let msg):
                return ["replyno": replyno, "msg": msg]
            case .caseSmartMsg:
                return nil
            }
            
        }()
        print("## URL:\(url)")
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue(EMISAPIRouter.API_Key, forHTTPHeaderField: "key")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("IOS", forHTTPHeaderField: "ostype")
        urlRequest.setValue(UserManager.shared.fcmToken, forHTTPHeaderField: "push_token")
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            urlRequest.setValue(uuid, forHTTPHeaderField: "deviceid")
        }
        
        if let session_token = EMISManager.shared.userModel.value.session_token as String? {
            if session_token.count > 0 {
                urlRequest.setValue(session_token, forHTTPHeaderField: "session_token")

            }
        }
        
        //set timeout
        switch self {
        case .login, .logout, .projectList, .caseList, .categoryList, .districtList, .sampleList, .addCase, .caseUpdate, .addCasualty, .deleteCasualty, .caseDetail, .updateGPS, .arrive, .mergerCase, .reply, .online, .casualtyList, .pushLog, .readPush, .pushType, .pushGetReply, .pushReply, .caseSmartMsg:
            urlRequest.timeoutInterval =  30
        }
        
        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
