//
//  EMISAPIManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

import Foundation
import Alamofire
import UIKit

class EMISAPIManager: NSObject {
    static let shared = EMISAPIManager()
    // MARK: - 帳號
    // 登入
    func login(username: String, password: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.login(username: username, password: password)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 登出
    func logout(result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.logout) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 線上人員 (回傳APP登入狀態，60 秒刷新)
    func online(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.online(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // MARK: - 專案
    // 專案列表
    func projectList(result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.projectList) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // MARK: - 案件
    // 案件列表
    func caseList(project_id: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.caseList(project_id: project_id)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 案件明細
    func caseDetail(case_no: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.caseDetail(case_no: case_no)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 新增案件
    func addCase(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.addCase(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 修改案件
    func updateCase(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.caseUpdate(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // 現場狀況智能內容
    func smartMsg( result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.caseSmartMsg) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // MARK: - 傷亡清冊
    // 傷亡清冊
    func casualtyList(case_no: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.casualtyList(case_no: case_no)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // 新增傷亡人員
    func addCasualty(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.addCasualty(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 刪除傷亡人員 *
    func deleteCasualty(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.deleteCasualty(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // MARK: - 案件處理
    // 處理回覆
    func reply(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.reply(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 建議併案
    func mergerCase(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.mergerCase(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 定位校正
    func updateGPS(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.updateGPS(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 抵達現場
    func arrive(parameterrs: [String: Any], result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.arrive(parameterrs: parameterrs)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // MARK: - 推播
    // 取得該帳號推播紀錄
    func pushLog(account: String, no: String, typeid: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.pushLog(account: account, no: no, typeid: typeid)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // 更新推播已讀狀態
    func readPush(account: String, deviceid: String, ostype: String, no: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        print("@@ send read Push account:\(account), deviceid:\(deviceid), ostype:\(ostype) no:\(no)")
        apiCall(urlRequest: EMISAPIRouter.readPush(account: account, deviceid: deviceid, ostype: ostype, no: no)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // 取得類別編號及名稱資料
    func pushType(result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.pushType) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // 取得推播留言串
    func pushGetReply(replyno: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.pushGetReply(replyno: replyno)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // 推播回覆
    func pushReply(replyno: String, msg: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.pushReply(replyno: replyno, msg: msg)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    
    // MARK: - 參數
    // 獲取案件類別
    func categoryList(result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.categoryList) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 現場狀況範本
    func sampleList(category_id: String, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.sampleList(category_id: category_id)) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // 行政區
    func districtList(result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        apiCall(urlRequest: EMISAPIRouter.districtList) { (apiResult) in
            switch apiResult {
            case .success(let response):
                result(.success(response))
            case .failure(_):
                result(.failure(.resultFail))
            }
        }
    }
    // MARK: - Generic Function for API calls
    func apiCall(urlRequest: URLRequestConvertible, result: @escaping (Swift.Result<ResponseModel, APIError>) -> Void) {
        var url: String? = ""
        var token = ""
        url = try! urlRequest.asURLRequest().url?.absoluteString
        
        if let session_token = EMISManager.shared.userModel.value.session_token as String? {
            token = session_token
        }
            
        guard token != "" || (url?.contains("Acc/Login"))! else {
            print("## apiCall return \(url!)")
            return
        }

        
        Alamofire.request(urlRequest)
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    log.debug("API call status code: \(status)")
                }
                switch response.result {
                case .success:
                    guard let status = response.response?.statusCode, status == 200 else {
                        log.warning("API call state fail: \(String(describing: response.response?.statusCode))")
                        result(.failure(.resultFail))
                        return
                    }
                    
                    if let responseValue = response.value as? [String: Any] {
                        print("responseValue:\(responseValue)")
                        let data = ResponseModel(data: responseValue)
                        if (data.message.contains("無效或過時的TOKEN") || data.message.contains("Token有誤")) {
                            print("憑證失效")
                            LoadingHUD.shared.dismiss()

                            ViewRouter.shared.presentWarningFrom(viewController: ViewRouter.shared.topMostController()!, title:"憑證失效", message: "請重新登入", type: .Alert) { (buttonEvent) in
                                EMISManager.shared.cleanUser()
                                LoadingHUD.shared.dismiss()
                                if (ViewRouter.shared.navigationController != nil) {

                                    if (ViewRouter.shared.navigationController.viewControllers.first! is ProjectListController) {
                                        ViewRouter.shared.navigationController.viewControllers.first!.dismiss(animated: true)
                                    }
                                }
                            }
                        } else {
                            result(.success(data))
                        }
                    } else {
                        result(.success(ResponseModel(success: false)))
                    }
                    
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut {
                        log.error("API call timeout!")
                        result(.failure(.timeOut))
                    } else {
                        log.error("API call URL:\(String(describing: response.request?.url?.absoluteString)), request failed with error: \(error.localizedDescription)")
                        result(.failure(.resultFail))
                    }
                }
        }
    }
}
