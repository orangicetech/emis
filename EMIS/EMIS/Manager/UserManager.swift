//
//  UserManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/1.
//

import UIKit

class UserManager: NSObject {
    static let shared = UserManager()
    var fcmToken = ""
    
    // MARK: - Public
    @objc public class func sharedInstance() -> UserManager {
      return UserManager.shared
    }
    
    func saveUserModel(_ userModel: UserModel) {
        UserDefaults.standard.set(userModel.dictionary(), forKey: "userModel")
        UserDefaults.standard.synchronize()
    }

    func userModel() -> UserModel {
        if UserDefaults.standard.dictionary(forKey: "userModel") != nil {
            return UserModel(data: (UserDefaults.standard.dictionary(forKey: "userModel")!))
        } else {
            return UserModel(data: [:])
        }
    }
    
    func saveAccount(_ account: String) {
        UserDefaults.standard.set(account.base64Encoded(), forKey: "account".base64Encoded() ?? "rtyr")
        UserDefaults.standard.synchronize()
    }

    func account() -> String {
        if let account = UserDefaults.standard.string(forKey: "account".base64Encoded() ?? "rtyr") {
            return account.base64Decoded() ?? ""
        } else {
            return ""
        }
    }
    
    func saveRemember(_ remember: Bool) {
        UserDefaults.standard.set(remember, forKey: "Remember")
    }
    
    func isRemember() -> Bool{
       return UserDefaults.standard.bool(forKey: "Remember")
    }
    
    func saveUploadModels(_ models: [UploadModel]) {
        var list: [[String: Any]] = []
        models.forEach {
            list.append($0.dictionary())
        }
        UserDefaults.standard.set(list, forKey: "UploadModel")
        UserDefaults.standard.synchronize()
    }

    func uploadModels() -> [UploadModel] {
        var models: [UploadModel] = []
        if UserDefaults.standard.array(forKey: "UploadModel") != nil {
            if let list = UserDefaults.standard.array(forKey: "userModel") {
                list.forEach {
                    if let data: [String: Any] = $0 as? [String : Any] {
                        models.append(UploadModel.conver(data: data))
                    }
                }
            }
            return models
        } else {
            return models
        }
    }
}
