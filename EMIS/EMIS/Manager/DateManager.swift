//
//  DateManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/1.
//

import UIKit

class DateManager: NSObject {
    class func dateChinaTransformTimestamp(_ timestamp: Int) -> String? {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .medium
        formatter.dateFormat = "yyy/MM/dd HH:mm"
        formatter.calendar = Calendar(identifier: .republicOfChina)
        let time = formatter.string(from: Date(timeIntervalSince1970: TimeInterval(timestamp)))

        return time
    }
    
    class func ApiDateTransformTimestamp(_ timestamp: Int) -> String? {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .medium
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let time = formatter.string(from: Date(timeIntervalSince1970: TimeInterval(timestamp)))
        return time
    }
}
