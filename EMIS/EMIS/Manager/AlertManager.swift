//
//  AlertManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/1.
//

import UIKit

typealias ConfirmHandler = () -> Void

class AlertManager: NSObject {
    var alertController: UIAlertController!
    
    static let shared = AlertManager()
    
    func showMessage(message: String?, title: String?) {
        alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let button = UIAlertAction(title: NSLocalizedString("了解", comment: "了解"), style: .default, handler: { action in

            })
        alertController.addAction(button)
        if NSStringFromClass(self.topMostController()!.classForCoder) != "UIAlertController"{
            self.topMostController()!.present(alertController, animated: true)
        }
    }
    
    func showConfirmMessager(message: String?, title: String?, confirm: @escaping ConfirmHandler) {
        alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("確認", comment: "確認"), style: .default) { (_) in
            confirm()
        }
        let cancel = UIAlertAction(title: NSLocalizedString("取消", comment: "取消"), style: .cancel, handler: nil)
        
        alertController.addAction(cancel)
        alertController.addAction(confirm)
        if NSStringFromClass(self.topMostController()!.classForCoder) != "UIAlertController"{
            self.topMostController()!.present(alertController, animated: true)
        }
    }
    
    func topMostController() -> UIViewController? {
        var topController = UIApplication.shared.windows[0].rootViewController
        while let presentedViewController = topController?.presentedViewController {
            topController = presentedViewController
        }
        return topController
    }
}
