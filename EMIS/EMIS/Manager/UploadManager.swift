//
//  UploadManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/19.
//

import Foundation
import Alamofire
import Photos

typealias Progress = (_ progress: ProgressStatus) -> Void
class UploadManager: NSObject {
    static let shared = UploadManager()
    private var sessionManager: Alamofire.SessionManager!
    private var backgroundSessionManager: Alamofire.SessionManager!
    private let heartbeatInterval = DispatchTimeInterval.seconds(3)
    private var heartbeatTimer: DispatchSourceTimer!
    private var headers: [String : String]!
    
    public var progressResult: Progress!
    public var inProcessing: Bool = false
    public var data: [UploadModel] = [] {
        didSet {
            UserManager.shared.saveUploadModels(data)
        }
    }
    
    // MARK: - Public
    public func addCase(model: UploadModel) {
        if backgroundSessionManager == nil {
            setup()
        }
        data.append(model)
        if !inProcessing {
            upload()
        }
    }
    
    public func start() {
        print("[info] Start upload")
        if heartbeatTimer == nil {
            self.data = UserManager.shared.uploadModels()
            heartbeatTimer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
            heartbeatTimer.schedule(deadline: DispatchTime.now(), repeating: heartbeatInterval)
            heartbeatTimer.setEventHandler {
                if !self.inProcessing {
                    self.upload()
                }
            }
            
            if (!heartbeatTimer.isCancelled) {
                heartbeatTimer.resume()
            }
        }
    }
    
    public func stop () {
        if heartbeatTimer != nil {
            if (!heartbeatTimer.isCancelled) {
                heartbeatTimer.cancel()
                heartbeatTimer = nil
                print("[info] Stop upload")
            }
        }
    }
    //MARK: - Private
    func setup() {
        sessionManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
        backgroundSessionManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.background(withIdentifier: "com.youApp.identifier.backgroundtransfer"))
        headers = [:]
        headers["Content-Type"] = "multipart/form-data"
        headers["key"] = EMISAPIRouter.API_Key
        if let session_token = EMISManager.shared.userModel.value.session_token as String? {
            if session_token.count > 0 {
                headers["session_token"] = session_token
            }
        }
    }
    
    func upload() {
        if data.count > 0 {
            EMISManager.shared.uploadingList.value = data
            inProcessing = true
            if let model = data.first {
                DispatchQueue.global().async {
                    var seqnos: [String] = []
                    model.casualtyOldList.forEach {
                        seqnos.append($0.seqno)
                    }
                    print("casualtyOldList:\(seqnos)")
                    EMISManager.shared.deleteCasualty(case_no: model.no, seqnos: seqnos) { (responseModel) in
                        var add_casualty_model = AddCasualtyModel()
                        add_casualty_model.case_no = model.no
                        add_casualty_model.casualties_list = model.casualtyList
                        if model.casualtyList.count > 0 {
                            EMISManager.shared.addCasualty(model: add_casualty_model) { (responseModel) in
                                
                            }
                        }
                    }

                    if model.situation.count > 0 {
                        var replceModel = ReplceCaseModel()
                        replceModel.case_no = model.no
                        replceModel.description = model.situation
                        EMISManager.shared.updateCase(model: replceModel) { (responseModel) in
                            
                        }
                    }

                    self.reply(model: model.replyModel, images: model.imageList, videos: model.videoList) { (progressStatus) in
                        print("type: \(progressStatus.type)")
                        print("progress: \(progressStatus.value)")
                    } result: { (responseModel) in
                        if responseModel.status {
                            LoadingHUD.shared.dismiss()
                            if responseModel.status {
                                self.data.remove(at: 0)
                                if self.data.count > 0 {
                                    self.upload()
                                } else {
                                    self.inProcessing = false
                                    EMISManager.shared.uploadingList.value = []
                                }
                            } else {
                                self.inProcessing = false
                            }
                        } else {
                            self.upload()
                        }
                    }
                }
            }
        } else {
            inProcessing = false
        }
    }
    
    func reply(model: ReplyModel,images: [FileModel], videos: [FileModel], progressResult: @escaping Progress, result: @escaping ResultCompletion) {
        self.progressResult = progressResult
        EMISAPIManager.shared.reply(parameterrs: model.dictionary()) { (apiResult) in
            switch apiResult {
            case .success(let response):
                log.info("Reply case API success: \(response.status)")
                let seq_no = response.data["seq_no"] as? String
                self.replyImage(seq_no: seq_no!, models: images) { (responseModel) in
                    if responseModel.status {
                        self.replyVideo(seq_no: seq_no!, models: videos) { (responseModel) in
                            if responseModel.status {
                                result(responseModel)
                            } else {
                                result(ResponseModel(error: .uploadError))
                            }
                        }
                    } else {
                        result(ResponseModel(error: .uploadError))
                    }
                }
            case .failure(let error):
                log.error("Reply case API failure: \(error)")
                result(ResponseModel(error: .resultFail))
            }
        }
    }
    
    func replyImage(seq_no: String, models: [FileModel], result: @escaping ResultCompletion) {
        print("replyImage")
        let progressStatus = ProgressStatus()
        progressStatus.type = .Image
        var dataList: [Data] = []
        if models.count > 0 {
            models.forEach {
                do {
                    let data = try Data(contentsOf: $0.url, options: .alwaysMapped)
                    dataList.append(data)
                } catch  {
                    print("Get image data erroor")
                }
            }
            
            backgroundSessionManager.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
                dataList.forEach {
                    print("Add image")
                    multipartFormData.append($0, withName: "photo[]", fileName: "photo\($0).jpeg" , mimeType: "image/jpeg")
                }
                multipartFormData.append(Data(seq_no.utf8), withName: "seq_no")
                
            }, to: "\(EMISAPIRouter.baseURLString)/Proc/UplPic", method: .post, headers: headers) { (apiResult) in
                switch apiResult {
                case .success(let upload, _ , _):
                    upload.uploadProgress(closure: { (progress) in
                        progressStatus.value = progress.fractionCompleted
                        self.progressResult(progressStatus)
                    })
                    
                    upload.responseJSON { response in
                        let resp = response.result.value! as! NSDictionary
                        if let status = resp["status"] as? Int {
                            if status == 1 {
                                print("開始刪除檔案")
                                models.forEach {
                                    MediaManager.shared.remove(path: $0.url)
                                }
                                result(ResponseModel(success: true))
                            } else {
                                result(ResponseModel(error: .uploadError))
                            }
                        } else {
                            result(ResponseModel(error: .decodeJSONFail))
                        }
                    }
                case .failure(_ ):
                    result(ResponseModel(error: .encodingError))
                }
            }
        } else {
            result(ResponseModel(success: true))
        }
    }

//    func replyVideo(seq_no: String, models: [FileModel], result: @escaping ResultCompletion) {
//        if models.count > 0 {
//            let progressStatus = ProgressStatus()
//            progressStatus.type = .Video
////            do {
////                let data = try Data(contentsOf: models.first!.url, options: .alwaysMapped)
////
////            } catch  {
////                print("Get video data erroor")
////                result(ResponseModel(error: .uploadError))
////            }
//            backgroundSessionManager.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
//                multipartFormData.append(Data(seq_no.utf8), withName: "seq_no")
//                multipartFormData.append(models.first!.url, withName: "photo[]", fileName: "video.mp4" , mimeType: "video/mp4")
//            }, to: "\(EMISAPIRouter.baseURLString)/Proc/UplPic", method: .post, headers: self.headers) { (apiResult) in
//                switch apiResult {
//                case .success(let upload, _ , _):
//                    upload.uploadProgress(closure: { (progress) in
//                        progressStatus.value = progress.fractionCompleted
//                        self.progressResult(progressStatus)
//                        print("video uploding: \(progress.fractionCompleted)")
//                    })
//
//                    upload.responseJSON { response in
//                        print("error:\(response)")
//                        let resp = response.result.value! as! NSDictionary
//                        if let status = resp["status"] as? Int {
//                            if status == 1 {
//                                result(ResponseModel(success: true))
//                            } else {
//                                result(ResponseModel(error: .uploadError))
//                            }
//                        } else {
//                            result(ResponseModel(error: .decodeJSONFail))
//                        }
//                    }
//
//                case .failure(_ ):
//                    result(ResponseModel(error: .encodingError))
//                }
//            }
//        } else {
//            result(ResponseModel(success: true))
//        }
//    }
    func replyVideo(seq_no: String, models: [FileModel], result: @escaping ResultCompletion) {
        print("replyVideo")
        if models.count > 0 {
            var headers: [String : String] = [:]
            headers["Content-Type"] = "multipart/form-data"
            headers["key"] = EMISAPIRouter.API_Key
            
            if let session_token = EMISManager.shared.userModel.value.session_token as String? {
                if session_token.count > 0 {
                    headers["session_token"] = session_token
                }
            }
            let progressStatus = ProgressStatus()
            progressStatus.type = .Video
            
            backgroundSessionManager.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
                for i in 0..<models.count{
                    if models[i].url != nil {
                        guard let videoData = try? Data(contentsOf: models[i].url) else {
                            return
                        }
                        multipartFormData.append(videoData, withName: "photo[]", fileName: "video\(i).mp4" , mimeType: "video/mp4")
                    } else {
                        print("mediaURL is nil")
                    }
                }
                
                multipartFormData.append(Data(seq_no.utf8), withName: "seq_no")
                
            }, to: "\(EMISAPIRouter.baseURLString)/Proc/UplPic", method: .post, headers: headers) { (apiResult) in
                switch apiResult {
                case .success(let upload, _ , _):
                    upload.uploadProgress(closure: { (progress) in
                        progressStatus.value = progress.fractionCompleted
                        self.progressResult(progressStatus)
                    })
                    
                    upload.responseJSON { response in
                        if let resp = response.result.value as? NSDictionary {
                            if let status = resp["status"] as? Int {
                                if status == 1 {
                                    result(ResponseModel(success: true))
                                } else {
                                    result(ResponseModel(error: .uploadError))
                                }
                            } else {
                                result(ResponseModel(error: .decodeJSONFail))
                            }
                        } else {
                            result(ResponseModel(error: .uploadError))
                        }
                    }
                    
                case .failure(_ ):
                    result(ResponseModel(error: .encodingError))
                }
            }
        } else {
            result(ResponseModel(success: true))
        }
    }
}
