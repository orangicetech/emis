//
//  MediaManager.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/22.
//
import UIKit
import Foundation
import Photos

typealias SaveCompletion = (_ identifier: String) -> Void
typealias GetDataCompletion = (_ data: Data?) -> Void
typealias GetPathCompletion = (_ path: String?) -> Void
class MediaManager {
    static let shared = MediaManager()
    let albumName = "EMIS"
    
    let imageCache = NSCache<NSURL, UIImage>()
    
    func fetchImage(url: URL, completionHandler: @escaping (UIImage?, URL) -> ()) {
        DispatchQueue.global().async {
            if let image = self.imageCache.object(forKey: url as NSURL) {
                completionHandler(image,url)
                return
            }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let data = data, let image = UIImage(data: data) {
                    self.imageCache.setObject(image, forKey: url as NSURL)
                    completionHandler(image,url)
                } else {
                    completionHandler(nil,url)
                }
            }.resume()
        }
    }
    
    func saveVideo(mediaURL: URL) -> URL? {
        let dateString = dateFormatter.string(from: Date())
        let path = fileDirectory.appendingPathComponent("\(dateString).mp4")
        do {
            try FileManager.default.copyItem(at: mediaURL, to: path)
            print("搬移影片成功")
            return path
        } catch {
            print("搬移影片失敗:\(error)")
            return nil
        }
        
    }
    
    func saveImage(image: UIImage) -> URL? {
        let dateString = dateFormatter.string(from: Date())
        let path = fileDirectory.appendingPathComponent("\(dateString).jpg")
        let imageData = image.jpegData(compressionQuality: 1)!
        do {
            try imageData.write(to: path)
            print("寫入照片成功")
            return path
        }catch {
            print("寫入照片失敗:\(error)")
            return nil
        }
    }
    
    func getImage(name: String) -> UIImage? {
        let url = fileDirectory.appendingPathComponent("\(name)")
        if let image = UIImage(contentsOfFile: url.path) {
            return image
        }
        return nil
    }
    
    func remove(path: URL) {
        do{
            try FileManager.default.removeItem(at: path)
            print("已刪除:\(path)")
        } catch {
            print("刪除檔案:\(error)")
        }
    }
    
    // MARK: - Date String

    private let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd--hh-mm-ss"
        return dateFormatter
    }()

    
    private var documentsDirectory: URL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }

    private var fileDirectory: URL {
        let logDirectory = documentsDirectory.appendingPathComponent("files", isDirectory: true)
        
        var isDir: ObjCBool = true
        if !FileManager.default.fileExists(atPath: logDirectory.path, isDirectory: &isDir) {
            // log folder doesn't exist, create it
            try? FileManager.default.createDirectory(at: logDirectory, withIntermediateDirectories: true, attributes: nil)
        }
        return logDirectory
    }
    
    private func getCustomAlbum(albumName: String, complete: @escaping (_ ablum: PHAssetCollection?) -> Void) {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        
        let assetCollections: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: fetchOptions)
        if assetCollections.count == 0 {
            complete(nil)
        } else {
            var customAlbum: PHAssetCollection?
            assetCollections.enumerateObjects({ album, idx, stop in
                if (album.localizedTitle == albumName) {
                    customAlbum = album
                    stop.pointee = true
                }
            })
            complete(customAlbum)
        }
    }
}
