//
//  ProjectListCell.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/26.
//

import UIKit

class ProjectListCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var renkView: CustomView!
    @IBOutlet weak var renkLabel: UILabel!
    @IBOutlet weak var categoryView: CustomView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var buttonImageView: UIImageView!
    @IBOutlet weak var buttonLabel: UILabel!
    
    
    
    var isButtonHighlighted: Bool = false {
        didSet {
            if isButtonHighlighted {
                buttonImageView.image = UIImage(named: "list_btn_go_bg_a")
                buttonLabel.textColor = UIColor(named: "TextHighlightedColor")
            } else {
                buttonImageView.image = UIImage(named: "list_btn_go_bg_n")
                buttonLabel.textColor = UIColor.white
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCellForModel(model: ProjectListModel) {
        titleLabel.text = model.name
        dateTimeLabel.text = model.creation_date
        renkLabel.text = model.level
        categoryLabel.text = model.category
        dateTimeLabel.text = "成立時間：\(model.creation_date)"
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
