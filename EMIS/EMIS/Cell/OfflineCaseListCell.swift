//
//  OfflineCaseListCell.swift
//  EMIS
//
//  Created by Jimmy on 2022/11/15.
//

import UIKit

class OfflineCaseListCell: UITableViewCell {

    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var projectView: UIView!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var maskImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    var upload: Bool = false {
        didSet {
            if upload {
                statusLabel.text = "已上傳"
                maskImageView.image = UIImage(named: "list_img_ing")
                projectView.isHidden = false

            } else {
                statusLabel.text = "待上傳"
                noLabel.text = ""
                maskImageView.image = UIImage(named: "list_img_under")
                projectView.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

