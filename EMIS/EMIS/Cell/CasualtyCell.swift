//
//  CasualtyCell.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/3.
//

import UIKit
typealias CasualtyDeleteHandle  = () -> Void
typealias CasualtyditHandle  = () -> Void
class CasualtyCell: UITableViewCell {
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var injuryLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var casualtyDeleteHandle: CasualtyDeleteHandle!
    var casualtyEditHandle: CasualtyditHandle!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onClickDelete(_ sender: Any) {
        casualtyDeleteHandle?()
    }
    
    @IBAction func onClickEdit(_ sender: Any) {
        casualtyEditHandle?()
    }
}
