//
//  MessageReplyCell.swift
//  EMIS
//
//  Created by Jimmy on 2022/11/4.
//

import UIKit

typealias ConfirmHandle = () -> Void
typealias PickMediaHandle = (Int) -> Void

class MessageReplyCell: UITableViewCell {

    @IBOutlet weak var replyTextView: CustomTextView!
    
    @IBOutlet weak var addPicView0: UIView!
    @IBOutlet weak var addPicButton0: UIButton!
    @IBOutlet weak var delPicButton0: UIButton!
    
    @IBOutlet weak var addPicView1: UIView!
    @IBOutlet weak var addPicButton1: UIButton!
    @IBOutlet weak var delPicButton1: UIButton!
    
    @IBOutlet weak var addPicView2: UIView!
    @IBOutlet weak var addPicButton2: UIButton!
    @IBOutlet weak var delPicButton2: UIButton!
    
    @IBOutlet weak var addVideoView0: UIView!
    @IBOutlet weak var addVideoButton0: UIButton!
    @IBOutlet weak var delVideoButton0: UIButton!
    
    @IBOutlet weak var confirmButton: CustomButton!
    
    var confirmHandle: ConfirmHandle?
    var pickMediaHandle: PickMediaHandle?
    var imageList: [FileModel] = [FileModel()]
    var videoList: [FileModel] = [FileModel()]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addPicButton0.tag = 0
        delPicButton0.tag = 0
        
        addPicButton1.tag = 1
        delPicButton1.tag = 1
        
        addPicButton2.tag = 2
        delPicButton2.tag = 2
        
        addVideoButton0.tag = 3
        delVideoButton0.tag = 3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateUI() {
        let addImage = UIImage(named: "step2_ic_add")
        switch imageList.count {
        case 0:
            addPicView0.isHidden = false
            addPicButton0.setImage(addImage, for: .normal)
            delPicButton0.isHidden = true
            
            addPicView1.isHidden = true
            
            addPicView2.isHidden = true

        case 1:
            let image0 = imageList[0].image

            addPicView0.isHidden = false
            addPicButton0.setImage(image0, for: .normal)
            delPicButton0.isHidden = false

            addPicView1.isHidden = false
            addPicButton1.setImage(addImage, for: .normal)
            delPicButton1.isHidden = true

            addPicView2.isHidden = true
            
        case 2:
            let image0 = imageList[0].image
            let image1 = imageList[1].image

            addPicView0.isHidden = false
            addPicButton0.setImage(image0, for: .normal)
            delPicButton0.isHidden = false

            addPicView1.isHidden = false
            addPicButton1.setImage(image1, for: .normal)
            delPicButton1.isHidden = false

            addPicView2.isHidden = false
            addPicButton2.setImage(addImage, for: .normal)
            delPicButton2.isHidden = true

        case 3:
            let image0 = imageList[0].image
            let image1 = imageList[1].image
            let image2 = imageList[2].image

            addPicView0.isHidden = false
            addPicButton0.setImage(image0, for: .normal)
            delPicButton0.isHidden = false

            addPicView1.isHidden = false
            addPicButton1.setImage(image1, for: .normal)
            delPicButton1.isHidden = false

            addPicView2.isHidden = false
            addPicButton2.setImage(image2, for: .normal)
            delPicButton2.isHidden = false

            
        default:
            addPicView0.isHidden = true
            addPicView1.isHidden = true
            addPicView2.isHidden = true
        }
        
        switch videoList.count {
        case 0:
            addVideoButton0.setImage(addImage, for: .normal)
            delVideoButton0.isHidden = true

        case 1:
            let image0 = videoList[0].image
            addVideoButton0.setImage(image0, for: .normal)
            delVideoButton0.isHidden = false
            
        default:
            delVideoButton0.isHidden = true

        }

    }
    
    @IBAction func onClickConfirm(_ sender: Any) {
        confirmHandle?()

    }
}
