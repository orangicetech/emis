//
//  ImageCell.swift
//  EMIS
//
//  Created by Polowu on 2021/3/6.
//

import UIKit
typealias DeleteHandler = () -> Void
class ImageCell: UICollectionViewCell {
    var deleteHandler: DeleteHandler!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func onClickDelete(_ sender: Any) {
        deleteHandler?()
    }
}
