//
//  MessageContentCell.swift
//  EMIS
//
//  Created by Jimmy on 2022/11/1.
//

import UIKit
import SDWebImage
import AVFoundation

class MessageContentCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftContentLabel: UILabel!
    @IBOutlet weak var leftContentBackgroundView: CustomView!
    @IBOutlet weak var rightContentLabel: UILabel!
    @IBOutlet weak var rightContentBackgroundView: CustomView!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var stackViewConstraints: NSLayoutConstraint!
    
    // add this for link
    @IBOutlet weak var leftTextView: UITextView!
    @IBOutlet weak var rightTextView: UITextView!
    
    var model: ReplyMessageModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setMsgContent(data: ReplyMessageModel) {
        model = data
        
        let fromId = model.from
        let msg = model.msg
        
        // 1: self, 2: admin
        leftContentLabel.isHidden = true
        rightContentLabel.isHidden = true
        leftTextView.textColor = UIColor.white
        rightTextView.textColor = UIColor.white
        
        buttonStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        if fromId == 1 {
            titleLabel.text = model.cdate
            rightContentLabel.text = " " + msg + " "
            rightTextView.text = msg

            titleLabel.textAlignment = .right
            
            buttonStackView.alignment = .trailing
            leftContentBackgroundView.isHidden = true
            rightContentBackgroundView.isHidden = false
            
            leftTextView.isHidden = true
            rightTextView.isHidden = false
            
        } else {
            titleLabel.text = model.fromname + " " + model.cdate
            leftContentLabel.text = " " + msg + " "
            leftTextView.text = msg
            titleLabel.textAlignment = .left
            
            buttonStackView.alignment = .leading
            leftContentBackgroundView.isHidden = false
            rightContentBackgroundView.isHidden = true
            
            leftTextView.isHidden = false
            rightTextView.isHidden = true
        }
        
        for i in 0..<model.pic.count {
            let url = model.pic[i]
            let btn = UIButton(type: .custom)
            btn.tag = i
            
//            btn.sd_setImage(with: URL(string: url), for: .normal)
            btn.sd_setImage(with: URL(string: url), for: .normal) { (image, error, cacheType, imageURL) in
                if let newImage = image?.sd_resizedImage(with: CGSize(width: 100, height: 100), scaleMode: .aspectFit) {
                    btn.setImage(newImage, for: .normal)
                }
            }
            btn.addTarget(self, action:#selector(onPicButtonClicked), for: .touchUpInside)
            buttonStackView.addArrangedSubview(btn)
        }
        
        for i in 0..<model.video.count {
            let url = model.video[i]
            let btn = UIButton(type: .custom)
            btn.tag = i

            btn.setImage(UIImage(named: "ic_video_play"), for: .normal)
            if let thumbnailImage = getThumbnailImage(url: url) {
                btn.setImage(thumbnailImage, for: .normal)
            }
            
            btn.addTarget(self, action:#selector(onVideoButtonClicked), for: .touchUpInside)
            buttonStackView.addArrangedSubview(btn)
        }
        
        let stackHeight = (model.pic.count + model.video.count) * 100 + (model.pic.count + model.video.count - 1) * 5
        if (stackHeight > 0) {
            stackViewConstraints.constant = CGFloat(stackHeight)
        } else {
            stackViewConstraints.constant = 0
        }
    }
    
    @objc func onPicButtonClicked(sender:UIButton) {
        let url = model.pic[sender.tag]
        
        if let url = URL(string: url), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                          })
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func onVideoButtonClicked(sender:UIButton) {
        let url = model.video[sender.tag]

        if let url = URL(string: url), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                          })
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func getThumbnailImage(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print("getThumbnailImage error:\(error)")
        }
        return nil
    }
}

