//
//  DesSampleCell.swift
//  EMIS
//
//  Created by Polo iMac on 2021/3/2.
//

import UIKit

class DesSampleCell: UITableViewCell {
    @IBOutlet weak var bView_1: CustomView!
    @IBOutlet weak var bView_2: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    public var cellSelected: Bool = false {
        didSet {
            if cellSelected {
                bView_1.lineColor = UIColor(named: "DSSColor")!
                bView_2.backgroundColor = UIColor(named: "DSSColor")!
            } else {
                bView_1.lineColor = UIColor(named: "172Color")!
                bView_2.backgroundColor = UIColor(named: "172Color")!
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
