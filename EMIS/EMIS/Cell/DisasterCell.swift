//
//  DisasterCell.swift
//  EMIS
//
//  Created by Polo iMac on 2021/2/22.
//

import UIKit

class DisasterCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: CustomView!
    
    var setSelected: Bool = false {
        didSet {
            if setSelected {
                bgView.backgroundColor = UIColor(named: "Rank2Color")
            } else {
                bgView.backgroundColor = UIColor(named: "172Color")
            }
        }
    }
}
