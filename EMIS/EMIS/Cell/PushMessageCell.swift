//
//  PushMessageCell.swift
//  EMIS
//
//  Created by Jimmy on 2022/10/17.
//

import UIKit

class PushMessageCell: UITableViewCell {

    @IBOutlet weak var titleBackgroundView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    public var isReaded: Bool = false {
        didSet {
            if isReaded {
                titleBackgroundView.backgroundColor = UIColor(named: "136Color")
                titleLabel.textColor = UIColor.white
                contentLabel.textColor = UIColor(named: "104Color")
                dateLabel.textColor = UIColor(named: "104Color")
            } else {
                titleBackgroundView.backgroundColor = UIColor(named: "MedBlueColor")
                titleLabel.textColor = UIColor.white
                contentLabel.textColor = UIColor(named: "DarkBlueColor")
                dateLabel.textColor = UIColor(named: "DarkBlueColor")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
