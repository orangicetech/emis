//
//  MergerCaseCell.swift
//  EMIS
//
//  Created by Polowu on 2021/3/12.
//
import UIKit

class MergerCaseCell: UITableViewCell {
    var deleteHandler: DeleteHandler!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onClickDelete(_ sender: Any) {
        deleteHandler?()
    }
}
