//
//  CaseListCell.swift
//  EMIS
//
//  Created by Polowu on 2021/3/7.
//

import UIKit

enum CaseStatus: Int {
    case Pending
    case Processing
}

class CaseListCell: UITableViewCell {

    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var maskImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    var status: String = "" {
        didSet {
            if status.contains("待回覆"){
                statusLabel.text = String(format: "%@", status)
                maskImageView.image = UIImage(named: "list_img_under")
            } else {
                statusLabel.text = String(format: "%@", status)
                maskImageView.image = UIImage(named: "list_img_ing")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
