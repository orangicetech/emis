//
//  CustomTextView.swift
//  EMIS
//
//  Created by Polo iMac on 2021/4/28.
//

import UIKit

class CustomTextView: UITextView {

    private var hasChinese:Bool = true
    public var hasSpecial:Bool = false
    public var hasAllSpecial:Bool = false
    private var long:Int = 20
    func addHandler(hasChinese: Bool, long: Int) {
        self.hasChinese = hasChinese
        self.long = long
        self.delegate = self
    }
    
    func topMostController() -> UIViewController? {
        var topController = UIApplication.shared.windows[0].rootViewController
        while let presentedViewController = topController?.presentedViewController {
            topController = presentedViewController
        }
        
        if NSStringFromClass(topController!.classForCoder) == "UIAlertController" {
            topController?.dismiss(animated: false, completion: nil)
            return UIApplication.shared.windows[0].rootViewController
        } else {
            return topController
        }
    }
}

extension CustomTextView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        print("textField.text: \(textView.text!)")
//        print("range: \(range.location)")
//        print("text: \(text)")
//        print("")
        
//        if textView.markedTextRange != nil {
//            showTextFieldErrorWith(errorCode: .space)
//            return false
//        }
//        // 不能輸入空白
//        if text.isContainsSpaceCharacters() {
//            showTextFieldErrorWith(errorCode: .space)
//            return false
//        }
        
        // 不能輸入中文字
        if text.isContainsChineseCharacters() {
            if !hasChinese {
                showTextFieldErrorWith(errorCode: .chinese)
            }
            return self.hasChinese
        }
        
        // 是否含有特殊字元
        if text.hasSpecialCharacter() {
            let specials: [String] = [" ",".",",",":",";","?","!","(",")","。","，","、",":","；","？","！","（","）"]
            var check = false
            var has = false
            specials.forEach {
                check = text.range(of:$0) != nil
                if check {
                    has =  true
                }
            }
            if hasAllSpecial {
                return true
            } else {
                if hasSpecial {
                    if !has {
                        showTextFieldErrorWith(errorCode: .special)
                    }
                    return has
                } else  {
                    showTextFieldErrorWith(errorCode: .special)
                    return false
                }
            }
        }
        
        // 長度不得大於等於 long
        guard let textViewText = textView.text,
                // 被取代的字數在 textField 的 range
                let rangeOfTextToReplace = Range(range, in: textViewText) else {
            return false
        }
        // 被取代的字
        let substringToReplace = textViewText[rangeOfTextToReplace]
        
        // textFieldText.count = textField.count(已存在的字數) - substringToReplace.count(被取代的字數) + string.count(輸入的字數)
        let nsString = textView.text as NSString?
        if let replaced = nsString?.replacingCharacters(in: range, with: text) {
            if replaced.count >= long {
                showTextFieldErrorWith(errorCode: .tooLong)
            }
            return (replaced.count <= long)
        }
        
        let count = textViewText.count - substringToReplace.count + text.count
        if count >= long {
            showTextFieldErrorWith(errorCode: .tooLong)
            return false
        }
        
        return true
    }
    
    func showTextFieldErrorWith(errorCode: TextFieldErrorCode) {
        var str = ""
        
        switch errorCode {
        case .chinese:
            str = "此欄位不支援中文"
        case .space:
            str = "此欄位不支援空白"
        case .special:
            str = "此欄位不支援特殊符號"
        case .tooLong:
            str = "輸入字數以超過上限"
        }
        
        ViewRouter.shared.presentPromptFrom(viewController: topMostController()!, message: str, type: .Alert) { (buttonEvent) in
            
        }
    }
}
