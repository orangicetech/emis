//
//  POButton.swift
//  Cupola360DashCam
//
//  Created by Polo iMac on 2020/2/18.
//  Copyright © 2020 Cupola360 Inc. All rights reserved.
//

import UIKit
typealias Highlighted = (Bool) -> Void
@IBDesignable
class CustomButton: UIButton {
    var buttonHighlighted: Highlighted!
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var lineWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = lineWidth
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var lineColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = lineColor.cgColor
            setNeedsDisplay()
        }
    }
    
    override var isHighlighted: Bool {
            didSet {
                buttonHighlighted?(isHighlighted)
            }
        }
}
