//
//  AppDelegate.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/4.
//

import UIKit
import IQKeyboardManager
import GoogleMaps
import FirebaseCore
import FirebaseMessaging

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in }
          )
        
        application.registerForRemoteNotifications()
        UNUserNotificationCenter.current().delegate = self

        IQKeyboardManager.shared().isEnabled = true
        GMSServices.provideAPIKey(MapManager.shared.API_KEY)
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        EMISManager.shared.setupRemoteConfig()


        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                if #available(iOS 10.0, *) {
                    application.open(URL(string: url)!, options: [:], completionHandler: nil)
                }
                else {
                    application.openURL(URL(string: url)!)
                }
                return
            }
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    // 使用者點選推播時觸發
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(#function)
        let content = response.notification.request.content
        print(content.userInfo)
        if let type = Int(content.userInfo["typeid"] as! String) {
            EMISManager.shared.pushtype = type
            // 留言
            if (type == 4) {
                if let replyno = Int(content.userInfo["replyno"] as! String) {
                    EMISManager.shared.replyno = replyno
                    if (ViewRouter.shared.navigationController != nil) {
                        if (ViewRouter.shared.navigationController.viewControllers.first! is ProjectListController) {
                            ViewRouter.shared.showMessageListFrom(viewController: ViewRouter.shared.navigationController.viewControllers.first!)
                        } else {
                            ViewRouter.shared.navigationController.popToRootViewController(animated: true)
                        }
                    }
                }
                
            } else {
                if let no = Int(content.userInfo["no"] as! String) {
                    EMISManager.shared.pushno = no
                    if (ViewRouter.shared.navigationController != nil) {
                        if (ViewRouter.shared.navigationController.viewControllers.first! is ProjectListController) {
                            ViewRouter.shared.showPushMessageListFrom(viewController: ViewRouter.shared.navigationController.viewControllers.first!)
                        } else {
                            ViewRouter.shared.navigationController.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }
        completionHandler()
    }
    
    // 讓 App 在前景也能顯示推播
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if #available(iOS 14.0, *) {
            completionHandler([.badge, .sound, .alert])
        } else {
            // Fallback on earlier versions
        }
    }
    
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken ?? "")")
        if let token = fcmToken {
            UserManager.shared.fcmToken = token
        }
    }
}
