//
//  ViewRouter.swift
//  EMIS
//
//  Created by Polo iMac on 2021/1/25.
//

public enum ButtonEvent {
    case cancel
    case confim
}

public enum SampleEvent {
    case cancel
    case confim
    case no_sample
}

import UIKit
typealias ButtonConfimHandle = (ButtonEvent) -> Void
typealias MapInfoConfimHandle = (Bool, MapInfo?) -> Void
typealias SampleConfimHandle = (SampleEvent, DesSampleModel?) -> Void
typealias CasualtyConfimHandle  = (Bool, CasualtyModel?) -> Void

class ViewRouter: NSObject {
    static let shared = ViewRouter()
    var function: FunctionController!
    var maskController: MaskController!
    var offlineListController: OfflineCaseListViewController!
    var navigationController: UINavigationController!
    
    func presentToProjectListFrom(viewController: UIViewController) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        navigationController = storyBoard.instantiateViewController(withIdentifier: "ProjectListIdentifier") as! UINavigationController
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.modalTransitionStyle = .crossDissolve
        viewController.present(navigationController, animated: false) {
        }
    }
    
    func presentToOfflineCaseListFrom(viewController: UIViewController) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "OfflineCase", bundle: nil)
        let offlineListController = (storyBoard.instantiateViewController(withIdentifier: "OfflineCaseListINavdentity") as! UINavigationController)
        offlineListController.modalPresentationStyle = .overCurrentContext
        offlineListController.modalTransitionStyle = .crossDissolve
        viewController.present(offlineListController, animated: true, completion: nil)
    }
    
    func showOfflineCaseListFrom(viewController: UIViewController) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "OfflineCase", bundle: nil)
        let offlineListController = (storyBoard.instantiateViewController(withIdentifier: "OfflineCaseListIdentity") as! OfflineCaseListViewController)
        offlineListController.modalPresentationStyle = .overCurrentContext
        viewController.navigationController?.pushViewController(offlineListController, animated: true)
    }
    
    func showPushMessageListFrom(viewController: UIViewController) {
        var pushMessageController: PushMessageListViewController!
        let storyBoard: UIStoryboard = UIStoryboard(name: "PushMessage", bundle: nil)
        pushMessageController = (storyBoard.instantiateViewController(withIdentifier: "PushMessageIdentity") as! PushMessageListViewController)
        pushMessageController.modalPresentationStyle = .overCurrentContext
        
        viewController.navigationController?.pushViewController(pushMessageController, animated: true)
    }
    
    func showPushMessageContentFrom(viewController: UIViewController, model: PushLogModel) {
        var pushMessageContentController: PushMessageContentViewController!
        let storyBoard: UIStoryboard = UIStoryboard(name: "PushMessage", bundle: nil)
        pushMessageContentController = (storyBoard.instantiateViewController(withIdentifier: "PushMessageContentIdentity") as! PushMessageContentViewController)
        pushMessageContentController.modalPresentationStyle = .overCurrentContext
        pushMessageContentController.model = model
        
        viewController.navigationController?.pushViewController(pushMessageContentController, animated: true)
    }
    
    func showMessageListFrom(viewController: UIViewController) {
        var messageController: MessageListViewController!
        let storyBoard: UIStoryboard = UIStoryboard(name: "Message", bundle: nil)
        messageController = (storyBoard.instantiateViewController(withIdentifier: "MessageIdentity") as! MessageListViewController)
        messageController.modalPresentationStyle = .overCurrentContext
        

        viewController.navigationController?.pushViewController(messageController, animated: true)
    }
    
    func showMessageContentFrom(viewController: UIViewController, model: PushLogModel) {
        var messageContentController: MessageContentViewController!
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Message", bundle: nil)
        messageContentController = (storyBoard.instantiateViewController(withIdentifier: "MessageContentIdentity") as! MessageContentViewController)
        messageContentController.modalPresentationStyle = .overCurrentContext
        messageContentController.model = model
        
        viewController.navigationController?.pushViewController(messageContentController, animated: true)
    }
    
    func presentWarningFrom(viewController: UIViewController,title: String, message: String, type: PromptType, confimHandle: ButtonConfimHandle?) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let promptController = storyBoard.instantiateViewController(withIdentifier: "PromptController") as! PromptController
        promptController.modalPresentationStyle = .overCurrentContext
        promptController.modalTransitionStyle = .crossDissolve
        promptController.confimHandle = confimHandle
        promptController.titleString = title
        promptController.message = message
        promptController.type = type
        viewController.present(promptController, animated: true, completion: nil)
    }
    
    func showFunctionFrom(viewController: UIViewController, model: ProjectListModel) {
        if function == nil {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
            function = (storyBoard.instantiateViewController(withIdentifier: "FunctionIdentity") as! FunctionController)
            function.modalPresentationStyle = .overCurrentContext
        }

        function.projectModel = model
        viewController.navigationController?.pushViewController(function, animated: true)
    }
    
    func backToFunctionFrom(viewController: UIViewController) {
        viewController.navigationController?.popToViewController(function, animated: true)
    }
    
    func showNewCaseFrom(viewController: UIViewController, model: ProjectListModel?) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "NewCase", bundle: nil)
        let newCase = storyBoard.instantiateViewController(withIdentifier: "NewCaseIdentity") as! NewCaseController
        newCase.modalPresentationStyle = .overCurrentContext
        newCase.projectModel = model

        viewController.navigationController?.pushViewController(newCase, animated: true)
    }
    
    func showOfflineCaseFrom(viewController: UIViewController, index: Int, model: OfflineCaseModel?) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "NewCase", bundle: nil)
        let newCase = storyBoard.instantiateViewController(withIdentifier: "NewCaseIdentity") as! NewCaseController
        newCase.modalPresentationStyle = .overCurrentContext
        newCase.offlineModel = model
        newCase.offlineDataIndex = index
        if (EMISManager.shared.isLogin.value) {
            newCase.isLoginEdit = true
        }
        viewController.navigationController?.pushViewController(newCase, animated: true)
    }
  
    
    func showCaseDetalFrom(viewController: UIViewController, model: CaseListModel, projectModel:ProjectListModel) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let caseDetal = storyBoard.instantiateViewController(withIdentifier: "CaseDetalController") as! CaseDetalController
        caseDetal.modalPresentationStyle = .overCurrentContext
        caseDetal.caseModel = model
        caseDetal.projectModel = projectModel
        viewController.navigationController?.pushViewController(caseDetal, animated: true)
    }
    
    func showCaseReportFrom(viewController: UIViewController, model: CaseListModel, projectModel:ProjectListModel, caseDetailModel:CaseDetailModel) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let caseReport = storyBoard.instantiateViewController(withIdentifier: "CaseReportController") as! CaseReportController
        caseReport.modalPresentationStyle = .overCurrentContext
        caseReport.caseModel = model
        caseReport.projectModel = projectModel
        caseReport.caseDetailModel = caseDetailModel
        viewController.navigationController?.pushViewController(caseReport, animated: true)
    }
    
    func showUploadListFrom(viewController: UIViewController, projectModel:ProjectListModel) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let upload = storyBoard.instantiateViewController(withIdentifier: "UploadController") as! UploadController
        upload.modalPresentationStyle = .overCurrentContext
        upload.projectModel = projectModel
        viewController.navigationController?.pushViewController(upload, animated: true)
    }
    
    func presentToMapViewFrom(viewController: UIViewController, mapInfo: MapInfo?, confimHandle: @escaping MapInfoConfimHandle) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let mapView = storyBoard.instantiateViewController(withIdentifier: "MapController") as! MapController
        mapView.modalPresentationStyle = .overCurrentContext
        mapView.modalTransitionStyle = .crossDissolve
        mapView.confimHandle = confimHandle
        mapView.mapInfo = mapInfo
        viewController.present(mapView, animated: true, completion: nil)
    }
    
    func presentToSampleViewFrom(viewController: UIViewController, sampleList: [DesSampleModel], confimHandle: @escaping SampleConfimHandle) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let sampleView = storyBoard.instantiateViewController(withIdentifier: "DesSampleController") as! DesSampleController
        sampleView.modalPresentationStyle = .overCurrentContext
        sampleView.modalTransitionStyle = .crossDissolve
        sampleView.sampleList = sampleList
        sampleView.confimHandle = confimHandle
        viewController.present(sampleView, animated: true, completion: nil)
    }
    
    func presentToCasualtyViewFrom(viewController: UIViewController, model: CasualtyModel?, confimHandle: @escaping CasualtyConfimHandle) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let casualtyView = storyBoard.instantiateViewController(withIdentifier: "CasualtyController") as! CasualtyController
        casualtyView.modalPresentationStyle = .overCurrentContext
        casualtyView.modalTransitionStyle = .crossDissolve
        casualtyView.model = model
        casualtyView.confimHandle = confimHandle
        viewController.present(casualtyView, animated: true, completion: nil)
    }
    
    func presentPromptFrom(viewController: UIViewController, title: String = "", message: String, type: PromptType, confimHandle: ButtonConfimHandle?) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let promptController = storyBoard.instantiateViewController(withIdentifier: "PromptController") as! PromptController
        promptController.modalPresentationStyle = .overCurrentContext
        promptController.modalTransitionStyle = .crossDissolve
        promptController.confimHandle = confimHandle
        promptController.titleString = title
        promptController.message = message
        promptController.type = type
        viewController.present(promptController, animated: true, completion: nil)
    }
    
    func presentToMergerCaseFrom(viewController: UIViewController, caseDetailModel: CaseDetailModel) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let mergerCase = storyBoard.instantiateViewController(withIdentifier: "MergerCaseController") as! MergerCaseController
        mergerCase.modalPresentationStyle = .overCurrentContext
        mergerCase.modalTransitionStyle = .crossDissolve
        mergerCase.caseDetailModel = caseDetailModel
        viewController.present(mergerCase, animated: true, completion: nil)
    }
    
    func presentToNearDetailFrom(viewController: UIViewController, nearCaseModel: NearCaseModel) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let nearDetail = storyBoard.instantiateViewController(withIdentifier: "NearDetailController") as! NearDetailController
        nearDetail.modalPresentationStyle = .overCurrentContext
        nearDetail.modalTransitionStyle = .crossDissolve
        nearDetail.model = nearCaseModel
        viewController.present(nearDetail, animated: true, completion: nil)
    }
    
    func presentToCasualtyDetalFrom(viewController: UIViewController, text: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
        let casualtyDetal = storyBoard.instantiateViewController(withIdentifier: "CasualtyDetalController") as! CasualtyDetalController
        casualtyDetal.modalPresentationStyle = .overCurrentContext
        casualtyDetal.modalTransitionStyle = .crossDissolve
        casualtyDetal.text = text
        viewController.present(casualtyDetal, animated: true, completion: nil)
    }
    
    func showMaskView() {
        if maskController == nil {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            maskController = (storyBoard.instantiateViewController(withIdentifier: "MaskController") as! MaskController)
            maskController.modalPresentationStyle = .overCurrentContext
            maskController.modalTransitionStyle = .crossDissolve
        }
        
        if !(topMostController() is MaskController) {
            topMostController()!.present(maskController, animated: true, completion: nil)
        }
    }
    
    func dismissMaskView() {
        if maskController != nil {
            maskController.dismiss(animated: true, completion: nil)
        }
    }
    
    func topMostController() -> UIViewController? {
        var topController = UIApplication.shared.windows[0].rootViewController
        while let presentedViewController = topController?.presentedViewController {
            topController = presentedViewController
        }
        if NSStringFromClass(topController!.classForCoder) == "UIAlertController" {
            topController?.dismiss(animated: false, completion: nil)
            return UIApplication.shared.windows[0].rootViewController
        } else {
            return topController
        }
    }
}



